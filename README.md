## Update Pricebook

```
// standard pricebook entries
map<id, PricebookEntry> PricebookEntriesMAP = new map<id, PricebookEntry>();

for (PricebookEntry pbe : [SELECT Id,Pricebook2Id,Product2Id, IsActive  FROM PricebookEntry where Pricebook2.IsStandard = true] )
{
	PricebookEntriesMAP.put(pbe.Product2Id, pbe);    
}

list<PricebookEntry> PricebookEntries = new list<PricebookEntry> ();

// check for those products witout a standard price
for (Product2 product : [select id from Product2])
{
    PricebookEntry pbe = new PricebookEntry();
    if (PricebookEntriesMAP.containsKey(product.id))
    {
        pbe = PricebookEntriesMAP.get(product.id);        
    }else {
        pbe.Product2Id = product.id;
        pbe.Pricebook2Id = '01s60000000AZIZAA4';
        pbe.UnitPrice = 1;    
    }
	pbe.isActive=true;
    
	PricebookEntries.add(pbe);    
}

upsert PricebookEntries;

```
---

## update new quantity field
```
list<Lease_Proposal_Acceptance_Form__c> lpafs = [select Quote__r.Equipment_Pricing_Request__r.Quantity__c, LPAF_Quantity__c  from Lease_Proposal_Acceptance_Form__c where LPAF_Quantity__c=0 or LPAF_Quantity__c=null];
for (Lease_Proposal_Acceptance_Form__c lpaf : lpafs)
{
	lpaf.LPAF_Quantity__c = lpaf.Quote__r.Equipment_Pricing_Request__r.Quantity__c;	    
}

update lpafs;
```

## update total of Equipment Pricing Requests
```
equipmentPricingHelper.RollupERtotal([select id, Equipment_Pricing_Request__c, unit_price__c from quote] , null);    
```