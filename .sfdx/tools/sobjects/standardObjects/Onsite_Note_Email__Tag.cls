// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Onsite_Note_Email__Tag {
    global Datetime CreatedDate;
    global Id Id;
    global Boolean IsDeleted;
    global Onsite_Note_Email__c Item;
    global Id ItemId;
    global String Name;
    global Datetime SystemModstamp;
    global TagDefinition TagDefinition;
    global Id TagDefinitionId;
    global String Type;

    global Onsite_Note_Email__Tag () 
    {
    }
}