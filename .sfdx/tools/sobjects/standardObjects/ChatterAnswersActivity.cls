// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ChatterAnswersActivity {
    global Integer BestAnswerReceivedCount;
    global Integer BestAnswerSelectedCount;
    global Community Community;
    global Id CommunityId;
    global Id Id;
    global Integer QuestionSubscrCount;
    global Integer QuestionSubscrReceivedCount;
    global Integer QuestionUpVotesCount;
    global Integer QuestionUpVotesReceivedCount;
    global Integer QuestionsCount;
    global Integer RepliesCount;
    global Integer ReplyDownVotesCount;
    global Integer ReplyDownVotesReceivedCount;
    global Integer ReplyUpVotesCount;
    global Integer ReplyUpVotesReceivedCount;
    global Integer ReportAbuseOnQuestionsCount;
    global Integer ReportAbuseOnRepliesCount;
    global Integer ReportAbuseReceivedOnQnCount;
    global Integer ReportAbuseReceivedOnReCount;
    global Datetime SystemModstamp;
    global User User;
    global Id UserId;

    global ChatterAnswersActivity () 
    {
    }
}