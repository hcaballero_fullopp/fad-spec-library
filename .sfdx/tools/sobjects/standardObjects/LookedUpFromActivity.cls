// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class LookedUpFromActivity {
    global Boolean AB__c;
    global Account Account;
    global Id AccountId;
    global String Account_Name__c;
    global Date ActivityDate;
    global String ActivitySubtype;
    global String ActivityType;
    global String Assigned_To_ID__c;
    global String BDEX__c;
    global Boolean BH__c;
    global Boolean BTE__c;
    global Double C15__c;
    global Double C30__c;
    global Double C45__c;
    global Double C60__c;
    global Boolean CC__c;
    global String CEO_Notes__c;
    global String CallDisposition;
    global Integer CallDurationInSeconds;
    global String CallObject;
    global String CallType;
    global Double Calls_Total__c;
    global Id Campaign__c;
    global Campaign Campaign__r;
    global String Contact_ID__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Boolean DU__c;
    global String Date_Bucket__c;
    global String Description;
    global String Dress_Code__c;
    global Integer DurationInMinutes;
    global Double E15__c;
    global Double E30__c;
    global Double E45__c;
    global Double E60__c;
    global Double Emails_Total__c;
    global Datetime EndDateTime;
    global String Event_Notes__c;
    global String Event_Status__c;
    global Boolean FB__c;
    global String Ground_Transportation__c;
    global Boolean HB__c;
    global Double Highlander__c;
    global String HubSpot_Inc__Guid__c;
    global Id Id;
    global Boolean IsAllDayEvent;
    global Boolean IsClosed;
    global Boolean IsDeleted;
    global Boolean IsHighPriority;
    global Boolean IsReminderSet;
    global Boolean IsTask;
    global Boolean IsVisibleInSelfService;
    global Boolean JF__c;
    global Boolean John_Flynn_Presence_Requested__c;
    global Datetime LID__Date_Sent__c;
    global String LID__URL__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String LegacyId__c;
    global String Legacy_CallType__c;
    global String Legacy_ContactNM__c;
    global String Location;
    global Boolean MM__c;
    global Boolean MS__c;
    global Double MTD__c;
    global String Meeting_Address__c;
    global String Nearest_Airport__c;
    global Double NoFA__c;
    global Double NoOEM__c;
    global Double NoOther__c;
    global Double NoPeopleNet__c;
    global Double NoTargetAttend__c;
    global Datetime Note_FU__c;
    global String Note_User__c;
    global Double OS120__c;
    global Double OS30__c;
    global Double OS60__c;
    global Double OS90__c;
    global Datetime OnSite_Sched__c;
    global Boolean OnSite_XCL__c;
    global Boolean On_Site__c;
    global Boolean On_Site_with_Finance__c;
    global Boolean On_Site_with_Ops__c;
    global Double Onsite_Call_30__c;
    global String Onsite_Call_Id__c;
    global Double Onsite_Total__c;
    global String Opportunity_ID__c;
    global String Other_Location__c;
    global Boolean Other__c;
    global User Owner;
    global Id OwnerId;
    global Boolean PF__c;
    global Account PrimaryAccount;
    global Id PrimaryAccountId;
    global SObject PrimaryWho;
    global Id PrimaryWhoId;
    global String Priority;
    global Double QTD__c;
    global Datetime ReminderDateTime;
    global String Reminder__c;
    global Boolean Ride_Along_w_BDEX__c;
    global String Standard_Presentations__c;
    global Datetime StartDateTime;
    global String Status;
    global String Subject;
    global Datetime SystemModstamp;
    global Boolean TC__c;
    global String Time_Zone__c;
    global String TypeText__c;
    global Boolean VC__c;
    global SObject What;
    global Id WhatId;
    global SObject Who;
    global Id WhoId;
    global Double YTD__c;

    global LookedUpFromActivity () 
    {
    }
}