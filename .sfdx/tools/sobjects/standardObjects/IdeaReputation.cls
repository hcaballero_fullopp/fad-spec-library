// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class IdeaReputation {
    global Integer CommentCount;
    global Integer CommentLikesReceivedCount;
    global Integer CommentsReceivedCount;
    global SObject Context;
    global Id ContextId;
    global Integer DownVotesGivenCount;
    global Integer DownVotesReceivedCount;
    global Id Id;
    global Integer IdeaCount;
    global String ReputationLevel;
    global Double Score;
    global Datetime SystemModstamp;
    global Integer UpVotesGivenCount;
    global Integer UpVotesReceivedCount;
    global User User;
    global Id UserId;

    global IdeaReputation () 
    {
    }
}