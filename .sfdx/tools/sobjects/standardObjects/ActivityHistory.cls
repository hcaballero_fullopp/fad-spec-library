// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ActivityHistory {
    global Boolean AB__c;
    global Account Account;
    global Id AccountId;
    global String Account_Name__c;
    global Date ActivityDate;
    global String ActivitySubtype;
    global String ActivityType;
    global EmailMessage AlternateDetail;
    global Id AlternateDetailId;
    global String Assigned_To_ID__c;
    global String BDEX__c;
    global Boolean BH__c;
    global Boolean BTE__c;
    global Boolean CC__c;
    global String CEO_Notes__c;
    global String CallDisposition;
    global Integer CallDurationInSeconds;
    global String CallObject;
    global String CallType;
    global Id Campaign__c;
    global Campaign Campaign__r;
    global String Contact_ID__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Boolean DU__c;
    global String Date_Bucket__c;
    global String Description;
    global Integer DurationInMinutes;
    global Datetime EndDateTime;
    global String Event_Notes__c;
    global String Event_Status__c;
    global Boolean FB__c;
    global Boolean HB__c;
    global Double Highlander__c;
    global String HubSpot_Inc__Guid__c;
    global Id Id;
    global Boolean IsAllDayEvent;
    global Boolean IsClosed;
    global Boolean IsDeleted;
    global Boolean IsHighPriority;
    global Boolean IsReminderSet;
    global Boolean IsTask;
    global Boolean IsVisibleInSelfService;
    global Boolean JF__c;
    global Datetime LID__Date_Sent__c;
    global String LID__URL__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String LegacyId__c;
    global String Legacy_CallType__c;
    global String Legacy_ContactNM__c;
    global String Location;
    global Boolean MM__c;
    global Boolean MS__c;
    global Double MTD__c;
    global Double NoFA__c;
    global Double NoOEM__c;
    global Double NoOther__c;
    global Double NoPeopleNet__c;
    global Double NoTargetAttend__c;
    global Datetime Note_FU__c;
    global String Note_User__c;
    global Datetime OnSite_Sched__c;
    global Boolean OnSite_XCL__c;
    global Boolean On_Site__c;
    global Boolean On_Site_with_Finance__c;
    global Boolean On_Site_with_Ops__c;
    global Double Onsite_Call_30__c;
    global String Onsite_Call_Id__c;
    global String Opportunity_ID__c;
    global String Other_Location__c;
    global Boolean Other__c;
    global User Owner;
    global Id OwnerId;
    global Boolean PF__c;
    global Account PrimaryAccount;
    global Id PrimaryAccountId;
    global SObject PrimaryWho;
    global Id PrimaryWhoId;
    global String Priority;
    global Double QTD__c;
    global Datetime ReminderDateTime;
    global Boolean Ride_Along_w_BDEX__c;
    global String Standard_Presentations__c;
    global Datetime StartDateTime;
    global String Status;
    global String Subject;
    global Datetime SystemModstamp;
    global Boolean TC__c;
    global Boolean VC__c;
    global SObject What;
    global Id WhatId;
    global SObject Who;
    global Id WhoId;
    global Double YTD__c;

    global ActivityHistory () 
    {
    }
}