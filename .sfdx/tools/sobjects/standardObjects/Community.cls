// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Community {
    global Boolean CanCreateCase;
    global List<ChatterAnswersReputationLevel> ChatterAnswersReputationLevels;
    global List<Case> Community;
    global List<IdeaReputation> Context;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String DataCategoryName;
    global String Description;
    global Boolean HasChatterService;
    global Id Id;
    global List<IdeaReputationLevel> IdeaReputationLevels;
    global Boolean IsActive;
    global Boolean IsPublished;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global Datetime SystemModstamp;

    global Community () 
    {
    }
}