// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Question {
    global Reply BestReply;
    global Id BestReplyId;
    global User BestReplySelectedBy;
    global Id BestReplySelectedById;
    global String Body;
    global Community Community;
    global Id CommunityId;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String CreatorFullPhotoUrl;
    global String CreatorName;
    global String CreatorSmallPhotoUrl;
    global List<QuestionDataCategorySelection> DataCategorySelections;
    global Boolean HasSingleFieldForContent;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Reply LastReply;
    global Datetime LastReplyDate;
    global Id LastReplyId;
    global Datetime LastViewedDate;
    global Integer MostReportAbusesOnReply;
    global Integer NumReplies;
    global Integer NumReportAbuses;
    global Integer NumSubscriptions;
    global String Origin;
    global List<SocialPost> Posts;
    global String Priority;
    global List<QuestionReportAbuse> QuestionReportAbuses;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Reply> Replies;
    global List<QuestionSubscription> Subscriptions;
    global Datetime SystemModstamp;
    global String Title;
    global Integer UpVotes;
    global Double VoteScore;
    global List<Vote> Votes;

    global Question () 
    {
    }
}