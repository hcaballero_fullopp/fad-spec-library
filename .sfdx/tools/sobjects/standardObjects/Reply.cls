// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Reply {
    global List<Question> BestReply;
    global String Body;
    global Community Community;
    global Id CommunityId;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String CreatorFullPhotoUrl;
    global String CreatorName;
    global String CreatorSmallPhotoUrl;
    global Integer DownVotes;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global List<Question> LastReply;
    global String Name;
    global Integer NumReportAbuses;
    global List<SocialPost> Posts;
    global Question Question;
    global Id QuestionId;
    global List<FlowRecordRelation> RelatedRecord;
    global List<ReplyReportAbuse> ReplyReportAbuses;
    global Datetime SystemModstamp;
    global Integer UpVotes;
    global Double VoteTotal;
    global List<Vote> Votes;

    global Reply () 
    {
    }
}