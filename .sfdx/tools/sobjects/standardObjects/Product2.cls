// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Product2 {
    global List<ActivityHistory> ActivityHistories;
    global List<Asset> Assets;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Axles__c;
    global String Brake_Type__c;
    global Double Bumper_to_Back_of_Cab_BBC__c;
    global String Bunk_Type__c;
    global Boolean CARB_Compliant__c;
    global String Cab_Type__c;
    global Double Cab_to_Axle_CA__c;
    global Double Capacity__c;
    global String Chassis_Make__c;
    global String Color__c;
    global List<CombinedAttachment> CombinedAttachments;
    global Double Compartments__c;
    global String Configuration__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global Decimal Cost__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Description;
    global Double Displacement_L__c;
    global String DisplayUrl;
    global String Drive__c;
    global Double E_track_Rows__c;
    global Double Engine_Horsepower__c;
    global String Engine_Make__c;
    global String Engine_Model__c;
    global String Engine_OEM__c;
    global String Engine_Series__c;
    global Double Engine_Torque__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global ExternalDataSource ExternalDataSource;
    global Id ExternalDataSourceId;
    global String ExternalId;
    global String Family;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Product2Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global String Fuel__c;
    global Double GCWR_GVWR__c;
    global Double Gallon_Capacity__c;
    global Double Height__c;
    global List<Product2History> Histories;
    global Id Id;
    global Boolean IsActive;
    global Boolean IsArchived;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Double Length__c;
    global String Limits__c;
    global String Make__c;
    global String Manufacturer__c;
    global String Material__c;
    global String Model_Year__c;
    global String Model__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global String Operation__c;
    global List<Opportunity_SubGroup__c> Opportunity_SubGroups1__r;
    global List<Opportunity_SubGroup__c> Opportunity_SubGroups2__r;
    global List<Opportunity_SubGroup__c> Opportunity_SubGroups3__r;
    global List<Opportunity_SubGroup__c> Opportunity_SubGroups__r;
    global List<FeedItem> Parent;
    global List<PricebookEntry> PricebookEntries;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<OpportunityLineItem> Product2;
    global String ProductCode;
    global String QuantityUnitOfMeasure;
    global Decimal Rebate_UTA__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<FlowRecordRelation> RelatedRecord;
    global Boolean Rollup_Door__c;
    global Boolean Side_Door__c;
    global String Sleeper_Configuration__c;
    global List<Spec_Products__c> Spec_Products__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup1__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup2__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup3__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup4__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup5__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup6__r;
    global List<Opportunity_SubGroup__c> Specs_Formerly_SubGroup__r;
    global Double Speeds__c;
    global String StockKeepingUnit;
    global String Suspension_Type__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global String Tractor_Type__c;
    global String Trailer_Type__c;
    global String Transmission_Configuration__c;
    global String Trim_Level__c;
    global List<UTA_Details__c> UTA_Details_del__r;
    global String Warranty_Type__c;
    global Double Weight__c;
    global String Wheel_Location__c;
    global String Wheel_Make__c;
    global Double Wheelbase_WB__c;
    global Double Width__c;
    global List<WorkOrderLineItem> WorkOrderLineItems;

    global Product2 () 
    {
    }
}