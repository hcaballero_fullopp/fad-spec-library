// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class cwbtool__Connection__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<cwbtool__Connection__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global List<cwbtool__Connection__Share> Shares;
    global Datetime SystemModstamp;
    global List<cwbtool__Connection__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global Boolean cwbtool__Current_Org__c;
    global String cwbtool__EndPoint__c;
    global String cwbtool__Enviroment__c;
    global Boolean cwbtool__Is_Active__c;
    global String cwbtool__Metadata_ServerUrl__c;
    global String cwbtool__OrgId__c;
    global String cwbtool__OrganizationName__c;
    global String cwbtool__Satge__c;
    global String cwbtool__SessionId__c;
    global Double cwbtool__Session_Time__c;
    global String cwbtool__UserEmail__c;
    global String cwbtool__UserFullName__c;
    global String cwbtool__UserId__c;
    global String cwbtool__UserName__c;
    global Double cwbtool__Valid_Second__c;

    global cwbtool__Connection__c () 
    {
    }
}