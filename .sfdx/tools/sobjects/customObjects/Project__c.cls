// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Project__c {
    global List<ActivityHistory> ActivityHistories;
    global Double Actual_Hours__c;
    global Boolean Add_l_Related_User_Subscribed__c;
    global Id Additional_Related_User__c;
    global User Additional_Related_User__r;
    global Id Assigned_To__c;
    global User Assigned_To__r;
    global Boolean Assignee_Subscribed__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Cancellation_Reason__c;
    global List<CombinedAttachment> CombinedAttachments;
    global Date Completed_Date__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global Decimal Cost_Benefit__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Current_Status__c;
    global Date Date_Cancelled__c;
    global Date Due_Date__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Decimal Estimated_Cost__c;
    global Double Estimated_Hours__c;
    global Decimal Estimated_Impact__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global Decimal Final_Estimated_Cost__c;
    global List<ContentVersion> FirstPublishLocation;
    global List<Project__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global SObject Owner;
    global Id OwnerId;
    global Boolean Owner_Subscribed__c;
    global List<Presentation__c_hd> Parent;
    global Id Parent_Project__c;
    global Project__c Parent_Project__r;
    global Double Percent_Complete__c;
    global String Primary_Department__c;
    global String Priority__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Project_Description__c;
    global List<Project__c> Projects__r;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global List<Related_ID_for_Projects__c> Related_ID_for_Projects__r;
    global Datetime SystemModstamp;
    global List<Project__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Project__c () 
    {
    }
}