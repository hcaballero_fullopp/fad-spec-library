// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Syndication_Underwriting_Review__c {
    global String Account__c;
    global List<ActivityHistory> ActivityHistories;
    global Decimal Amount_Seeking__c;
    global Decimal Approval_Amount__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Bank_Index__c;
    global Decimal Bank_Residual_Value_Per_Unit__c;
    global Double Bank_Residual_Value__c;
    global Date Bank_Response_Date__c;
    global Double Bank_Response_Spread__c;
    global Id Bank__c;
    global Account Bank__r;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Date_Received__c;
    global Date Date_Submitted_to_Bank__c;
    global String Decision__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Syndication_Underwriting_Review__History> Histories;
    global Id Id;
    global String Index__c;
    global Boolean IsDeleted;
    global Id LPAF__c;
    global Lease_Proposal_Acceptance_Form__c LPAF__r;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String Notes__c;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global Decimal Residual_Value_Per_Unit__c;
    global Double Residual_Value__c;
    global Double Spread__c;
    global Datetime SystemModstamp;
    global List<Syndication_Underwriting_Review__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Syndication_Underwriting_Review__c () 
    {
    }
}