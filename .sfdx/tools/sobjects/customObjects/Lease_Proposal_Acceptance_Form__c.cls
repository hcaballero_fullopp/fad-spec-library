// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Lease_Proposal_Acceptance_Form__c {
    global String Account__c;
    global List<ActivityHistory> ActivityHistories;
    global Double Age__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Attachment__c> Attachments__r;
    global Boolean Ballpark_Pricing_Request__c;
    global String Bonus_Depreciation__c;
    global Boolean Client_Selected_this_LPAF__c;
    global String Co_Lessee_Field_Name__c;
    global Id Co_Lessee__c;
    global Account Co_Lessee__r;
    global List<CombinedAttachment> CombinedAttachments;
    global Date Completed__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Datetime Date_Name_Verified__c;
    global String Delivery_Date_Q_Yr__c;
    global Date Due__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Date Est_Delivery__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global Double ExchangeIT_Month__c;
    global Double ExchangeIt_Term_Mos_minus_3__c;
    global Double Ext_Term_Years__c;
    global String FAID__c;
    global String Fed_Tax__c;
    global Decimal Fee_Dollar__c;
    global Double Fee__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String Guarantor_Field_Name__c;
    global String Guarantor_Formula__c;
    global Id Guarantor__c;
    global Account Guarantor__r;
    global Double HPY__c;
    global Boolean Has_Spec__c;
    global List<Lease_Proposal_Acceptance_Form__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LPAF_Approval_Date__c;
    global Date LPAF_Completed__c;
    global Date LPAF_Date__c;
    global Date LPAF_Due_Date_DDP__c;
    global Double LPAF_Quantity__c;
    global Double LRF__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Date Lease_Pricing_Approved__c;
    global Date Lease_Pricing_Complete__c;
    global Date Lease_Pricing_Due__c;
    global Double Lease_Term_Yrs_3_Months__c;
    global String Lease_Type__c;
    global String Lessee_Account__c;
    global String Lessee_Formula__c;
    global Double MPY__c;
    global String Name;
    global Boolean None_TBD__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String Notes__c;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global List<FeedComment> Parent;
    global String Payment_Option__c;
    global Decimal Payment__c;
    global Date Presented__c;
    global List<Pricing_Summary__c> Pricing_Summaries__r;
    global Id Primary_Contact__c;
    global Contact Primary_Contact__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Double Quantity__c;
    global Id Quote__c;
    global Quote Quote__r;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<ContentDistribution> RelatedRecord;
    global Date Requested__c;
    global Decimal Residual_Value_Cur__c;
    global Double Residual_Value__c;
    global Id Signatory__c;
    global Contact Signatory__r;
    global String Spec_Id__c;
    global Id Spec__c;
    global Opportunity_SubGroup__c Spec__r;
    global List<Opportunity_SubGroup__c> Specs__r;
    global Double Spread__c;
    global String Status__c;
    global String Sub_Lessee_Field_Name__c;
    global Id Sub_Lessee__c;
    global Account Sub_Lessee__r;
    global Date Swap_Date__c;
    global Double Swap_Rate__c;
    global Double Swap_Term_Years__c;
    global List<Syndication_Underwriting_Review__c> Syndication_Underwriting_Reviews__r;
    global Datetime SystemModstamp;
    global List<Lease_Proposal_Acceptance_Form__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Double Term_Mos_Formula__c;
    global Double Term_Mos__c;
    global String Title__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Total_Unit_Quantity__c;
    global String Upload_LPAF__c;
    global Double X10_BP_Yield_Adjustment_Factor__c;
    global Double Yield__c;

    global Lease_Proposal_Acceptance_Form__c () 
    {
    }
}