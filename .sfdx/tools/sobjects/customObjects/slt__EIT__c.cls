// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class slt__EIT__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<slt__EIT__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global String slt__AccountContactMatching__c;
    global Boolean slt__Active__c;
    global Id slt__Category__c;
    global slt__Category__c slt__Category__r;
    global String slt__Color__c;
    global Double slt__CustSatBest__c;
    global Boolean slt__CustSatEmptyNeutral__c;
    global String slt__CustSatFieldPath__c;
    global String slt__CustSatField__c;
    global Double slt__CustSatHalfLife__c;
    global Boolean slt__CustSatOnly__c;
    global Double slt__CustSatWeighting__c;
    global Double slt__CustSatWorst__c;
    global String slt__CustomTimelineObject__c;
    global String slt__DescriptionFieldPath__c;
    global String slt__DescriptionField__c;
    global String slt__Description__c;
    global String slt__EndTimeValuePath__c;
    global String slt__EndTimeValue__c;
    global Boolean slt__ExecuteAsSubquery__c;
    global String slt__HoverFieldPath__c;
    global String slt__HoverField__c;
    global String slt__Icon__c;
    global String slt__Image__c;
    global String slt__InclusionFieldPath__c;
    global String slt__InclusionField__c;
    global String slt__ObjectType__c;
    global Double slt__Order__c;
    global String slt__ParentAccountIDFieldPath__c;
    global String slt__ParentAccountIDField__c;
    global String slt__ParentContactIDFieldPath__c;
    global String slt__ParentContactIDField__c;
    global String slt__RecordIDFieldPath__c;
    global String slt__RecordIDField__c;
    global Double slt__RecordLimit__c;
    global String slt__RecordNameFieldPath__c;
    global String slt__RecordNameField__c;
    global String slt__RecordTypes2__c;
    global String slt__RecordTypes__c;
    global String slt__StartTimeValuePath__c;
    global String slt__StartTimeValue__c;
    global String slt__TapeImage__c;
    global String slt__TapeRepeat__c;
    global String slt__TextColor__c;

    global slt__EIT__c () 
    {
    }
}