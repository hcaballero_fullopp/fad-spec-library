// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Bank_References__c {
    global Id Account_Lookup__c;
    global Account Account_Lookup__r;
    global String Account__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Decimal Bank_EF_Exposure__c;
    global Decimal Bank_Exposure__c;
    global Id Bank_Lookup__c;
    global Account Bank_Lookup__r;
    global String Bank_Rating__c;
    global Date Bank_Reply_Date__c;
    global String Bank_Response__c;
    global String Bank__c;
    global List<CombinedAttachment> CombinedAttachments;
    global Id Contact_at_Bank__c;
    global Contact Contact_at_Bank__r;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Credit_Tracking_Record_Name__c;
    global Credit_Worthiness_Tracking__c Credit_Tracking_Record_Name__r;
    global Date Date_Reference_Requested__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Bank_References__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global SObject Owner;
    global Id OwnerId;
    global List<Presentation__c_hd> Parent;
    global String Potential_Line__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global String Syndication_Response__c;
    global Datetime SystemModstamp;
    global List<Bank_References__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Bank_References__c () 
    {
    }
}