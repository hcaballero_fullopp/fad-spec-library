// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__DDP__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Loop__AdHoc_Apex_Class__c;
    global String Loop__AddendumDocIdField__c;
    global Boolean Loop__AllowOutputAttachment__c;
    global Boolean Loop__Allow_Attachments__c;
    global Boolean Loop__Allow_UserId_Override__c;
    global String Loop__Attach_As__c;
    global String Loop__Business_User__c;
    global String Loop__Case_Articles__c;
    global String Loop__Content_Workspace__c;
    global List<Loop__DDP_Integration_Option__c> Loop__Custom_Integration_Options__r;
    global String Loop__DDP_Additional_To__c;
    global String Loop__DDP_BCC__c;
    global String Loop__DDP_CC__c;
    global List<Loop__DDP_File__c> Loop__DDP_Files__r;
    global List<Loop__DDP_Text_Group__c> Loop__DDP_Text_Groups__r;
    global String Loop__Day__c;
    global String Loop__Default_Email_Template__c;
    global String Loop__Description__c;
    global String Loop__Document_Type__c;
    global String Loop__Email_Template__c;
    global String Loop__Enclosure_Folder_Id__c;
    global Double Loop__Enclosure_Limit__c;
    global Boolean Loop__Exclude_Email__c;
    global Boolean Loop__Exclude_LOOP__c;
    global Boolean Loop__Exclude_Queue__c;
    global String Loop__Filter__c;
    global List<Loop__Form_Field__c> Loop__Form_Fields__r;
    global List<Loop__Form_Rule__c> Loop__Form_Rules__r;
    global List<Loop__Form_Step__c> Loop__Form_Steps__r;
    global String Loop__Frequency__c;
    global String Loop__Industry__c;
    global List<Loop__Insert_Update__c> Loop__Insert_Updates__r;
    global Boolean Loop__IsActive__c;
    global String Loop__KA_Optional__c;
    global String Loop__KA_Required__c;
    global String Loop__KA_Separate__c;
    global Boolean Loop__Keep_Word_Formulas__c;
    global Datetime Loop__LastSent__c;
    global String Loop__Limit_Availability__c;
    global String Loop__Limit_Deployment__c;
    global String Loop__Locale__c;
    global String Loop__MassEmailReport__c;
    global String Loop__Mass_Email_Report_Name__c;
    global String Loop__Mass_Email_Report_to__c;
    global String Loop__Object_Name_Link__c;
    global String Loop__Object_Name__c;
    global String Loop__Office_Version__c;
    global String Loop__OrgWideEmailAddress__c;
    global String Loop__OrgWideEmailId__c;
    global String Loop__OrgWideEmailName__c;
    global String Loop__Org_Wide_Email__c;
    global String Loop__Output_Filename__c;
    global String Loop__Output__c;
    global List<Loop__PDF_Stamp__c> Loop__PDF_Stamps__r;
    global String Loop__Preview_URL__c;
    global String Loop__ProcessingText__c;
    global Boolean Loop__Published__c;
    global String Loop__Queue_Folder_Id__c;
    global String Loop__Quote_Attachments__c;
    global String Loop__RelatedContent__c;
    global String Loop__RelatedObjects__c;
    global List<Loop__Related_Object__c> Loop__Related_Objects__r;
    global Boolean Loop__Remove_Invalid_Characters__c;
    global String Loop__RenameLOOP__c;
    global String Loop__ReportStatus__c;
    global Boolean Loop__RequireAttachment__c;
    global Boolean Loop__RequireContact__c;
    global Boolean Loop__RequireOutputAttachment__c;
    global Id Loop__Scheduled_Delivery_Option__c;
    global Loop__DDP_Integration_Option__c Loop__Scheduled_Delivery_Option__r;
    global String Loop__SecurityType__c;
    global String Loop__Security__c;
    global Boolean Loop__Skip_AdHoc_Processing__c;
    global String Loop__Time__c;
    global String Loop__Type__c;
    global String Loop__WorkspaceId__c;
    global String Loop__WorkspaceName__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<Loop__DDP__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__DDP__c () 
    {
    }
}