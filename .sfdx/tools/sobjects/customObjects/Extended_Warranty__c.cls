// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Extended_Warranty__c {
    global Decimal Aftertreatment_Cost__c;
    global String Aftertreatment_Limits__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Decimal Chassis_Cost__c;
    global String Chassis_Limits__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Decimal Engine_Cost__c;
    global String Engine_Limits__c;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Decimal HVAC_Cost__c;
    global String HVAC_Limits__c;
    global List<Extended_Warranty__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Id Lightning_Spec__c;
    global Quote Lightning_Spec__r;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global Decimal Reefer_Cost__c;
    global String Reefer_Limits__c;
    global List<ContentDistribution> RelatedRecord;
    global Decimal Starter_Alt_Cost__c;
    global String Starter_Alt_Limits__c;
    global Datetime SystemModstamp;
    global List<Extended_Warranty__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global Decimal Total_Warranty_Costs__c;
    global Decimal Towing_Cost__c;
    global String Towing_Limits__c;

    global Extended_Warranty__c () 
    {
    }
}