// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__SecureField__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Boolean Loop__Conceal__c;
    global List<Loop__SecureField__c> Loop__Conditional_Fields__r;
    global String Loop__Font__c;
    global String Loop__GroupName__c;
    global Double Loop__Height__c;
    global String Loop__ListItems__c;
    global Boolean Loop__Locked__c;
    global Id Loop__Recipient__c;
    global Loop__dsRecipient__c Loop__Recipient__r;
    global Boolean Loop__Required__c;
    global Boolean Loop__Selected__c;
    global String Loop__ToolTip__c;
    global String Loop__Type__c;
    global String Loop__Update_Field__c;
    global String Loop__ValidationMessage__c;
    global String Loop__ValidationPattern__c;
    global String Loop__Value__c;
    global Double Loop__Width__c;
    global Double Loop__X_Offset__c;
    global Double Loop__Y_Offset__c;
    global Id Loop__dsParentField__c;
    global Loop__SecureField__c Loop__dsParentField__r;
    global String Loop__dsParentValue__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<Loop__SecureField__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__SecureField__c () 
    {
    }
}