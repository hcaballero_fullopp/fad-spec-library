// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class HubSpot_Inc__HubSpot_Settings__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<HubSpot_Inc__HubSpot_Settings__History> Histories;
    global String HubSpot_Inc__De_duplication_Rule__c;
    global String HubSpot_Inc__HAPI_Domain__c;
    global String HubSpot_Inc__HAPI_Key__c;
    global Boolean HubSpot_Inc__Hide_Dynamic_Intelligence__c;
    global List<HubSpot_Inc__HubSpot_Field__c> HubSpot_Inc__HubSpot_Fields__r;
    global String HubSpot_Inc__Logging_Level__c;
    global String HubSpot_Inc__Portal_ID__c;
    global String HubSpot_Inc__Store_HubSpot_Activities__c;
    global Boolean HubSpot_Inc__Sync_Accounts__c;
    global Boolean HubSpot_Inc__autoadd_accounts__c;
    global Boolean HubSpot_Inc__autoadd_accounts_update__c;
    global Boolean HubSpot_Inc__autoadd_contacts__c;
    global Boolean HubSpot_Inc__autoadd_contacts_update__c;
    global Boolean HubSpot_Inc__autoadd_leads__c;
    global Boolean HubSpot_Inc__autoadd_leads_update__c;
    global String HubSpot_Inc__conversion_event_format__c;
    global String HubSpot_Inc__create_sobject_type__c;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedComment> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<HubSpot_Inc__HubSpot_Settings__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global HubSpot_Inc__HubSpot_Settings__c () 
    {
    }
}