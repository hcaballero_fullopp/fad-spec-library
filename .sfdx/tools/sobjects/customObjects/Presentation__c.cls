// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Presentation__c {
    global List<ActivityHistory> ActivityHistories;
    global Double Age__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Attachment__c> Attachments__r;
    global String BDEX__c;
    global List<CombinedAttachment> CombinedAttachments;
    global Date Completed__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Customer_Contact__c;
    global Contact Customer_Contact__r;
    global Date Date_Presented__c;
    global Date Due__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Presentation__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OnSiteCall_Presentation__c> OnSiteCall_Presentations__r;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global List<Opportunity__hd> Parent;
    global String Presentation_Requested__c;
    global Id Presented_at_Onsite_Call__c;
    global Onsite_Call__c Presented_at_Onsite_Call__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Record_Type_ID__c;
    global List<FlowRecordRelation> RelatedRecord;
    global Id Related_to_Account__c;
    global Account Related_to_Account__r;
    global Date Requested_Date__c;
    global Datetime SystemModstamp;
    global List<Presentation__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global String Upload_Presentation__c;
    global String notes__c;

    global Presentation__c () 
    {
    }
}