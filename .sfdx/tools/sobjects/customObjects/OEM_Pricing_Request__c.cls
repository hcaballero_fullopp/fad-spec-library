// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class OEM_Pricing_Request__c {
    global List<ActivityHistory> ActivityHistories;
    global Decimal Adjusted_List_Price__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Id Awarded_Opportunity_Group__c;
    global Awarded_Opportunity_Group__c Awarded_Opportunity_Group__r;
    global Id BDEX__c;
    global User BDEX__r;
    global List<CombinedAttachment> CombinedAttachments;
    global Decimal Concession__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Date_Received__c;
    global Date Date_Received_from_Dealer__c;
    global Date Date_Sent_to_Dealer__c;
    global String Dealer__c;
    global Double Discount_off_List_Price_After_Rebates__c;
    global Double Discount_off_List_Price__c;
    global Date Due_Date__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global String Engine_Description__c;
    global Double Engine_Limit_Miles__c;
    global Double Engine_Limit_Years__c;
    global Decimal Engine_Warranty__c;
    global Double Engine_w_EATS_Miles__c;
    global Decimal Engine_w_EATS_Warranty__c;
    global Double Engine_w_EATS_Years__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global String Extended_Warranty__c;
    global Decimal FET__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Double HVAC_Limit_Miles__c;
    global Double HVAC_Limit_Years__c;
    global Decimal HVAC_Warranty__c;
    global Decimal Hardware_Install_Cost_Per_Unit__c;
    global List<OEM_Pricing_Request__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Double Misc_Limit_Miles__c;
    global Double Misc_Limit_Years__c;
    global Decimal Misc_Warranty__c;
    global String Model__c;
    global Decimal Monthly_Service_Cost_Per_Unit__c;
    global String Name;
    global Decimal Net_Price_excl_FET__c;
    global Decimal Net_Price_w_FET__c;
    global Decimal Net_Price_w_FET_and_After_Rebates__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String OBC_Brand__c;
    global String OEM_Make__c;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedComment> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Double Quantity__c;
    global Decimal Rebate_Manufacturer__c;
    global Decimal Rebate_OEM__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global Date Request_Date__c;
    global List<OEM_Pricing_Request__Share> Shares;
    global Date Spec_Approval_Date__c;
    global Id Spec_Approved_By__c;
    global User Spec_Approved_By__r;
    global String Spec_Provided_By__c;
    global String Special_Instructions__c;
    global Double Start_Alt_Limit_Miles__c;
    global Double Start_Alt_Limit_Years__c;
    global Decimal Starter_Alt_Warranty__c;
    global Decimal Sub_Total__c;
    global Datetime SystemModstamp;
    global List<OEM_Pricing_Request__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global Decimal Total_Rebates__c;
    global Decimal Total__c;
    global String Type__c;
    global Decimal Warranty_Sub_Total__c;
    global String Year__c;

    global OEM_Pricing_Request__c () 
    {
    }
}