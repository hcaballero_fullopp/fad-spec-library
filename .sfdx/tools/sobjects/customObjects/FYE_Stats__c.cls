// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class FYE_Stats__c {
    global Double AT_Return_of_Equity__c;
    global String Account__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Credit_Worthiness_Record__c;
    global Credit_Worthiness_Tracking__c Credit_Worthiness_Record__r;
    global Decimal Current_Assets__c;
    global Decimal Current_Liabilities__c;
    global Double Current_Ratio__c;
    global Double Debt_Equity__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Decimal Equity__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global Date FYE__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Decimal Goodwill__c;
    global List<FYE_Stats__History> Histories;
    global Id Id;
    global Decimal Intangible_Assets__c;
    global Decimal Interest_Expense__c;
    global Boolean IsDeleted;
    global Double LT_Debt_Equity__c;
    global Double LT_Debt_Tangible_Equity__c;
    global Decimal LT_Debt__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Decimal Long__c;
    global Decimal NIBT__c;
    global Decimal NI__c;
    global String Name;
    global Decimal Net_Income_After_Tax__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Decimal Operating_Income__c;
    global SObject Owner;
    global Id OwnerId;
    global List<Opportunity__hd> Parent;
    global Decimal Preferred__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global Decimal Revenue__c;
    global Decimal ST_LT_Debt__c;
    global Decimal Short_Term_Debt__c;
    global Datetime SystemModstamp;
    global Decimal TNW__c;
    global List<FYE_Stats__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Double Tax_Rate__c;
    global Decimal Tax__c;
    global List<TopicAssignment> TopicAssignments;
    global Decimal Total_Assets__c;
    global Decimal Total_Equity__c;
    global Decimal Total_Liabilities_Equity__c;
    global Decimal Total_Liabilities__c;
    global Double Weighted_Average_Cost_of_Debt_Pre_Tax__c;

    global FYE_Stats__c () 
    {
    }
}