// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__LoopSettings__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Boolean Loop__Decryption_Allowed__c;
    global String Loop__DocuSign_Account_Ids__c;
    global String Loop__DocuSign_Account_Names__c;
    global String Loop__DocuSign_User_Ids__c;
    global Boolean Loop__Job_Queue_Status_Enabled__c;
    global String Loop__Pause_to_Edit_Type__c;
    global Boolean Loop__Sandbox__c;
    global Boolean Loop__Show_DDP_Wizard_Message__c;
    global Boolean Loop__Show_Edit_Form_Message__c;
    global Boolean Loop__Store_Attachments_As_Salesforce_Files__c;
    global String Loop__Veeva_Vault_Subdomain__c;
    global String Name;
    global List<Presentation__c_hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global SObject SetupOwner;
    global Id SetupOwnerId;
    global Datetime SystemModstamp;
    global List<Loop__LoopSettings__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__LoopSettings__c () 
    {
    }
}