// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__DDP_Integration_Option__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Loop__Agreement_Name__c;
    global Boolean Loop__AllowOutputAttachment__c;
    global String Loop__Attach_As__c;
    global String Loop__Authoritative_Copy__c;
    global String Loop__Brand_Id__c;
    global String Loop__CC__c;
    global String Loop__CI_Additional_To__c;
    global String Loop__CI_BCC__c;
    global String Loop__CI_CC__c;
    global String Loop__Content_Workspace__c;
    global String Loop__DDP_File_Template__c;
    global Id Loop__DDP__c;
    global Loop__DDP__c Loop__DDP__r;
    global String Loop__Data_Mapping_Id__c;
    global String Loop__Data_Mapping_Name__c;
    global String Loop__Data_Mapping__c;
    global Boolean Loop__Decryption_Required__c;
    global String Loop__Delivery_Data__c;
    global String Loop__Description_Hover__c;
    global List<Loop__DDP__c> Loop__Document_Packages__r;
    global Boolean Loop__Email_Doc_Links__c;
    global String Loop__Email_Template__c;
    global Boolean Loop__Enable_Revisions__c;
    global Boolean Loop__EnforceSignerVisibility__c;
    global String Loop__FTPDomain__c;
    global String Loop__FTPUserName__c;
    global String Loop__FTP__c;
    global Boolean Loop__HTML_Email__c;
    global Boolean Loop__Hosted_Signing__c;
    global Boolean Loop__Internal_Email__c;
    global String Loop__Language__c;
    global String Loop__Limit_Availability__c;
    global String Loop__Location__c;
    global String Loop__Message__c;
    global Double Loop__Order__c;
    global String Loop__OrgWideEmailAddress__c;
    global String Loop__OrgWideEmailId__c;
    global String Loop__OrgWideEmailName__c;
    global String Loop__Org_Wide_Email__c;
    global String Loop__Output__c;
    global String Loop__PDF_Option__c;
    global String Loop__Password__c;
    global Boolean Loop__PasswordonPDF__c;
    global Boolean Loop__PasswordtoSign__c;
    global String Loop__PostParameters__c;
    global String Loop__Preview_Step_Help_Text__c;
    global String Loop__Queue_Folder_Id__c;
    global List<Loop__dsRecipient__c> Loop__Recipients__r;
    global String Loop__Redirect_URL__c;
    global String Loop__Remind_Recipient__c;
    global Boolean Loop__RequireOutputAttachment__c;
    global String Loop__SMTPDomain__c;
    global String Loop__SMTPUserName__c;
    global String Loop__SMTP_Password__c;
    global Boolean Loop__SSL__c;
    global Boolean Loop__SenderSigns__c;
    global String Loop__Signature_Order__c;
    global String Loop__Signature_Type__c;
    global Boolean Loop__SkipPreview__c;
    global String Loop__Storage_Data__c;
    global String Loop__Storage_Folder__c;
    global String Loop__Storage_Location__c;
    global Boolean Loop__Tag_Before_Sending__c;
    global String Loop__Template__c;
    global Boolean Loop__TurnTrackingOn__c;
    global String Loop__Type__c;
    global Boolean Loop__Wait__c;
    global String Loop__WorkspaceId__c;
    global String Loop__WorkspaceName__c;
    global Boolean Loop__dsAllowEmailCustomizations__c;
    global String Loop__dsCustomFields__c;
    global String Loop__dsEmailText__c;
    global Double Loop__dsExpireAfter__c;
    global Double Loop__dsExpireWarn__c;
    global Boolean Loop__dsExposeExpirations__c;
    global Double Loop__dsReminderDelay__c;
    global Double Loop__dsReminderFrequency__c;
    global String Loop__dsSubject__c;
    global String Loop__dsTemplateId__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<FlowRecordRelation> RelatedRecord;
    global Datetime SystemModstamp;
    global List<Loop__DDP_Integration_Option__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__DDP_Integration_Option__c () 
    {
    }
}