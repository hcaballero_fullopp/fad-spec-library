// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Resale_Prospect__c {
    global List<ActivityHistory> ActivityHistories;
    global String Address_2__c;
    global String Address__c;
    global Double AnnualUnitMilesReported__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double AverageUnitMilesPerYear__c;
    global Decimal CalculatedRevenue__c;
    global String City__c;
    global Double ClientId__c;
    global Double ClientTypeID__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global String Country__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Double DaycabCount__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global Double FIPS__c;
    global String Fax__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String FleetDescription__c;
    global Double FleetMPY__c;
    global String FleetOwnershipTypeId__c;
    global Double FleetSeekId__c;
    global Double FleetSize__c;
    global Double FleetUseType__c;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Main_Email__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global SObject Owner;
    global Id OwnerId;
    global List<Opportunity__hd> Parent;
    global String Phone_Number__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Decimal ReportedRevenue__c;
    global String SICCodeGroupNomenclature__c;
    global Double SICCodeGroup__c;
    global String SICCodeNomenclature__c;
    global Double SICCode__c;
    global Double SleepCount__c;
    global String State__c;
    global Datetime SystemModstamp;
    global List<Resale_Prospect__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global String Zipcode__c;

    global Resale_Prospect__c () 
    {
    }
}