// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Client_Intel_Duty_Cycle__c {
    global Id Account__c;
    global Account Account__r;
    global List<ActivityHistory> ActivityHistories;
    global String Address__c;
    global String Aftertreatment_Limits__c;
    global String Aftertreatment__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Average_Driving_MPG__c;
    global Double Average_Overall_MPG__c;
    global Double Average_Power_Miles__c;
    global String Average_Unit_Downtime_month__c;
    global Double Average_Weight_Load_lbs__c;
    global String Breakdown_Frequency__c;
    global String Campaigns__c;
    global String City__c;
    global List<CombinedAttachment> CombinedAttachments;
    global String Company__c;
    global Id Contact__c;
    global Contact Contact__r;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Current_Lifecycle_SADC__c;
    global String Current_Lifecycle_Sleepers__c;
    global String Current_Lifecycle_Straight_Trucks__c;
    global String Current_Lifecycle_TADC__c;
    global Double Daily_Miles_SADC__c;
    global Double Daily_Miles_Sleepers__c;
    global Double Daily_Miles_Straight_Trucks__c;
    global Double Daily_Miles_TADC__c;
    global Date Date__c;
    global String Description_of_Routes__c;
    global String Description_of_Terrain__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global String End_Times__c;
    global String Engine_Limits__c;
    global String Engine__c;
    global String Equipment_Type_SADC__c;
    global String Equipment_Type_Sleepers__c;
    global String Equipment_Type_Straight_Trucks__c;
    global String Equipment_Type_TADC__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String HVAC_Limits__c;
    global String HVAC__c;
    global Id Id;
    global Double Inbound_backhaul__c;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Decimal Maintenance_Costs__c;
    global String Maintenance_Provider__c;
    global String Maintenance_Software__c;
    global Double Maximum_Road_Speed__c;
    global String Mechanics_or_Technicians__c;
    global String Name;
    global Double Non_Power_Units_Average_Age_Yrs__c;
    global Double Non_Power_Units_Leased__c;
    global Double Non_Power_Units_Owned__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String OBC__c;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global String Other_Limits__c;
    global String Other__c;
    global Double Outbound_lbs__c;
    global String PMI_Interval__c;
    global List<Presentation__c_hd> Parent;
    global Double Power_Units_Average_Age_Yrs__c;
    global Double Power_Units_Leased__c;
    global Double Power_Units_Owned__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global String Skill_Training_Level__c;
    global String Start_Alternator_Limits__c;
    global String Start_Alternator__c;
    global String Start_Times__c;
    global String State__c;
    global Datetime SystemModstamp;
    global List<Client_Intel_Duty_Cycle__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global String Towing_Limits__c;
    global String Towing__c;
    global String Utilization_SADC__c;
    global String Utilization_Sleepers__c;
    global String Utilization_Straight_Trucks__c;
    global String Utilization_TADC__c;
    global String Work_Week__c;
    global Decimal Year_10__c;
    global Decimal Year_11__c;
    global Decimal Year_12__c;
    global Decimal Year_1__c;
    global Decimal Year_2__c;
    global Decimal Year_3__c;
    global Decimal Year_4__c;
    global Decimal Year_5__c;
    global Decimal Year_6__c;
    global Decimal Year_7__c;
    global Decimal Year_8__c;
    global Decimal Year_9__c;
    global String ZIP__c;
    global Double of_Backhaul__c;

    global Client_Intel_Duty_Cycle__c () 
    {
    }
}