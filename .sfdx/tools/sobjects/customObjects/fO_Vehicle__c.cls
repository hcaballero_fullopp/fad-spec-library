// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class fO_Vehicle__c {
    global Date Accepted_Delivery_Date__c;
    global Id Account__c;
    global Account Account__r;
    global List<ActivityHistory> ActivityHistories;
    global Date Actual_Delivery_Date__c;
    global String Asset_ID__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Bank_ID__c;
    global String Bank_Short_Name__c;
    global Date Build_Date__c;
    global List<fO_Child_Assets__c> Child_Assets__r;
    global String City__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Double Days_2_Delivery__c;
    global Double Days_Overdue__c;
    global String Draw_Down_ID__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Date Estimated_Delivery_Date__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id FleetZip__c;
    global FleetZip__c FleetZip__r;
    global String Got_MSO__c;
    global Id Group__c;
    global fO_Group__c Group__r;
    global Id Id;
    global Date In_Service_Date__c;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Legacy_FAID__c;
    global String Location_Name__c;
    global Date MSO_Copy_Received__c;
    global Date MSO_Received__c;
    global String Make__c;
    global String Manufactured_Year__c;
    global String Model__c;
    global String Name;
    global String Normalized_ZIP_Code__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global Date Off_Lease_Date__c;
    global List<OpenActivity> OpenActivities;
    global List<FeedComment> Parent;
    global String Portfolio_Number__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Date Purchase_Order_Date__c;
    global String Purchase_Order_Number__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global Date Report_End_Date__c;
    global Date Report_Start_Date__c;
    global Id Schedule__c;
    global fO_Schedule__c Schedule__r;
    global String State__c;
    global String Status__c;
    global Datetime SystemModstamp;
    global List<fO_Vehicle__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global String Type__c;
    global Decimal Unit_Cost__c;
    global Boolean Unit_Doc_Accepted__c;
    global String Unit_Number__c;
    global Double VIN_Count__c;
    global String VIN__c;
    global String Vendor_ID__c;
    global String ZIP_Code__c;

    global fO_Vehicle__c () 
    {
    }
}