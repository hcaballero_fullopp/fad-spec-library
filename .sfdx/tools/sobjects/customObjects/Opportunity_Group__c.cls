// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Opportunity_Group__c {
    global List<ActivityHistory> ActivityHistories;
    global Double Age__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String BDEX__c;
    global List<CombinedAttachment> CombinedAttachments;
    global Date Completed__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Decimal Equipment_Pricing_Total__c;
    global String Equipment_Type__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global String Extended_Warranty__c;
    global String FAID_Sequential__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Opportunity_Group__Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global Decimal Group_Total__c;
    global List<Opportunity_Group__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global List<Quote> LightningSpecs__r;
    global String MPG_Spec__c;
    global String Make__c;
    global String Model_Desc__c;
    global String Model_Year__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String OBC_Pricing__c;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity_SubGroup__c> Opportunity_SubGroups__r;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global String Other__c;
    global List<FeedItem> Parent;
    global Decimal Potential_Deal_Size__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Double Quantity__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<FlowRecordRelation> RelatedRecord;
    global String Request_Opportunity_Spec__c;
    global Date Requested__c;
    global Id Spec_Library__c;
    global Opportunity_SubGroup__c Spec_Library__r;
    global String Spec_Type__c;
    global String Special_Instructions__c;
    global String Sub_Type__c;
    global Datetime SystemModstamp;
    global List<Opportunity_Group__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Opportunity_Group__c () 
    {
    }
}