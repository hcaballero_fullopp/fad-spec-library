// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__Related_Object__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Loop__Advanced_Filter_Conditions__c;
    global String Loop__Copy_Type__c;
    global Id Loop__DDP__c;
    global Loop__DDP__c Loop__DDP__r;
    global Boolean Loop__Delete_Table__c;
    global String Loop__Filter_Conditions__c;
    global String Loop__Group_By_Fields__c;
    global String Loop__Group_By_SortOrders__c;
    global String Loop__Hierarchy_Field__c;
    global Double Loop__Index__c;
    global String Loop__Order_By__c;
    global String Loop__Parent_Object_Field__c;
    global String Loop__Parent_Object__c;
    global Id Loop__Parent_Relationship__c;
    global Loop__Related_Object__c Loop__Parent_Relationship__r;
    global String Loop__Parent__c;
    global Double Loop__Record_Limit__c;
    global String Loop__Related_Object_Alias__c;
    global List<Loop__Related_Object__c> Loop__Relationships__r;
    global String Loop__SOQL__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<Loop__Related_Object__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__Related_Object__c () 
    {
    }
}