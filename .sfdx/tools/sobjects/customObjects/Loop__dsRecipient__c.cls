// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__dsRecipient__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Loop__Address__c;
    global String Loop__Calc_Signing_Order__c;
    global String Loop__CustomAccessCode__c;
    global Boolean Loop__DisplayTitle__c;
    global String Loop__EmailText__c;
    global String Loop__Embedded_Signer__c;
    global String Loop__Host_Address__c;
    global Id Loop__Host_Contact__c;
    global Contact Loop__Host_Contact__r;
    global String Loop__Host_Role_Group_Names__c;
    global String Loop__Host_Static_Email__c;
    global String Loop__Host_Static_Name__c;
    global Id Loop__Host_User__c;
    global User Loop__Host_User__r;
    global Id Loop__IntegrationOption__c;
    global Loop__DDP_Integration_Option__c Loop__IntegrationOption__r;
    global String Loop__Language__c;
    global String Loop__OnAccessCodeFailed__c;
    global String Loop__OnCancel__c;
    global String Loop__OnDecline__c;
    global String Loop__OnException__c;
    global String Loop__OnIdCheckFailed__c;
    global String Loop__OnSessionTimeout__c;
    global String Loop__OnSigningComplete__c;
    global String Loop__OnTTLExpired__c;
    global String Loop__OnViewingComplete__c;
    global String Loop__RoleGroupNames__c;
    global List<Loop__SecureField__c> Loop__Secure_Fields__r;
    global String Loop__SigningGroupId__c;
    global String Loop__SigningGroupName__c;
    global Double Loop__SigningOrder__c;
    global String Loop__Specified_Host__c;
    global String Loop__StaticEmail__c;
    global String Loop__StaticName__c;
    global String Loop__Subject__c;
    global Boolean Loop__dsAllowAccessCode__c;
    global Boolean Loop__dsCheckId__c;
    global Id Loop__dsContact__c;
    global Contact Loop__dsContact__r;
    global Boolean Loop__dsRequired__c;
    global String Loop__dsRoleName__c;
    global Double Loop__dsRoutingOrder__c;
    global String Loop__dsSignOptions__c;
    global String Loop__dsStaticRecipient__c;
    global String Loop__dsType__c;
    global Id Loop__dsUser__c;
    global User Loop__dsUser__r;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<Loop__dsRecipient__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__dsRecipient__c () 
    {
    }
}