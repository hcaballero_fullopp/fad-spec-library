// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Onsite_Call__c {
    global String Account_Type__c;
    global Id Account__c;
    global Account Account__r;
    global List<Account> Accounts__r;
    global String Activity_Type__c;
    global String Agenda_Focus__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Attendees__c> Attendees__r;
    global String BDEX__c;
    global Id Campaign__c;
    global Campaign Campaign__r;
    global Double Client_Attendees_VP_or_Higher__c;
    global List<CombinedAttachment> CombinedAttachments;
    global Id Completed_by__c;
    global User Completed_by__r;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Date_of_Onsite__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<Onsite_Note_Email__c> Emails__r;
    global Datetime End_Date__c;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Onsite_Call__Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global List<Onsite_Call__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Boolean Last_Completed_Interaction_on_Account__c;
    global String Location__c;
    global String Meeting_Type__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String Notes__c;
    global List<OnSiteCall_Presentation__c> OnSiteCall_Presentations__r;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global List<Opportunity__hd> Parent;
    global List<Presentation__c> Presentations__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global String Stage__c;
    global Datetime Start_Date__c;
    global String Subject_or_Keywords__c;
    global Datetime SystemModstamp;
    global List<Onsite_Call__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<Value_Added_Activity__c> Value_Added_Activities__r;

    global Onsite_Call__c () 
    {
    }
}