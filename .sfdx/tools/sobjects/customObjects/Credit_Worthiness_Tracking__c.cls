// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Credit_Worthiness_Tracking__c {
    global Double AT_Return_of_Equity__c;
    global Id Account__c;
    global Account Account__r;
    global List<ActivityHistory> ActivityHistories;
    global String Analyst_Comments__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Background__c;
    global Id Bank_1__c;
    global Account Bank_1__r;
    global List<Bank_References__c> Bank_References__r;
    global List<CombinedAttachment> CombinedAttachments;
    global Date Comments_Date__c;
    global Date Completed_Date__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Credit_Approval_Status__c;
    global Decimal Credit_Line__c;
    global Decimal Current_Assets__c;
    global Decimal Current_Exposure__c;
    global Decimal Current_Liabilities__c;
    global Double Current_Ratio__c;
    global Double Debt_Equity__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global String FA_Credit_Rating__c;
    global Date FA_Rating_Date__c;
    global List<FYE_Stats__c> FYE_Stats__r;
    global Date FYE__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global String Financial_Review_Comments__c;
    global Date Financial_Review_Date__c;
    global List<ContentVersion> FirstPublishLocation;
    global Decimal Goodwill_Intangible__c;
    global List<Credit_Worthiness_Tracking__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Boolean Is_Last_Credit_Tracking__c;
    global String Item_Reviewed__c;
    global String Key_Metrics__c;
    global Double LT_Debt_Equity__c;
    global Double LT_Debt_Tangible_Equity__c;
    global Decimal LT_Debt__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Date Moody_s_As_of_Date__c;
    global String Moody_s_Rating__c;
    global Decimal NIBT__c;
    global Decimal NI__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity__hd> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Rating_Comments__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global Decimal Revenue__c;
    global Date S_P_As_of_Date__c;
    global String S_P_Rating__c;
    global String Stock_Ticker__c;
    global Datetime SystemModstamp;
    global Decimal TNW__c;
    global List<Credit_Worthiness_Tracking__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Double Tax_Rate__c;
    global Decimal Tax__c;
    global List<TopicAssignment> TopicAssignments;
    global Decimal Total_Equity__c;

    global Credit_Worthiness_Tracking__c () 
    {
    }
}