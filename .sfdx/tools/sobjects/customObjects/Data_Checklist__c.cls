// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Data_Checklist__c {
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Date Cab_Type_Identified__c;
    global String Cab_Type_Notes__c;
    global Date Checklist_Complete__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Date_Received__c;
    global String Depreciation_Rent_Notes__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Data_Checklist__Feed> Feeds;
    global Boolean Financial_Data_Expected__c;
    global String Financial_Data_Notes__c;
    global Date Financial_Data_Received_Date__c;
    global List<ContentVersion> FirstPublishLocation;
    global String Fleet_List_Notes__c;
    global Datetime Fleet_List_Received_Date__c;
    global Id Fleet_List_Received_by__c;
    global User Fleet_List_Received_by__r;
    global List<Data_Checklist__History> Histories;
    global Date ISD_Identified__c;
    global String ISD_Notes__c;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Date Location_Identified__c;
    global String Location_Notes__c;
    global String M_R_Costs_Notes__c;
    global String M_R_Date_of_Record_Notes__c;
    global String M_R_Notes__c;
    global Date M_R_Received_Date__c;
    global String M_R_VMRS_Notes__c;
    global Boolean Maintenance_Repair_Data_Expected__c;
    global String Name;
    global String Net_Book_Value_Notes__c;
    global Decimal New_DC_OEC__c;
    global Date New_ISD_Estimate__c;
    global Double New_Lease_Term_Mos__c;
    global Double New_MPY_Allowance__c;
    global Decimal New_Sleeper_OEC__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String OBC_Company_ID_Name__c;
    global String OBC_Login_ID__c;
    global String OBC_Notes__c;
    global String OBC_Password__c;
    global String OBC__c;
    global String OEC_Notes__c;
    global String Odometer_Notes__c;
    global Date Odometer_Received__c;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity__hd> Parent;
    global Date Portal_Load_Date__c;
    global Boolean Portal_Load_Expected__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<FlowRecordRelation> RelatedRecord;
    global Id Related_To_Opportunity__c;
    global Opportunity Related_To_Opportunity__r;
    global Id Related_to_Account__c;
    global Account Related_to_Account__r;
    global String Stage__c;
    global Datetime SystemModstamp;
    global List<Data_Checklist__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global String Term_Notes__c;
    global List<TopicAssignment> TopicAssignments;
    global Date Unit_Number_Identified__c;
    global String Unit_Number_Notes__c;
    global Date VINS_Decoded__c;
    global Date VIN_Identified__c;

    global Data_Checklist__c () 
    {
    }
}