// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class HubSpot_Inc__HubSpot_Intelligence__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Double HubSpot_Inc__Average_Page_Views__c;
    global String HubSpot_Inc__Changes__c;
    global Id HubSpot_Inc__Contact__c;
    global Contact HubSpot_Inc__Contact__r;
    global List<Contact> HubSpot_Inc__Contacts__r;
    global Double HubSpot_Inc__Conversion_Events__c;
    global String HubSpot_Inc__Error_Message__c;
    global Datetime HubSpot_Inc__First_Conversion_Date__c;
    global String HubSpot_Inc__First_Conversion_Event__c;
    global Datetime HubSpot_Inc__First_Visit__c;
    global String HubSpot_Inc__Found_Site_Via__c;
    global String HubSpot_Inc__GUID__c;
    global Boolean HubSpot_Inc__Has_Changes__c;
    global List<HubSpot_Inc__HubSpot_Activity__c> HubSpot_Inc__HubSpot_Activities__r;
    global String HubSpot_Inc__HubSpot_Detail__c;
    global String HubSpot_Inc__IP_Address__c;
    global String HubSpot_Inc__IP_City__c;
    global String HubSpot_Inc__IP_Country__c;
    global String HubSpot_Inc__IP_Domain__c;
    global String HubSpot_Inc__IP_ISP__c;
    global String HubSpot_Inc__IP_Latitude__c;
    global String HubSpot_Inc__IP_Longitude__c;
    global String HubSpot_Inc__IP_Region__c;
    global String HubSpot_Inc__IP_Time_Zone__c;
    global String HubSpot_Inc__IP_Zip_Code__c;
    global String HubSpot_Inc__Incoming_Changes__c;
    global Double HubSpot_Inc__Lead_Grade__c;
    global Id HubSpot_Inc__Lead__c;
    global Lead HubSpot_Inc__Lead__r;
    global List<Lead> HubSpot_Inc__Leads__r;
    global String HubSpot_Inc__Portal_ID__c;
    global Datetime HubSpot_Inc__Recent_Conversion_Date__c;
    global String HubSpot_Inc__Recent_Conversion_Event__c;
    global Datetime HubSpot_Inc__Recent_Visit__c;
    global String HubSpot_Inc__Timezone__c;
    global Double HubSpot_Inc__Total_Page_Views__c;
    global Double HubSpot_Inc__Unique_Pages_Viewed__c;
    global Double HubSpot_Inc__Website_Visits__c;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<HubSpot_Inc__HubSpot_Intelligence__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global HubSpot_Inc__HubSpot_Intelligence__c () 
    {
    }
}