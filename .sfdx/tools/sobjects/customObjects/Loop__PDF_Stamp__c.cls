// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Loop__PDF_Stamp__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Loop__Background_Color__c;
    global Double Loop__Background_Padding__c;
    global String Loop__DDP_Files__c;
    global Id Loop__DDP__c;
    global Loop__DDP__c Loop__DDP__r;
    global String Loop__Delivery_Methods__c;
    global String Loop__Description__c;
    global Double Loop__FontSize__c;
    global String Loop__Font__c;
    global String Loop__HAlign__c;
    global Double Loop__Height__c;
    global Double Loop__Opacity__c;
    global Double Loop__Order__c;
    global String Loop__Pages__c;
    global String Loop__RGB_Color__c;
    global Double Loop__Rotation__c;
    global String Loop__Scale_By__c;
    global String Loop__Stamp_Text__c;
    global String Loop__Style__c;
    global String Loop__Type__c;
    global String Loop__VAlign__c;
    global Double Loop__Width__c;
    global Double Loop__XOffset__c;
    global Double Loop__YOffset__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<Loop__PDF_Stamp__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global Loop__PDF_Stamp__c () 
    {
    }
}