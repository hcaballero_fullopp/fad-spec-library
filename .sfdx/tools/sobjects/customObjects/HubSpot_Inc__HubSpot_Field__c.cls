// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class HubSpot_Inc__HubSpot_Field__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<HubSpot_Inc__HubSpot_Field__History> Histories;
    global String HubSpot_Inc__Allow_HubSpot_Updates__c;
    global Boolean HubSpot_Inc__Custom_Field__c;
    global String HubSpot_Inc__Field_Format__c;
    global String HubSpot_Inc__Force_com_Field_Name__c;
    global String HubSpot_Inc__HubSpot_Field_Name__c;
    global Id HubSpot_Inc__HubSpot_Settings__c;
    global HubSpot_Inc__HubSpot_Settings__c HubSpot_Inc__HubSpot_Settings__r;
    global String HubSpot_Inc__Map_Object_Type__c;
    global Boolean HubSpot_Inc__Sync_Disabled__c;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Datetime SystemModstamp;
    global List<HubSpot_Inc__HubSpot_Field__Tag> Tags;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;

    global HubSpot_Inc__HubSpot_Field__c () 
    {
    }
}