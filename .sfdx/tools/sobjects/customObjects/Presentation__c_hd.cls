// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Presentation__c_hd {
    global Date Completed__c_hpr;
    global Date Completed__c_hst;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Due__c_hpr;
    global Date Due__c_hst;
    global Id Id;
    global Boolean IsDeleted;
    global Presentation__c Parent;
    global Id ParentId;
    global String Presentation_Requested__c_hpr;
    global String Presentation_Requested__c_hst;
    global Datetime SystemModstamp;
    global Datetime ValidFromDate;
    global Datetime ValidToDate;

    global Presentation__c_hd () 
    {
    }
}