public with sharing class LeadController {
	
	public Lead lead {get; set;}
	public String minTotalVehicles {get; set;}
	public String maxTotalVehicles {get; set;}
	public Lead[] leads {get; set;}
		
	public LeadController() {
		clear();
	}
	
	private String addFilter(String query, String field) {
		String value = String.valueOf(lead.get(field));
		if (value == null || value == '')
			return query;
			if (!query.contains(' WHERE '))
			query += ' WHERE ';
			else
			query += ' AND ';
			query += field + ' LIKE \'%' + value + '%\'';
			return query;
	}
	
	private String addFilter(String query, String field, String operator, String value) {
		if (value == null || value == '')
			return query;
			if (!query.contains(' WHERE '))
			query += ' WHERE ';
			else
			query += ' AND ';
			query += field + ' ' + operator + ' ' + value;
			return query;
	}
	
	public String query {get; set;} 
	
	public System.Pagereference search() {
		Id remarketingId = Schema.Sobjecttype.Lead.getRecordTypeInfosByName().get('Remarketing').getRecordTypeId();
		this.query = 'SELECT Id, Name, LastActivityDate, Phone, Total_Vehicles__c, Company FROM Lead WHERE RecordTypeId = \'' + remarketingId + '\'';
		this.query = addFilter(query, 'Company');
		this.query = addFilter(query, 'Client_Type__c');
		this.query = addFilter(query, 'FirstName');
		this.query = addFilter(query, 'LastName');
this.query = addFilter(query, 'State_Name__c');
this.query = addFilter(query, 'PostalCode');
this.query = addFilter(query, 'Total_Vehicles__c', '>=', minTotalVehicles);
this.query = addFilter(query, 'Total_Vehicles__c', '<=', maxTotalVehicles);
this.query = addFilter(query, 'SIC_Code_Group__c');
this.query = addFilter(query, 'Unit_Type__c');
this.query = addFilter(query, 'Trailer_Type__c');
		this.leads = (Lead[])Database.query(query);
		return null; 
	}
	
	public System.Pagereference clear() {
		this.lead = new Lead();
		minTotalVehicles = null;
		maxTotalVehicles = null;
		search();
		return null;
	}

}