public with sharing class RequestLPAFController {
	
	public class RequestLPAFException extends Exception {}
	
	public Opportunity opportunity {get; set;}
	public Lease_Proposal_Acceptance_Form__c lpaf {get; set;}
	public List<Opportunity_SubGroup__c> specs {get; set;}
	//public Map<Id, Lease_Proposal_Acceptance_Form__c> lpafMap {get; set;}
	public String debug {get; set;}
	
	public RequestLPAFController(ApexPages.StandardController controller) {
		if (!Test.isRunningTest())
		controller.addFields(new String[] {});
		this.opportunity = (Opportunity)controller.getRecord();
		this.lpaf = new Lease_Proposal_Acceptance_Form__c();
		this.lpaf.Opportunity__c = opportunity.Id;
		initSpecs();
		String bpr = System.currentPageReference().getParameters().get('bpr');
		this.lpaf.Ballpark_Pricing_Request__c = bpr == '1';
		//this.debug = '<br/>Lista al inicio: ' + specs.size();
	}
	
	public void initSpecs() 
	{
		String query = 'SELECT Id, Select_This_Spec_for_LPAF__c, LPAF_Request__c';
		for ( Schema.FieldSetMember member : getLabelSpecFields()) {
			query += ', ' + member.getFieldPath();
		}
		
		for ( Schema.FieldSetMember member : getInputSpecFields()) {
			//query += ', LPAF_Request__r.' + member.getFieldPath();
			query += ', ' + member.getFieldPath();
		}
		query += ', LPAF_Request__r.Id FROM Opportunity_SubGroup__c WHERE Opportunity_Group__r.Opportunity__c = \'' + opportunity.Id + '\'';
		this.specs = (Opportunity_SubGroup__c[])Database.query(query);
		/**
		for (Opportunity_SubGroup__c spec : specs) {
			for ( Schema.FieldSetMember member : getInputSpecFields()) {
				Lease_Proposal_Acceptance_Form__c lpaf1 = spec.LPAF_Request__r;
				if (lpaf1 == null)
				continue;
				String field = member.getFieldPath();
				Object value = lpaf1.get(field);
				spec.put(member.getFieldPath(), value);
			}
		}
		*/
		//initLPAFMap();
		//System.assertNotEquals(0, specs.size(), 'No hay specs');
			}
	

	public List<Schema.FieldSetMember> getFields(Schema.SobjectType stype, String fieldSetName) {
		Schema.Describesobjectresult result = stype.getDescribe();
		Map<String, Schema.FieldSet>  fieldSetMap = result.fieldSets.getMap();
		Schema.FieldSet fs = fieldSetMap.get(fieldSetName);
		if (fs == null)
		throw new RequestLPAFException('The fieldset: ' + fieldSetName + ' is required for ' + stype.getDescribe().getName());
		return fs.getFields();
	}
	
	public List<Schema.FieldSetMember> getLabelSpecFields() {
		return getFields(Opportunity_SubGroup__c.getSobjectType(), 'LPAF_Spec_Read_Only');
	}
	
	public List<Schema.FieldSetMember> getInputSpecFields() {
		return getFields(Opportunity_SubGroup__c.getSobjectType(), 'LPAF_Spec_Inputs');
	}
	
	public List<Schema.FieldSetMember> getInputLPAFFields() {
		return getFields(Lease_Proposal_Acceptance_Form__c.getSobjectType(), 'LPAF_Request');
	}
	
    public List<Schema.FieldSetMember> getInputLPAFFieldsOptional() {
		return getFields(Lease_Proposal_Acceptance_Form__c.getSobjectType(), 'LPAF_Request_Optional');
	}
	
	public static String leftPad(String source, String pad, Integer length) {
		if (source == null)
		return leftPad(pad, pad, length);
		if (source.length() >= length)
		return source;
		return pad + leftPad(source, pad, length - 1);
			}
	
	public static String nvl(Object value) {
		return nvl(value, 0);
	}
	
	public static String nvl(Object value, Integer length) {
		if (value == null)
		return '*';
		if (value instanceof Decimal)
		return leftPad(String.valueOf(value), '0', length);
		if (value instanceof Date) {
			Date d = (Date)value;
			Integer v = d.year()*10000 + d.month()*100 + d.day();
		return String.valueOf(v);
		}
		return String.valueOf(value);
	}
	
	public static String getKey(Opportunity_SubGroup__c spec) {
		if (spec == null)
		return '*';
		return nvl(spec.Lease_Type__c) + '|' +
			nvl(spec.Payment_Option__c) + '|' +
	nvl(spec.MPY__c, 18) +
		nvl(spec.HPY__c, 8) +
		nvl(spec.Est_Delivery__c) +
		nvl(spec.Term_Mos__c) +
		nvl(spec.Ext_Term__c, 2) +
		nvl(spec.ExchangeIT_Month__c, 2);
	}
    
	public System.Pagereference save() {
		Map<String, Schema.Sobjectfield> lpafFieldMap = Lease_Proposal_Acceptance_Form__c.getSObjectType().getDescribe().fields.getMap();
		Map<String, Schema.Sobjectfield> specFieldMap = Opportunity_SubGroup__c.getSObjectType().getDescribe().fields.getMap();
		Map<String, Lease_Proposal_Acceptance_Form__c> lpafMap = new Map<String, Lease_Proposal_Acceptance_Form__c>();
		for ( Opportunity_SubGroup__c spec : specs) {
			if (!spec.Select_This_Spec_for_LPAF__c || spec.LPAF_Request__c != null) 
				continue;
		String key = getKey(spec);
			Lease_Proposal_Acceptance_Form__c lpafSpec = lpafMap.get(key);
			if (lpafSpec == null) {
				lpafSpec = new Lease_Proposal_Acceptance_Form__c();
				lpafMap.put(key, lpafSpec);
			}
			if (spec.MPY__c == null)
				spec.MPY__c.addError('This field is required');
			if (spec.HPY__c == null)
				spec.HPY__c.addError('This field is required');
			if (spec.Est_Delivery__c == null)
				spec.Est_Delivery__c.addError('This field is required');
			if (spec.Term_Mos__c == null)
				spec.Term_Mos__c.addError('This field is required');
			if (spec.Ext_Term__c == null)
				spec.Ext_Term__c.addError('This field is required');
			if (spec.ExchangeIT_Month__c == null)
				spec.ExchangeIT_Month__c.addError('This field is required');
			lpafSpec.Opportunity__c = opportunity.Id;
			// Cloning values from input LPAF to each lpafSpec record
			for (Schema.FieldsetMember member : getInputSpecFields()) {
					Object value = spec.get(member.getFieldPath());
					Schema.Sobjectfield fs = specFieldMap.get(member.getFieldPath());
					if (fs == null)
						throw new RequestLPAFException('The field ' + member.getFieldPath() + ' was not found in the spec object. Allowed values are: ' + specFieldMap.keySet());
				fs = lpafFieldMap.get(member.getFieldPath());
					if (fs == null)
						continue;
						//throw new RequestLPAFException('The field ' + member.getFieldPath() + ' was not found in the lpaf object. Allowed values are: ' + lpafFieldMap.keySet());
										Schema.Describefieldresult field = fs.getDescribe();
					if (field != null && field.isUpdateable())
						lpafSpec.put(member.getFieldPath(), value);
				}
		
			// Cloning values from input LPAF to each lpafSpec record
			for (Schema.FieldsetMember member : getInputLPAFFields()) {
					Schema.Sobjectfield fs = lpafFieldMap.get(member.getFieldPath());
					if (fs == null)
						throw new RequestLPAFException('The field ' + member.getFieldPath() + ' not found on ' + lpafFieldMap.keySet());
				Object value = lpaf.get(member.getFieldPath());
										Schema.Describefieldresult field = fs.getDescribe();
					if (field != null && field.isUpdateable())
						lpafSpec.put(member.getFieldPath(), value);
				}
		
			// Cloning values from input LPAF to each lpafSpec record
			for (Schema.FieldsetMember member : getInputLPAFFieldsOptional()) {
					Schema.Sobjectfield fs = lpafFieldMap.get(member.getFieldPath());
					if (fs == null)
						throw new RequestLPAFException('The field ' + member.getFieldPath() + ' not found on ' + lpafFieldMap.keySet());
				Object value = lpaf.get(member.getFieldPath());
										Schema.Describefieldresult field = fs.getDescribe();
					if (field != null && field.isUpdateable())
						lpafSpec.put(member.getFieldPath(), value);
				}
		lpafSpec.Ballpark_Pricing_Request__c = lpaf.Ballpark_Pricing_Request__c;
		}
			
		if (ApexPages.hasMessages())
		return null;
		if (lpafMap.size() == 0) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one spec to create the LPAF'));
			return null;
		}
		Savepoint sp = Database.setSavepoint();
		try {
			upsert lpafMap.values();
		} catch (Exception e) {
			ApexPages.AddMessages(e);
			Database.rollback(sp);
			return null;
		}
		
		for ( Opportunity_SubGroup__c spec : specs) {
						if (!spec.Select_This_Spec_for_LPAF__c || spec.LPAF_Request__c != null) 
				continue;
			String key = getKey(spec);
			if (!lpafMap.containsKey(key)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The LPAF for this spec was not found.'));
				Database.rollback(sp);
				return null;
			}
			if (spec.Select_This_Spec_for_LPAF__c && spec.LPAF_Request__c == null) {
				Lease_Proposal_Acceptance_Form__c lpafSpec = lpafMap.get(key);
				spec.LPAF_Request__c = lpafSpec == null ? null : lpafSpec.Id;
			}
		}
		try {
			update specs;
		} catch (Exception e) {
			ApexPages.AddMessages(e);
			Database.rollback(sp);
			return null;
		}
		return new System.Pagereference('/' + opportunity.Id);
	}
	
	public System.Pagereference cloneSpec() {
		String specId = System.currentPageReference().getParameters().get('specId');
		System.assertNotEquals(null, specId, 'Spec id is null');
				Map<String, Schema.Sobjectfield> fieldMap = Opportunity_SubGroup__c.getSObjectType().getDescribe().fields.getMap();
		String query = '';
		for (String fieldName : fieldMap.keySet()) {
			query += (query == '' ? 'SELECT ' : ', ') + fieldName;
		}
		query += ' FROM Opportunity_SubGroup__c WHERE Id = \'' + specId + '\'';
		Opportunity_SubGroup__c[] sps = (Opportunity_SubGroup__c[])Database.query(query);
		System.assertEquals(1, sps.size(), 'La lista no tiene 1 elemento');
		Opportunity_SubGroup__c spec = sps[0];
		Opportunity_SubGroup__c cloned = spec.clone(false, false, false, false);
		cloned.lpaf_request__c = null;
		cloned.Active__c = true;
		cloned.Select_This_Spec_for_LPAF__c = false;
		if (cloned.Original_Spec__c == null)
			cloned.Original_Spec__c = specId;
		insert cloned;
		this.debug += '<br/>Lista antes: ' + specs.size();
		this.specs.add(cloned);
		this.debug += '<br/>Lista después: ' + specs.size();
		return null;
}
}