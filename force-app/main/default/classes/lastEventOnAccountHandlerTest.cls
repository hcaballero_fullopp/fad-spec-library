@istest
public class lastEventOnAccountHandlerTest 
{
    @istest
    public static void testLastEvent()
    {
        Account acc = new Account (name= 'Test');
        insert acc;
        
        Event e = new Event (Subject = 'Test', DurationInMinutes =15,ActivityDateTime = system.now(), whatid = acc.id);
        insert e;
        
        acc = [select Last_Event_Subject__c from account limit 1];
        
        system.assertEquals('Test', acc.Last_Event_Subject__c);
        
    }

}