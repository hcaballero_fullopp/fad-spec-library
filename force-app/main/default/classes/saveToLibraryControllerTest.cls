/**
	Author	: Henry Caballero @fullOpp
	Date	: 9/2/2018
**/
@istest
private class saveToLibraryControllerTest  
{ 
	@IsTest
	public static void testSave()
	{  
        String txt = 'name of thespecName';
        
		Opportunity opp = TestDataFactory.createOpportunity();		
		insert opp;
        
        quote quo = TestDataFactory.createQuote();		
		insert quo;
        
   		Opportunity_Group__c grop = TestDataFactory.createOpportunitySubGroup();		
		insert grop;
        
        Opportunity_SubGroup__c SubGroup = TestDataFactory.createOpportunitySubGroupOpportuni();		
		insert SubGroup;
        
		ApexPages.StandardController sc = new ApexPages.standardController(quo);
		saveToLibraryController ext = new saveToLibraryController(sc);
        
        ext.specName = txt;
        ext.OppGroupId = grop.id;
        test.startTest();
        ext.cancel();
        ext.save();
        test.stopTest();
        System.assertEquals(txt, ext.specName);
    }
}