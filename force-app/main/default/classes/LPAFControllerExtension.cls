public with sharing class LPAFControllerExtension {
	// Not anywhere near ready to go.  Just started working on this
	
	//This class will be used to extend the LPAF to include the related opp's line items for the LPAF detail form
	
    public LPAFControllerExtension(ApexPages.StandardController controller) {
    }
    
        // Map<Id,String> OppOEMsMap = new Map<Id,String>();
    List<Opportunity> oppItems;
    public List<Opportunity> getoppItems() {
        if(oppItems == null) {
          oppItems = [Select 
          				(Select Id, OpportunityId, SortOrder, PricebookEntryId, Product2Id, ProductCode, Name, Quantity, 
          					TotalPrice, UnitPrice, ListPrice, ServiceDate, Description, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, IsDeleted 
          					From OpportunityLineItems) 
						From Opportunity o
						where Id=:ApexPages.CurrentPage().getparameters().get('id')];
            }
            return oppItems;
        }
}