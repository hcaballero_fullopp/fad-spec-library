public class LPAF2HelperController {
    public static List<LPAF2Controller.dataWrapper> addNewRowToList (List<LPAF2Controller.dataWrapper> wrapObjList, Id specId, id oppId) {
        List<LPAF2Controller.dataWrapper> newRecord = new List<LPAF2Controller.dataWrapper>();
        for (LPAF2Controller.dataWrapper wraper : wrapObjList){
            if(wraper.SpecId == specId){
                system.debug(' Add Row');
                Lease_Proposal_Acceptance_Form__c LPAF = new Lease_Proposal_Acceptance_Form__c(Opportunity__c=oppId);
                wraper.lpafList.add(LPAF);
            }
            system.debug('Row List-->>>'+wraper.lpafList.size());
            newRecord.add(wraper);
        }
        return newRecord;
    }
    
    public static List<LPAF2Controller.dataWrapper> removeRowToList ( Id lpafId , Id specId , List<LPAF2Controller.dataWrapper> wrapObjList) {
        List<Lease_Proposal_Acceptance_Form__c> deleteRec = new List<Lease_Proposal_Acceptance_Form__c>();
        List<LPAF2Controller.dataWrapper> newRecord = new List<LPAF2Controller.dataWrapper>();
        for (LPAF2Controller.dataWrapper wraper : wrapObjList){
            if(wraper.SpecId != null && wraper.SpecId == specId){
                for(integer i = 0 ; i<(wraper.lpafList).size();i++){
                    if(wraper.lpafList[i].Id == lpafId){
                        deleteRec.add(wraper.lpafList[i]);
                        wraper.lpafList.remove(i);
                        break;
                    }
                    else if(lpafId == null || wraper.lpafList[i].Id == null){
                        system.debug(' In null method');
                        wraper.lpafList.remove(i);
                        break;
                    }
                }
            }
            newRecord.add(wraper);
        }
        if(deleteRec.size() > 0){
            delete deleteRec;
        }
        return newRecord;
    }
    
    public static List<LPAF2Controller.dataWrapper> saveRowToList (List<LPAF2Controller.dataWrapper> wrapObjList) {
        List<Lease_Proposal_Acceptance_Form__c> saveList = new List<Lease_Proposal_Acceptance_Form__c>();
        List<LPAF2Controller.dataWrapper> newRecord = new List<LPAF2Controller.dataWrapper>();
        for (LPAF2Controller.dataWrapper wraper : wrapObjList){
                for(Lease_Proposal_Acceptance_Form__c lpaf : wraper.lpafList){
                    lpaf.Quote__c = wraper.specId;
                   // if(lpaf.Opportunity__c == null){
                     //   lpaf.Opportunity__c = '0060d00001rkX5L';
                   // }
                    
                    if(lpaf.Id == null){
                        lpaf.Due__c = wrapObjList[0].lpafNew.Due__c;
                        lpaf.Primary_Contact__c = wrapObjList[0].lpafNew.Primary_Contact__c;
                        lpaf.Notes__c = wrapObjList[0].lpafNew.Notes__c;
                    }
                    system.debug('lpaf--->>>'+lpaf);
                    saveList.add(lpaf);
            }
            newRecord.add(wraper);
        }
        if(saveList.size() > 0){
            upsert saveList;
        }
        return newRecord;
    }
}