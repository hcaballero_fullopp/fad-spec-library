@isTest
public class NDATest{

    static testMethod void testNDA(){
    
    Account a = new Account();
    a.name = 'Test Account';
    insert a;
    
    Confidentiality_Agreement__c c = new Confidentiality_Agreement__c();
    c.Account__c = a.Id;
    c.CA_Type__c = 'Fleet Advantage';
    c.Date_Sent__c = date.valueOf('2020-01-01');
    c.Date_Returned__c = date.valueOf('2020-01-01');
    insert c;
    
    // change something on the agreement and update
    c.Date_Returned__c = date.valueOf('2020-01-02');
    update c;
    
    }
    
    
    
}