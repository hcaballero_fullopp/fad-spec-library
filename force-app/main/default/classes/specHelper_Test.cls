@isTest
private class specHelper_Test {
	@IsTest
    public static void specTriggerTest(){
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        opsp.add(Osg);
        insert opsp;
        test.stopTest();
        Spec_Products__c specPro = new Spec_Products__c();
        specPro = [Select id,Product__c, Spec__c from Spec_Products__c where Spec__c=: OSg.Id];
        System.assert(specPro != null);
    }
    
    @isTest 
    public static void preventUpdateProduct_OEM () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Tractor');
        OSg.OEM__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod = TestDataFactory.createProduct('Tractor');
        OSg.OEM__c = prod.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,OEM__c from Opportunity_SubGroup__c where OEM__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    
    @isTest 
    public static void preventUpdateProduct_Rear_Axle () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Rear Axle');
        OSg.OEM__c = null;
        OSg.Rear_Axle__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Rear Axle');
        OSg.Rear_Axle__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Rear_Axle__c from Opportunity_SubGroup__c where Rear_Axle__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_X5th_Wheel () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('5th Wheel');
        OSg.OEM__c = null;
        OSg.X5th_Wheel__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('5th Wheel');
        OSg.X5th_Wheel__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,X5th_Wheel__c from Opportunity_SubGroup__c where X5th_Wheel__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Engine () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Engine');
        OSg.OEM__c = null;
        OSg.Engine__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Engine');
        OSg.Engine__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Engine__c from Opportunity_SubGroup__c where Engine__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Axle_LO () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Front Axle');
        OSg.OEM__c = null;
        OSg.Axle_LO__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Front Axle');
        OSg.Axle_LO__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Axle_LO__c from Opportunity_SubGroup__c where Axle_LO__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Front_Suspension () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Front Suspension');
        OSg.OEM__c = null;
        OSg.Front_Suspension__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Front Suspension');
        OSg.Front_Suspension__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Front_Suspension__c from Opportunity_SubGroup__c where Front_Suspension__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Front_Wheel () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Front Wheel');
        OSg.OEM__c = null;
        OSg.Front_Wheel__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Front Wheel');
        OSg.Front_Wheel__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Front_Wheel__c from Opportunity_SubGroup__c where Front_Wheel__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Rear_Suspension () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Rear Suspension');
        OSg.OEM__c = null;
        OSg.Rear_Suspension__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Rear Suspension');
        OSg.Rear_Suspension__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Rear_Suspension__c from Opportunity_SubGroup__c where Rear_Suspension__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Rear_Wheel () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Rear Wheel');
        OSg.OEM__c = null;
        OSg.Rear_Wheel__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Rear Wheel');
        OSg.Rear_Wheel__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Rear_Wheel__c from Opportunity_SubGroup__c where Rear_Wheel__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Reefer () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Reefer');
        OSg.OEM__c = null;
        OSg.Reefer__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Reefer');
        OSg.Reefer__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Reefer__c from Opportunity_SubGroup__c where Reefer__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
    
    @isTest 
    public static void preventUpdateProduct_Transmission () {
        list<Opportunity_SubGroup__c> opsp = new list<Opportunity_SubGroup__c>();
        test.startTest();
        Opportunity_SubGroup__c OSg = TestDataFactory.createOpportunitySubGroupOpportuni();
        Product2 prod1 = TestDataFactory.createProduct('Transmission');
        OSg.OEM__c = null;
        OSg.Transmission__c = prod1.Id;
        opsp.add(Osg);
        insert opsp;
        Product2 prod2 = TestDataFactory.createProduct('Transmission');
        OSg.Transmission__c = prod2.Id;
        update OSg;
        test.stopTest();
        list<Opportunity_SubGroup__c> opspupdateList = new list<Opportunity_SubGroup__c>();
        opspupdateList = [Select id, Name,Transmission__c from Opportunity_SubGroup__c where Transmission__c =:prod1.Id ];
        System.assert(opspupdateList.size() > 0);
    }
}