/**
* Generate the enviroment for the Unit Tests
* @author Sebastian Muñoz - Force.com Labs
* @createddate 06/08/2010
*/
public with sharing class AccountHierarchyTestData{
	
	/**
	* Set the necesary attributes used during test
	*/
    public static void createTestHierarchy(){
    	
    	InlineAcountHerachy_TestUtilities testUtils = new InlineAcountHerachy_TestUtilities();
    	//Set of Fields should be checked
    	Set<String> fieldsToCheck = new Set<String>{'AnnualRevenue', 'BillingCity','BillingCountry','BillingPostalCode','BillingState', 'BillingStreet', 'Type', 'ShippingCity', 'ShippingStreet', 'Name', 'ShippingState', 'ShippingPostalCode', 'ShippingCountry' };
		
		//Create my Parent Account 
		testUtils.createAccounts( 1 , fieldsToCheck );
 		testUtils.testAccList[0].Type = 'HierarchyTest0';
 		testUtils.updateAccountList( fieldsToCheck );
 		
 		Account parentAccount = testUtils.testAccList[0];
        Id parentID = parentAccount.Id;
        System.Assert( parentID != null , 'Parent Id not found' );
        
        // Create 10 sub accounts
    	testUtils.createAccounts( 10 , fieldsToCheck );
    	testUtils.testAccList[0].Name = 'Will Smith';
    	testUtils.testAccList[1].Name = 'Christopher Reeve';
    	testUtils.testAccList[2].Name = 'Tom Hanks';
    	testUtils.testAccList[3].Name = 'Antonio Banderas';
    	testUtils.testAccList[4].Name = 'Julia Roberts';
    	testUtils.testAccList[5].Name = 'Demi Moore';
    	testUtils.testAccList[6].Name = 'Pamela Anderson';
    	testUtils.testAccList[7].Name = 'Michael Duglas';
    	testUtils.testAccList[8].Name = 'Michael Fox';
    	testUtils.testAccList[9].Name = 'Robert Deniro';
    	
    	
    	Integer i = 0;
        for ( Account accAux : testUtils.testAccList ){ //Now i need change the names
        	accAux.Type = 'HierarchyTest' + String.valueOf( i );
            i++;
        }
        testUtils.updateAccountList( fieldsToCheck );        
        
        List<Account> accountList = [ Select Id, parentID, name from account where Type like 'HierarchyTest%' AND Id <> :parentId ORDER BY Type limit 10];
                
        for ( Integer x = 0; x < accountList.size(); x++ ){
                accountList[x].parentID = parentID;
                parentID = accountList[x].Id; 
                    }
        
        testUtils.testAccList.clear();
        testUtils.testAccList.addAll( accountList );
        testUtils.updateAccountList( fieldsToCheck );

		// Create 10 sub accounts
		Account subTreeParent = [ Select id, parentID, name, Type from account where Type = 'HierarchyTest4' limit 10 ];
        parentID = subTreeParent.Id;
        testUtils.createAccounts(10, fieldsToCheck);
    	 
		i = 0;
		for ( Account accAux : testUtils.testAccList ){ //Now i need change the names
        	accAux.Type = 'HierarchyTest' + '4.' + String.valueOf( i );
        }
		testUtils.updateAccountList( fieldsToCheck );

        List<Account> subAccountsList = [ Select Id, Type, parentID, Name from Account where Type like 'HierarchyTest4%' limit 10  ];
        for ( Integer z = 1; z < subAccountsList.size(); z++ ){
            subAccountsList[z].parentID = parentID;
            parentID = accountList[z].Id; 
        }
        
        testUtils.testAccList.clear();
        testUtils.testAccList.addAll( subAccountsList );
    }
}