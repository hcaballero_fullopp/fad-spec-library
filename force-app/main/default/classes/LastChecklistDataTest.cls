/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LastChecklistDataTest {

    static testMethod void myUnitTest() {
    	Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true, Type = 'Client');
    	insert account;
    	Opportunity opportunity = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
    	insert opportunity;
    	Data_Checklist__c data = new Data_Checklist__c(Fleet_List_Received_Date__c = Datetime.now(), Portal_Load_Date__c = Date.today(), Related_to_Account__c = account.Id, Related_To_Opportunity__c = opportunity.Id);
    	    	Test.startTest();
    	insert data;
    	Test.stopTest();
    	opportunity = [SELECT Id, OBC_Data_Load__c, Fleet_List_Rec_d__c FROM Opportunity WHERE Id = :opportunity.Id];
    	System.assertNotEquals(null, data.Fleet_List_Received_Date__c, 'data.Fleet_List_Received_Date__c is null');
    	System.assertEquals(data.Fleet_List_Received_Date__c, opportunity.Fleet_List_Rec_d__c, 'data.Fleet_List_Received_Date__c does not match');
    	System.assertEquals(data.Portal_Load_Date__c, opportunity.OBC_Data_Load__c);
            }
}