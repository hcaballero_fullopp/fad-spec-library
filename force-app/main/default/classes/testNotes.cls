@isTest
private class testNotes {

    static testMethod void myNotesTest() {
        Account acct = new Account ( Name = 'Acme, Inc.');
        insert acct;
        Note oNote = new Note( ParentId = acct.Id, Title = 'Test', Body = 'Test' );
        insert oNote;
        System.assertEquals('Test', [Select Title From Note where ParentId = :acct.Id].Title);

        Onsite_Call__c oc = new Onsite_Call__c ( Account__c = acct.id, Location__c='None',Subject_or_Keywords__c='Testing');
        insert oc;
        oNote = new Note( ParentId = oc.Id, Title = 'Test', Body = 'Test' );
        insert oNote;


        //System.assertEquals('Test-TRIGGER-WAS-HERE',[Select Title From Note where Id = :oNote.Id].Title);
        
        //Need to add check to ensure that a new email record was created when the new note was inserted.
        
        //Trigger works but not this text class.
        //Fails on line 15: Expected Test-TRIGGER-WAS-HERE, Actual: Test
       
    }
}