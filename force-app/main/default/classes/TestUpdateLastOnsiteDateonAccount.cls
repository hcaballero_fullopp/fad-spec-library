@isTest
public class TestUpdateLastOnsiteDateonAccount{
    
    static testMethod void testOSC(){
        Account a = new Account();
        a.name = 'Test Account';
        insert a;
        
        Event e = new Event();
        e.WhatID = a.Id;
        e.subject = 'none';
        e.type = 'Onsite Call';
        e.DurationInMinutes=5;
        e.ActivityDateTime=date.valueOf('2020-01-01 01:00:00 AM');
        insert e;
    }
}