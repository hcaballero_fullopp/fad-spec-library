public with sharing class OpportunitiesForAccountID {
    public OpportunitiesForAccountID(ApexPages.StandardController controller) {
    }
    
        // Map<Id,String> OppOEMsMap = new Map<Id,String>();

    List<Opportunity_Group__c> opps;
    public List<Opportunity_Group__c> getoppgroup() {
        if(opps == null) {
                //strOEMs='';
          opps = [SELECT Opportunity__r.StageName, Opportunity__r.AccountId, Opportunity__r.id, Opportunity__r.Name, Opportunity__r.Amount, 
                        id, Name, Opportunity__c, Quantity__c,Equipment_Pricing_Total__c,   Tractor_Type__c,Trailer__c, Opportunity__r.Opportunity_Value__c, Opportunity__r.Total_Units__c, 
                    (SELECT id, Name,Model__c,Total_Price__c,Manufacturer__c,Year__c,Unit_Price__c,TermYears__c,MPY__c,Select_This_SubGroup_For_Opportunity__c,
                        LRF__c,ExchangeIT_Month__c,Extension_Discount__c, Axle__c
                    FROM Opportunity_SubGroups__r 
                    WHERE Select_This_SubGroup_For_Opportunity__c = True)
                FROM Opportunity_Group__c 
                WHERE Opportunity__r.AccountId =:ApexPages.CurrentPage().getparameters().get('id') 
                ORDER BY Opportunity__r.StageName];
            }
            return opps;
    }
}