@isTest
private class LastPresentationDatesTest {
		static Presentation__c presentation;
	static Opportunity opportunity;
	
	
	static void init() {
		Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true, Type = 'Client');
		insert account;
    	
		opportunity = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
		insert opportunity;

		presentation = new Presentation__c();
		Integer financeDays = Integer.valueOf(Math.random()*200);
		presentation.Presented_to_Finance__c = Date.today().addDays(-financeDays);
		Integer operationDays = Integer.valueOf(Math.random()*200);
		presentation.Presented_to_Ops__c = Date.today().addDays(-operationDays);
		presentation.Date_Presented__c = Date.today();
		presentation.Related_to_Account__c = account.Id;
		presentation.Opportunity__c = opportunity.Id;
		presentation.Completed__c = Date.today();
	}

	static testMethod void introTCOTest() {
		init();
		presentation.Presented__c = 'TCO Intro';
		Test.startTest();
		insert presentation;
		Test.stopTest();
		opportunity = [SELECT Intro_TCO_Presented__c FROM Opportunity WHERE Id = :opportunity.Id];
		presentation = [SELECT Intro_TCO_Presented__c FROM Presentation__c WHERE Id = :presentation.Id];
		System.assertEquals(presentation.Intro_TCO_Presented__c, opportunity.Intro_TCO_Presented__c, 'The field Intro_TCO_Presented__c does not match');
	}
	
	static testMethod void fuellBreakEvenTest() {
		init();
		presentation.Presented__c = 'Fuel Breakeven';
		Test.startTest();
		insert presentation;
		Test.stopTest();
		opportunity = [SELECT Fuel_Break_Even_Date__c FROM Opportunity WHERE Id = :opportunity.Id];
		presentation = [SELECT Fuel_Break_Even__c FROM Presentation__c WHERE Id = :presentation.Id];
		System.assertEquals(presentation.Fuel_Break_Even__c, opportunity.Fuel_Break_Even_Date__c, 'The field Fuel_Break_Even_Date__c does not match');
	}
	
	static testMethod void ccaBarrelChartTest() {
		init();
		presentation.Presented__c = 'CCA';
		Test.startTest();
		insert presentation;
		Test.stopTest();
		opportunity = [SELECT CCA_Barrel_Chart__c FROM Opportunity WHERE Id = :opportunity.Id];
		presentation = [SELECT CCA_Barrel_Chart__c FROM Presentation__c WHERE Id = :presentation.Id];
		System.assertEquals(presentation.CCA_Barrel_Chart__c, opportunity.CCA_Barrel_Chart__c, 'The field CCA_Barrel_Chart__c does not match');
	}
	
	static testMethod void LMSDateTest() {
		init();
		presentation.Presented__c = 'LMS';
		Test.startTest();
		insert presentation;
		Test.stopTest();
		opportunity = [SELECT LMS_Date__c FROM Opportunity WHERE Id = :opportunity.Id];
		presentation = [SELECT LMS_Date__c FROM Presentation__c WHERE Id = :presentation.Id];
		System.assertEquals(presentation.LMS_Date__c, opportunity.LMS_Date__c, 'The field LMS_Date__c does not match');
	}

	static testMethod void mpgMpyTest() {
		init();
		presentation.Presented__c = 'MPG';
		Test.startTest();
		insert presentation;
		Test.stopTest();
		opportunity = [SELECT MPG_MPY__c FROM Opportunity WHERE Id = :opportunity.Id];
		presentation = [SELECT MPG_MPY__c FROM Presentation__c WHERE Id = :presentation.Id];
		System.assertEquals(presentation.MPG_MPY__c, opportunity.MPG_MPY__c, 'The field MPG_MPY__c does not match');
	}
	
	static testMethod void m_rTest() {
		init();
		presentation.Presented__c = 'M&R';
		Test.startTest();
		insert presentation;
		Test.stopTest();
		opportunity = [SELECT M_R__c FROM Opportunity WHERE Id = :opportunity.Id];
		presentation = [SELECT M_R__c FROM Presentation__c WHERE Id = :presentation.Id];
		System.assertEquals(presentation.M_R__c, opportunity.M_R__c, 'The field M_R__c does not match');
	}
}