/***    Author      : Henry Caballero @ fullOpp
Date        : 8/27/2018
Description : Logic to save the "Opportunity Spec (Quote)" as a Spec Library        
***/

public class saveToLibraryController  
{
    private id quoteId;
    public string specName {get;set;}
    public id OppGroupId {Get;set;}
    
    public saveToLibraryController (ApexPages.StandardController controller)
    {
        quoteId = controller.getId();
        OppGroupId = 'a004F000002iQN8';
    }
    
    public PageReference save()
    {  
        quote q =  getQuote();
        OppGroupId = 'a004F000002iQN8';
        // create a new spec libray     
        Opportunity_SubGroup__c spec = new Opportunity_SubGroup__c ();
        spec.Description__c = specName;
        spec.Opportunity_Group__c = OppGroupId;
        for(Schema.FieldSetMember f : equipmentPricingHelper.fields) 
        {                
            spec.put( f.getFieldPath() , q.get(f.getFieldPath()) );
        }
        
        insert spec;
        
        List<Spec_Products__c> specAttributes = new List<Spec_Products__c> ();
        for ( QuoteLineItem qli :  [select Product2Id from  QuoteLineItem where QuoteId=:quoteId]  )
        {
            specAttributes.add( new Spec_Products__c(product__c =  qli.Product2Id, Spec__c = spec.id ) );
        }
        
        insert  specAttributes;
        
        q.put('Spec_Library__c', spec.id);
        update q;
        
        return new PageReference('/'  + quoteId );      
    }
    
    public PageReference cancel()
    {
        return new PageReference('/'  + quoteId );
    }
    
    private quote getQuote()
    {
         
        // get the quote with the fields defined to be mapped to the new spec library
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : equipmentPricingHelper.fields) 
        {  
            query += f.getFieldPath() + ', ';
        }
        
        query += 'Id, Name FROM quote where id =: quoteId';
        
        return (quote)Database.query(query);
    }
    
}