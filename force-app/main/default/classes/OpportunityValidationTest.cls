/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OpportunityValidationTest {
	
	static {
		TriggerHandler.enable(OpportunityValidationHandler.class);
	}

    static testMethod void accountTypeClient()
    {
        list<recordType> recordTypeClient = [SELECT id FROM RecordType where SobjectType = 'Account' and name = 'Client' ];
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true, type ='Client', recordTypeId = recordTypeClient[0].id);
		insert account;
		system.debug(recordTypeClient[0].id);
        Opportunity opportunity = new Opportunity(	Management_Override__c=false,Name = 'Test Opp', 
										CloseDate = Date.today(), StageName = '2 - Validate - Fleet Study', AccountId = account.Id,
										Intro_TCO_Presented__c= Date.today(),
										CCA_Barrel_Chart__c= Date.today(),
										Fuel_Break_Even_Date__c= Date.today(),
										LMS_Date__c= Date.today(),
										MPG_MPY__c= Date.today(),
										M_R__c= null
										);
		insert opportunity;
        
    }
    
	static testMethod void OnHoldTest() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'Test Opp', RecordTypeId = accountRecordTypeId);
        insert account;
		Opportunity opportunity = new Opportunity(Name = 'On Hold Opportunity', CloseDate = Date.today(), StageName = '0 - On Hold', AccountId = account.Id);
		Test.startTest();
		insert opportunity;
		Test.stopTest();
	}
	
	static testMethod void validationUtilsTest() {
		Opportunity opportunity = new Opportunity(Name = '1 - Estimate - Gathering Info CNA Opportunity', CloseDate = Date.today(), StageName = '1 - Estimate - Gathering Info CNA');
		ValidationUtils.validateRequiredFields('1 - Estimate - Gathering Info CNA Required Fields', opportunity);
				Schema.FieldSetMember[] fieldSets = ReflectUtils.getFields(Opportunity.getSobjectType(), '1 - Estimate - Gathering Info CNA Required Fields');
				System.assertEquals(fieldSets.size(), ApexPages.getMessages().size());
					}
					
	static testMethod void validationUtilsStage2Test() {
		String stageName = '2 - Validate - Fleet Study Required Fields';
		Opportunity opportunity = new Opportunity(Name = stageName + ' Opportunity', CloseDate = Date.today(), StageName = stageName);
		ValidationUtils.validateRequiredFields(stageName, opportunity);
				Schema.FieldSetMember[] fieldSets = ReflectUtils.getFields(Opportunity.getSobjectType(), stageName);
				System.assertEquals(fieldSets.size(), ApexPages.getMessages().size());
					}
            
	static testMethod void gatheringInfoCNATest() {
		Opportunity opportunity = new Opportunity(Name = '1 - Estimate - Gathering Info CNA Opportunity', CloseDate = Date.today(), StageName = '1 - Estimate - Gathering Info CNA');
				Test.startTest();
		try {
			insert opportunity;
		} catch (Exception e) {
		}
		Test.stopTest();
				System.assertEquals(0, ApexPages.getMessages().size());
					}
	
	static testMethod void fleetStudyTest() {
		Opportunity opportunity = new Opportunity(Name = '2 - Validate - Fleet Study Opportunity', CloseDate = Date.today(), StageName = '2 - Validate - Fleet Study');
		Test.startTest();
		try {
			insert opportunity;
		} catch (Exception e) {
		}
		Test.stopTest();
		Integer expectedErrors = 0;
		Schema.FieldSetMember[] fieldSets = ReflectUtils.getFields(Opportunity.getSobjectType(), '1 - Estimate - Gathering Info CNA Required Fields');
		expectedErrors += fieldSets.size(); 
				String errors = '';
		for (ApexPages.Message message : ApexPages.getMessages()) {
			errors += message.getDetail() + '\n';
		}
		//System.debug('\n' + errors);
		//System.assertEquals(expectedErrors, ApexPages.getMessages().size(), 'Se registraron los siguientes errores: \n' + errors);
	}            
}