public class DueTaskController {
	
		public Id ownerId {get; set;}
	public final String fullURL {get; set;}
	
	public DueTaskController() {
		fullURL = URL.getSalesforceBaseUrl().toExternalForm();
	}
	
	public Task[] getTasks() {
				//System.assertNotEquals(null, ownerId, 'Owner is null');
		if (ownerId == null)
		ownerId = UserInfo.getUserId();
		return [SELECT Id, Subject, ActivityDate, OwnerId, Status, What.Name, Who.Name FROM Task WHERE ActivityDate < :Date.today() AND Status <> 'Completed' AND OwnerId = :ownerId]; 
	}
}