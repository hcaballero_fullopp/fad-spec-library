public with sharing class ReflectUtils {
	
	public class ReflectUtilsException extends Exception {}
	
	public static List<Schema.FieldSetMember> getFields(Schema.SobjectType stype, String fieldSetName) {
		Schema.Describesobjectresult result = stype.getDescribe();
		Map<String, Schema.FieldSet>  fieldSetMap = result.fieldSets.getMap();
		Schema.FieldSet fs = fieldSetMap.get(fieldSetName);
		if (fs != null)
			return fs.getFields();
		for (Schema.FieldSet fieldSet : fieldSetMap.values()) {
			if (fieldSet.getLabel() == fieldSetName)
				return fieldSet.getFields();
		}
		throw new ReflectUtilsException('The fieldset: ' + fieldSetName + ' is required for ' + stype.getDescribe().getName());
	}
}