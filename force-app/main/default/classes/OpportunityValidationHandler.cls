public with sharing class OpportunityValidationHandler implements TriggerHandler.HandlerInterface {
				public static final Set<String> isolatedStages = new Set<String> {
		'7 - Lost', 
		'8 - Dead', 
		'0 - On Hold'
					};
					
	public static final String[] stages = new String[] {
		'1 - Estimate - Gathering Info CNA',
		'2 - Validate - Fleet Study',
		'3 - Equipment Pricing',
		'4 - Lease Pricing/Proposal',
		'5 - Closing',
		'6 - Awarded'
	};
	
	public void handle() {
		
		Set<String> stagesSet = new Set<String>(stages);
        // get the recordType client
        list<recordType> recordTypeClient = [SELECT id FROM RecordType where SobjectType = 'Account' and name = 'Client' ];
        
        // get the accounts from the opportunities      
        Set<String> acc = new Set<String>();
        for (Opportunity opportunity : (Opportunity[])Trigger.new) 
        {
            if (!acc.contains(opportunity.AccountId) )
            	acc.add(opportunity.AccountId);
        }
		
        map<id, account> accClient;
		if (recordTypeClient.size()> 0)        
        	accClient= new map<id, account>([select id from account where id in : acc and recordTypeId = :recordTypeClient[0].id  ]);
        else
            accClient= new map<id, account>();
        
	for (Opportunity opportunity : (Opportunity[])Trigger.new) {
        
        system.debug('trigger');
        system.debug(accClient.containsKey(opportunity.Accountid));
             
        if (accClient.containsKey(opportunity.Accountid))
            continue;
        
		if (opportunity.Management_Override__c)
			continue;
		if (isolatedStages.contains(opportunity.StageName)) {
			ValidationUtils.validateRequiredFields(opportunity.StageName + ' Required Fields', opportunity);
			continue;
		}
		
		if (!stagesSet.contains(opportunity.StageName))
			continue;
		for (String stage : stages) {
			if (stage == opportunity.StageName)
				break;
				

			ValidationUtils.validateRequiredFields(stage + ' Required Fields', opportunity, ' As a reminder, in order to advance to the next Stage, please ensure that all required information for the current Stage is collected, or request for an override from the Management team.');
		}
	}
	}
}