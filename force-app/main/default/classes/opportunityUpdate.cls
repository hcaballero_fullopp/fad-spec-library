public class opportunityUpdate {

//
/***********************************************************************************
*  
* Trigger:      Opportunity Update
* Author:       Harvest Solutions
* Date:         March 2014
* Description:  Calculate the Opportunity Amount from the Opportunity SubGroup records that have been marked as
*               Selected For Opportunity.  This is a brute force approach that takes every Opportunity referenced 
*               by Opportunity Subgroups in the triggers and updates their amount to the roll-up summary of Opportunity
*               Group Amounts on the Opportunity.
*
* Updates
*
* Date        Author         Description
*------------------------------------------------------------------------------------
*5/10/14    CMK     Adding groups and subgroups to opportunity 
*
************************************************************************************/

   public void updateOpps(Set<Id> oppIds) {
      List<Opportunity> OpportunitiesForUpdate = new List<Opportunity>();
      List<Opportunity_Group__c> Groups = new List<Opportunity_Group__c>();
      List<Opportunity_SubGroup__c> SubGroups = new List<Opportunity_SubGroup__c>();

      Map<Id,Decimal> GroupTotalMap = new Map<Id,Decimal>();
      Map<Id,Decimal> OppTotalMap = new Map<Id,Decimal>();
      Map<Id,Id> GroupOppMap = new Map<Id,Id>();
      Map<Id,String> OppOEMsMap = new Map<Id,String>();
      //Map<Id,String> SGOEMsMap = new Map<Id,String>(); //I think is is going to be needed... ?



// Get the Opportunities
      for (Opportunity o : [select Id, Amount, GrandTotal__c, OEM_Make__c from Opportunity where Id in :oppIds]) {
         OpportunitiesForUpdate.add(o);
         OppTotalMap.put(o.Id, 0);
         OppOEMsMap.put(o.Id,'');
      }
      
// Collect all of the Opportunity Group Ids 
      for (Opportunity_Group__c g : [select id, Opportunity__c, Group_Total__c 
                                      from Opportunity_Group__c
                                      where Opportunity__c in :oppIds]) {
           GroupTotalMap.put(g.Id,0);
           Groups.add(g);
    }

      for (Opportunity_SubGroup__c sg : [select Id, Amount__c, Total_Price__c, Opportunity_Group__c, Select_This_SubGroup_For_Opportunity__c, Manufacturer__c, Opportunity_ID__c 
                                         from Opportunity_SubGroup__c where Opportunity_Group__c in :GroupTotalMap.keyset()]) {
         if (sg.Select_This_SubGroup_For_Opportunity__c) {
            GroupTotalMap.put(sg.Opportunity_Group__c,GroupTotalMap.get(sg.Opportunity_Group__c) + sg.Total_Price__c);
            
            SubGroups.add(sg);
            //SGEOMsMap.put(sg.Opportunity_Group__c,SGEOMsMap.get(sg.Opportunity_Group__c) + strOEMS);

            System.debug('Adding subgroup ' + sg.Id + ' total to group total, amount: ' + sg.Total_Price__c + ' flag: ' + sg.Select_This_SubGroup_For_Opportunity__c);
         }
      }
      

// Get the Opportunities

 for (Opportunity_Group__c g : Groups) {
    g.Group_Total__c = GroupTotalMap.get(g.Id);
    oppTotalMap.put(g.Opportunity__c,oppTotalMap.get(g.Opportunity__c) + g.Group_Total__c);
    string OEMs = '';
    string strComma = '';
        for( Opportunity_Subgroup__c sg : SubGroups) {
            if(sg.Manufacturer__c != Null) {
                if(OEMs.contains(sg.Manufacturer__c)==False) {
                    OEMs= sg.Manufacturer__c + strComma + OEMs;
                    strComma = ', ';
                }
            }
        }
              if(OEMs!=Null) {OppOEMsMap.put(g.Opportunity__c, OEMs);}
    }



// Update the Opportunity Amounts from the roll up summary

      for (Opportunity o : OpportunitiesForUpdate) {
        
         if (o.GrandTotal__c != null) {
            o.Amount = o.GrandTotal__c;
         } else {
            if( o.Amount != OppTotalMap.get(o.Id)) {
                o.Amount = OppTotalMap.get(o.Id);
                System.debug('Updating ' + o.Id + ' amount from ' + o.Amount + ' to ' + OppTotalMap.get(o.Id));
            }
         }
         o.OEM_Make__c = OppOEMsMap.get(o.Id);
         System.debug('Updating ' + o.Id + ' OEMS From ' + o.OEM_Make__c + ' to ' + OppOEMsMap.get(o.Id));
         
      }

      if (Groups.size() > 0) {
         update Groups;
      }
      if (OpportunitiesForUpdate.size() > 0) {
         update OpportunitiesForUpdate;
      }
    
      

      }
}