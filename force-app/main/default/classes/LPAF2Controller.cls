/**
	Author		: Henry Caballero @fullOpp
	Date		: 9/5/2018
	Description : controller for the new LPAF page 
**/

public class LPAF2Controller
{
	public class RequestLPAFException extends Exception {}

	public Opportunity opportunity {get; set;}
	public Lease_Proposal_Acceptance_Form__c newLPAF {get; set;}
	public List<dataWrapper> wraplist {get; set;}
	public List<quote> specs {get; set;}
	public id OppId;
	public id recordID {get; set;}
    public id SpecIdFromPage {get; set;}


	public LPAF2Controller  ( ApexPages.StandardController controller )
		{			
			specs = new List<quote>();
			this.OppId = controller.getId();
			wraplist = new List<dataWrapper>();
			initSpecs();
		}

	public void initSpecs()
		{
			specs = [select LPAF_Page_Header__c,(Select Id,LPAF_Quantity__c,Lease_Type__c,Payment_Option__c,MPY__c,HPY__c,Est_Delivery__c,
					Term_Mos__c,Ext_Term_Years__c,ExchangeIT_Month__c,Quote__c,Opportunity__c,Due__c,Primary_Contact__c,Notes__c from LPAFs__r) from quote where OpportunityId =: OppId]; //:OppId
						
			for(quote qt : specs){
				wraplist.add(new dataWrapper(qt.LPAF_Page_Header__c, qt.Id ,qt.LPAFs__r));
			}
		}

	public pageReference save()
		{
			integer i = 0;
			
			for(dataWrapper wrap: wraplist){
				for(Lease_Proposal_Acceptance_Form__c lp : wrap.lpafList) {
					if(lp.Id == null){
						if(wraplist[0].lpafNew.Due__c == null){
							i = i+1;
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'LPAF Due is a required field.'));
						}
						else if(wraplist[0].lpafNew.Primary_Contact__c == null){
							i = i+1;
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Primary Contact is a required field.'));
						}
						else if(wraplist[0].lpafNew.Notes__c == null || wraplist[0].lpafNew.Notes__c == '' ){
							i = i+1;
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Note is a required field.'));
						}
						else if(lp.LPAF_Quantity__c == null || lp.LPAF_Quantity__c <= 0){
							i = i+1;
							system.debug(' Quantity is low. ');
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Quantity should be more that zero.'));
						}
					}
				}
			}

			if(i < 1){
				wraplist = LPAF2HelperController.saveRowToList(wraplist);
				if(wraplist != null){
					pageReference pr = new pageReference('/'+OppId);
					pr.setRedirect(true);
					return pr;
				}
			}
			else {
				return null;
			}
			return null;
		}

	public pageReference cancel()
		{
            Id OppId = Apexpages.currentPage().getParameters().get('id');
			pageReference pr = new pageReference('/'+OppId);
			pr.setRedirect(true);
			return pr;
		}

	public void addNewRowToLPAFList (){
		//Id specId = Apexpages.currentPage().getParameters().get('specId');
        system.debug('specId::::'+SpecIdFromPage);
		wraplist = LPAF2HelperController.addNewRowToList(wraplist , SpecIdFromPage, OppId);
		system.debug('wraplist ::::'+wraplist);
	}
	public void deleteRecord()
		{
			//Id specId = Apexpages.currentPage().getParameters().get('specId');
            system.debug('specId::::'+SpecIdFromPage);
			wraplist = LPAF2HelperController.removeRowToList(recordID, SpecIdFromPage, wraplist);
			
		}

	public class dataWrapper {
		public Id SpecId {get;set;}
		public String pageHeader {get; set;}
		public List<Lease_Proposal_Acceptance_Form__c> lpafList {get; set;}
		public Lease_Proposal_Acceptance_Form__c lpafNew {get;set;}
		public dataWrapper (String pageHeader, Id quoteId,  List<Lease_Proposal_Acceptance_Form__c> lpaf)
		{
			init();
			this.pageHeader = pageHeader;
			this.lpafList = lpaf;
			this.SpecId = quoteId;
		
		}

		public dataWrapper ()
		{
			init();			 
		}

		private void init()
		{
			this.lpafNew = new Lease_Proposal_Acceptance_Form__c();
			this.lpafList = new  List<Lease_Proposal_Acceptance_Form__c> ();
		}
	}
}