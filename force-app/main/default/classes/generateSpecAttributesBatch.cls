global class generateSpecAttributesBatch implements Database.Batchable<SObject> {
	
	global Set<string> SpecProductFields = new Set<string> {
													'X5th_Wheel__c',
													'Engine__c',
													'Axle_LO__c',
													'Front_Suspension__c',
													'Front_Wheel__c',
													'OEM__c',
													'Rear_Axle__c',
													'Rear_Suspension__c',
													'Rear_Wheel__c',
													'Reefer__c',
													'Transmission__c'
												};

	
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('select Id, X5th_Wheel__c, Engine__c, Axle_LO__c, Front_Suspension__c, Front_Wheel__c, OEM__c, Rear_Axle__c, Rear_Suspension__c, Rear_Wheel__c, Reefer__c, Transmission__c from Opportunity_SubGroup__c');
	}
   	global void execute(Database.BatchableContext context, List<Opportunity_SubGroup__c> scope) 
	{
		List<Spec_Products__c> products = new List<Spec_Products__c>();

		for (Opportunity_SubGroup__c spec: scope )
		{
			for (string field : SpecProductFields)
			{
				id productId = (id)spec.get(field);
				if (productId==null)
					continue;

				products.add(new Spec_Products__c (Product__c = productId , Spec__c = spec.id) );	
			}
		}

		upsert products;	
	}
	
	global void finish(Database.BatchableContext context) {
		
	}
}