@isTest
private class LastStageOnOpportunityTest {

    static testMethod void myUnitTest() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'Test Opp', RecordTypeId = accountRecordTypeId);
        insert account;
        Opportunity opportunity = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
        Test.startTest();
        insert opportunity;
        opportunity.StageName = 'Closed Stage';
        update opportunity;
        Test.stopTest();
        opportunity = [SELECT StageName, Last_Stage__c FROM Opportunity WHERE Id = :opportunity.Id];
        System.assertNotEquals(opportunity.StageName, opportunity.Last_Stage__c);
                System.assertEquals('Closed Stage', opportunity.StageName);
        System.assertEquals('Draft', opportunity.Last_Stage__c);
    }
    
    static testMethod void approvalTest() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'Test Opp', RecordTypeId = accountRecordTypeId);
        insert account;
        Opportunity opportunity = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
        Test.startTest();
        insert opportunity;
        opportunity.StageName = 'Closed Stage';
        update opportunity;
opportunity.Is_Approved__c = 'No';
        update opportunity;
        Test.stopTest();
        opportunity = [SELECT StageName, Last_Stage__c FROM Opportunity WHERE Id = :opportunity.Id];
        //System.assertEquals('Draft', opportunity.Last_Stage__c);
        //System.assertEquals(opportunity.StageName, opportunity.Last_Stage__c);
                //System.assertEquals('Draft', opportunity.StageName);
        //System.assertEquals('Draft', opportunity.Last_Stage__c);
    }
}