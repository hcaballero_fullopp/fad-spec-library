@isTest
private class UpdateOpportunityAmountTest {

    static testMethod void myUnitTest() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'Test Opp', RecordTypeId = accountRecordTypeId);
        insert account;
        Opportunity opportunity = new Opportunity(Name = 'Opportunity Test', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
        insert opportunity;
        Opportunity_Group__c er = new Opportunity_Group__c(Quantity__c = 10, Opportunity__c = opportunity.Id);
        insert er;
        Opportunity_Subgroup__c spec = new Opportunity_Subgroup__c(Active__c = true, Opportunity_Group__c = er.Id, Unit_Price__c = 100, Select_This_SubGroup_For_Opportunity__c = true);
        insert spec;
        Test.startTest();
        er.Quantity__c = 15;
        update er;
        Test.stopTest();
        opportunity =[SELECT Id, Name, Amount FROM Opportunity WHERE Id = :opportunity.Id];
        System.assert(!ApexPages.hasMessages(), 'Hay mensajes de error!');
        System.assertEquals(1500, opportunity.Amount, 'La oportunidad no fué actualizada. ' + opportunity.Name);
    }
}