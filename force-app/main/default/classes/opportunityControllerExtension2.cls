/**
	Author		: Henry Caballero @fullOpp
	Date		: 9/5/2018
	Description : build a "equipment pricing/opp specs" request hierarchy
**/

public class opportunityControllerExtension2 
{    
    private id Oppid;
    private Set<String> proFamily = new Set<String>{'Trailer', 'Auto Hauler' ,'Truck','Tractor'};
    public list<Equipment_Pricing> oppgroup {get;set;}

	public opportunityControllerExtension2(ApexPages.StandardController controller) 
    {
        Oppid = controller.getId();
        getoppgroup(); 
    }
        
    private void getoppgroup() 
    {   
        set<id> setOfQuotes = new set<id>();        
        oppgroup = new list<Equipment_Pricing> ();
        list<QuoteLineItem> qlines = new list<QuoteLineItem>();
        map<id, map<string, QuoteLineItem>> spect_attributesMAP = new map<id, map<string, QuoteLineItem>>();

        // get the Equipment Pricing Requests with theirs opportunity specs
        list<Opportunity_Group__c> ogs = [SELECT id, Name, Opportunity__c,Quantity__c,Equipment_Pricing_Total__c, Tractor_Type__c,Trailer__c,
                (SELECT id, Name, Model__c, Type__c, Equipment_Type__c, 
                RecordType.Name,
                Manufacturer__c,Year__c, Axle__c, OEM_Manufacturer__c, Equipment_Pricing_Request__c,Unit_Price__c,Select_This_SubGroup_For_Opportunity__c,EqPricing_Subtype__c
                    FROM LightningSpecs__r  ) 
                FROM Opportunity_Group__c 
                WHERE Opportunity__c =:Oppid];
                   
        // create a set of opportunity specs in order to get the required spec attributes
        for (Opportunity_Group__c og : ogs )
        {
            for (quote q : og.LightningSpecs__r)
            {
                setOfQuotes.add(q.id);
            }
        }        
        // get the quotelines for those products are spec attibutes then we put them into a map
        if (!setOfQuotes.isEmpty())
             qlines = [select quoteid, Product2Id, Product2.Family, Product2.name, Product2.Manufacturer__c from QuoteLineItem where Product2.Family in : proFamily and quoteid in :setOfQuotes ];

         for (QuoteLineItem qli: qlines )   
         {
             spect_attributesMAP.put(qli.QuoteId, new map <string,QuoteLineItem >{ 'OEM'=> qli} );
         }

        //prepare output data
         for (Opportunity_Group__c og : ogs )
         {
            Equipment_Pricing ep  = new Equipment_Pricing();
            list<opportunity_spec> opp_specs = new list<opportunity_spec>();
            ep.epr = og;
            for (quote q : og.LightningSpecs__r)
            {
                QuoteLineItem OEM = spect_attributesMAP.containsKey(q.id) ? spect_attributesMAP.get(q.id).get('OEM') : null;
                opp_specs.add(new opportunity_spec (q,OEM ) ) ;

            }
            
            ep.opp_specs =opp_specs;

            oppgroup.add(ep);
         }

                    
    }




    public class Equipment_Pricing
    {
        public Opportunity_Group__c epr {get;set;}
        public list<opportunity_spec> opp_specs {get;set;}
    }

    public class opportunity_spec
    {
        public quote    opp_spec {get;set;}
        public QuoteLineItem   OEM      {get;set;}

        public opportunity_spec (quote opp_spec, QuoteLineItem OEM )
        {
            this.OEM = OEM;
            this.opp_spec = opp_spec;
        }

    }


}