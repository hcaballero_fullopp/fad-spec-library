@isTest
private class LastContactDatesTest {

	static testMethod void myUnitTest() {
		Account account = new Account(Name = 'Test Account');
		insert account;
		Contact financeContact = new Contact(FirstName = 'Peter', LastName = 'Smith', COntact_Type__c = 'Finance/Treasury', AccountId = account.Id);
		insert financeContact;
		Contact operationsContact = new Contact(FirstName = 'Marta', LastName = 'Sanchez', Contact_Type__c = 'Operations', AccountId = account.Id);
		insert operationsContact;
		Onsite_Call__c financeCall = new Onsite_Call__c(Date_of_Onsite__c = Date.today(), Account__c = account.Id, Contact__c = financeContact.Id);
		Onsite_Call__c operationsCall = new Onsite_Call__c(Date_of_Onsite__c = Date.today(), Account__c = account.Id, Contact__c = operationsContact.Id);
		Test.startTest();
		insert financeCall;
		insert operationsCall;
		Test.stopTest();
		account = [SELECT Onsite_with_Operations__c, Onsite_with_Finance__c FROM Account WHERE Id = :account.Id];
		financeCall = [SELECT Contact_Type__c, Date_of_Onsite__c, Date_of_Finance_Contact__c FROM Onsite_Call__c WHERE Id = :financeCall.Id];
		operationsCall = [SELECT Contact_Type__c, Date_of_Operations_Contact__c, Date_of_Onsite__c FROM Onsite_Call__c WHERE Id = :operationsCall.Id];
		System.assert(financeCall.Contact_Type__c.contains('Finance'), 'Finance call does not contains finance contact');
		System.assert(operationsCall.Contact_Type__c.contains('Operations'), 'OperationsCall does not contains operations');
		System.assertEquals(financeCall.Date_of_Onsite__c, financeCall.Date_of_Finance_Contact__c, 'The date formula of finance contact does not match');
		System.assertEquals(operationsCall.Date_of_Onsite__c, operationsCall.Date_of_Operations_Contact__c, 'Date formula of Operations Contact does not match');
		System.assertEquals(account.Onsite_with_Finance__c, financeCall.Date_of_Onsite__c, 'Finance date does not match');
		System.assertEquals(account.Onsite_with_Operations__c, operationsCall.Date_of_Onsite__c, 'Operations date does not match');
    }
}