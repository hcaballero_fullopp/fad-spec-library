/**
    Author  : Henry Caballero @fullOpp
    Date    : 9/2/2018
**/
@istest
public class TestDataFactory
{
    public static account createAccount(){
        return new account (Name='Test');
    }

    public static opportunity createOpportunity(){
        account acc = createAccount();
        insert acc;

        return new Opportunity (Name='test', AccountId = acc.id, StageName='4B- Lease Pricing & Proposal', CloseDate= System.today());
    }

    public static quote createQuote(){
        opportunity opp = createOpportunity();
        insert opp;

        quote quo = new quote();
        quo.Name = 'test';
        quo.OpportunityId = opp.Id;
        return quo;
    }
    public static Product2 createProduct2(){
        return new Product2 (Name='Moto - G1',family = 'Tractor', isActive=true);
    }

    //------------Start Only for the EquipmentPricingHelper class-------------
    public static List<quote> createQuoteEquipment(){
        opportunity opp = createOpportunity();
        insert opp;

        List<quote> quotlist = new List<quote>();

        quote quo = new quote();
        quo.Name = 'test';
        quo.OpportunityId = opp.Id;
        quotlist.add(quo);
        return quotlist;
    }
    public static List<Opportunity_Group__c> createOpportunitySubGroupEquipment(){
        List<Opportunity_Group__c> Opportunity_Grouplist = new List<Opportunity_Group__c>();

        Product2 pro = createProduct2();
        insert pro;

        opportunity opp = createOpportunity();
        insert opp;

        Opportunity_Group__c requests1 = new Opportunity_Group__c();
        requests1.Opportunity__c = opp.Id;
        requests1.Quantity__c = 1;
        requests1.Model_Year__c = '2018';
        requests1.Make__c = 'Autocar';
        requests1.Model_Desc__c = 'test';
        requests1.Sub_Type__c = 'Straight Truck';
        requests1.Spec_Type__c = 'FA Spec';
        requests1.Extended_Warranty__c = 'Yes';
        requests1.MPG_Spec__c = 'Performance';
        requests1.OBC_Pricing__c = 'No';

        insert requests1;

        Opportunity_SubGroup__c spec1 = new Opportunity_SubGroup__c();
        spec1.Opportunity_Group__c = requests1.Id;
        spec1.Save_for_Library__c = true;
        spec1.Engine_Warranty__c = 100;
        spec1.Safety_AdaptiveCruiseControl__c = true;
        spec1.Type__c = 'FA Spec';
        spec1.Description__c = 'Trailer Spec - Industry Vertical 1 - Option A';
        spec1.Save_for_Library__c = true;
        spec1.Year__c = '2019';
        spec1.OEM__c = pro.Id;
        spec1.Brakes_Front__c = 'Not Specified';
        spec1.Brakes_Rear__c = 'Not Specified';
        spec1.Refrigeratation_Type__c = 'Single-Temp';
        spec1.Body_Make__c = 'Hyundai 28';
        spec1.Dealer__c = 'Hyundai Translead';
        spec1.Date_Sent__c = date.today();
        spec1.Date_Received__c = date.today();
        spec1.List_Price__c = 46100;
        spec1.Upfit_Description_1__c = 'Reefer';
        spec1.Upfit_1_Cost__c = 46100;
        spec1.Upfit_1_InvoiceMethod__c = 'Create Separate Invoice for Upfitting';
        spec1.Upfit_Description_2__c = 'Liftgate';
        spec1.Upfit_2_Cost__c = 0;
        spec1.Upfit_2_InvoiceMethod__c = 'N/A';
        spec1.Unit_Price__c = 45635;
        spec1.FA_Spec_Name__c = 'Hyundai 28 ST reefer trailer w/LG';
        spec1.Spec_Pricing_Good_Until__c = date.today();
        insert spec1;

        Opportunity_Group__c requests = new Opportunity_Group__c();
        requests.Spec_Library__c = spec1.id;
        requests.Opportunity__c = opp.Id;
        requests.Quantity__c = 1;
        requests.Model_Year__c = '2018';
        requests.Make__c = 'Autocar';
        requests.Model_Desc__c = 'test';
        requests.Sub_Type__c = 'Straight Truck';
        requests.Spec_Type__c = 'FA Spec';
        requests.Extended_Warranty__c = 'Yes';
        requests.MPG_Spec__c = 'Performance';
        requests.OBC_Pricing__c = 'No';
        Opportunity_Grouplist.add(requests);
        return Opportunity_Grouplist;
    }

    public static List<Opportunity_SubGroup__c> createOpportunitySubGroupOpportuniEquipment(){

        List<Opportunity_SubGroup__c> Opportunity_SubGrouplist = new List<Opportunity_SubGroup__c>();
        Product2 pro = createProduct2();
        insert pro;

        Pricebook2 pb = new Pricebook2();
        pb.Id = Test.getStandardPricebookId();
        update pb;

        system.debug('----Pricebook in test class---->>>>'+pb);
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = pro.Id;
        pbe.Pricebook2Id = pb.Id;
        pbe.UnitPrice = 100;
        pbe.IsActive = true;
        insert pbe;
        system.debug('pbe in test class---->>>'+pbe);

        opportunity opp = createOpportunity();
        insert opp;

        Opportunity_Group__c requests = createOpportunitySubGroup();
        insert requests;

        Opportunity_SubGroup__c spec1 = new Opportunity_SubGroup__c();
        spec1.Engine_Warranty__c = 100;
        spec1.Opportunity_Group__c = requests.Id;
        spec1.Safety_AdaptiveCruiseControl__c = true;
        spec1.Type__c = 'FA Spec';
        spec1.Description__c = 'Trailer Spec - Industry Vertical 1 - Option A';
        spec1.Save_for_Library__c = true;
        spec1.Year__c = '2019';
        spec1.OEM__c = pro.Id;
        spec1.Brakes_Front__c = 'Not Specified';
        spec1.Brakes_Rear__c = 'Not Specified';
        spec1.Refrigeratation_Type__c = 'Single-Temp';
        spec1.Body_Make__c = 'Hyundai 28';
        spec1.Dealer__c = 'Hyundai Translead';
        spec1.Date_Sent__c = date.today();
        spec1.Date_Received__c = date.today();
        spec1.List_Price__c = 46100;
        spec1.Upfit_Description_1__c = 'Reefer';
        spec1.Upfit_1_Cost__c = 46100;
        spec1.Upfit_1_InvoiceMethod__c = 'Create Separate Invoice for Upfitting';
        spec1.Upfit_Description_2__c = 'Liftgate';
        spec1.Upfit_2_Cost__c = 0;
        spec1.Upfit_2_InvoiceMethod__c = 'N/A';
        spec1.Unit_Price__c = 45635;
        spec1.FA_Spec_Name__c = 'Hyundai 28 ST reefer trailer w/LG';
        spec1.Spec_Pricing_Good_Until__c = date.today();
        Opportunity_SubGrouplist.add(spec1);
        return Opportunity_SubGrouplist;
    }
    //--------End Only for the EquipmentPricingHelper class--------

    public static Opportunity_Group__c createOpportunitySubGroup(){
        Contact con = new Contact();
        con.LastName = 'Test Contact';
        insert con;

        Product2 pro = createProduct2();
        insert pro;
        Pricebook2 pb = new Pricebook2();

        pb.Id = Test.getStandardPricebookId();

        update pb;

        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = pro.Id;
        pbe.Pricebook2Id = pb.Id;
        pbe.UnitPrice = 100;
        pbe.IsActive = true;
        insert pbe;
        system.debug('pbe in test class12---->>>'+pbe);

        opportunity opp = createOpportunity();
        insert opp;

        Opportunity_Group__c requests1 = new Opportunity_Group__c();
        requests1.Opportunity__c = opp.Id;
        requests1.Quantity__c = 1;
        requests1.Model_Year__c = '2018';
        requests1.Make__c = 'Autocar';
        requests1.Model_Desc__c = 'test';
        requests1.Sub_Type__c = 'Straight Truck';
        requests1.Spec_Type__c = 'FA Spec';
        requests1.Extended_Warranty__c = 'Yes';
        requests1.MPG_Spec__c = 'Performance';
        requests1.OBC_Pricing__c = 'No';

        insert requests1;
        quote quo = new quote();
        quo.Name = 'test';
        quo.OpportunityId = opp.Id;
        quo.Model__c = '8100';
        quo.Type__c = 'FA Spec';
        quo.Equipment_Type__c = 'Trailer';
        quo.Manufacturer__c='Cotrell';
        quo.Year__c = '2019';
        quo.Axle__c = '4x2';
        quo.OEM_Manufacturer__c = 'accuride';
        quo.Equipment_Pricing_Request__c = requests1.Id;
        quo.Unit_Price__c = 100;
        quo.Select_This_Opportunity_spec_for_LPAF__c = true;
        quo.OpportunityId = opp.Id;
        insert quo;

        Lease_Proposal_Acceptance_Form__c lpaf = new Lease_Proposal_Acceptance_Form__c();
        lpaf.LPAF_Quantity__c = 10;
        lpaf.Lease_Type__c = 'FMV - OPERATING LEASE';
        lpaf.Payment_Option__c = 'ADVANCE';
        lpaf.MPY__c = 12;
        lpaf.HPY__c = 32;
        lpaf.Est_Delivery__c = Date.Today();
        lpaf.Term_Mos__c = 3;
        lpaf.Ext_Term_Years__c = 18;
        lpaf.ExchangeIT_Month__c = 2018;
        lpaf.Quote__c = quo.Id;
        lpaf.Opportunity__c = opp.Id;
        lpaf.Due__c = Date.Today().addDays(4);
        lpaf.Primary_Contact__c = con.Id;
        lpaf.Notes__c = 'Notes Text';
        insert lpaf;


        Opportunity_SubGroup__c spec1 = new Opportunity_SubGroup__c();
        spec1.Opportunity_Group__c = requests1.Id;
        spec1.Save_for_Library__c = true;
        spec1.Engine_Warranty__c = 100;
        spec1.Safety_AdaptiveCruiseControl__c = true;
        spec1.Type__c = 'FA Spec';
        spec1.Description__c = 'Trailer Spec - Industry Vertical 1 - Option A';
        spec1.Save_for_Library__c = true;
        spec1.Year__c = '2019';
        spec1.OEM__c = pro.Id;
        spec1.Brakes_Front__c = 'Not Specified';
        spec1.Brakes_Rear__c = 'Not Specified';
        spec1.Refrigeratation_Type__c = 'Single-Temp';
        spec1.Body_Make__c = 'Hyundai 28';
        spec1.Dealer__c = 'Hyundai Translead';
        spec1.Date_Sent__c = date.today();
        spec1.Date_Received__c = date.today();
        spec1.List_Price__c = 46100;
        spec1.Upfit_Description_1__c = 'Reefer';
        spec1.Upfit_1_Cost__c = 46100;
        spec1.Upfit_1_InvoiceMethod__c = 'Create Separate Invoice for Upfitting';
        spec1.Upfit_Description_2__c = 'Liftgate';
        spec1.Upfit_2_Cost__c = 0;
        spec1.Upfit_2_InvoiceMethod__c = 'N/A';
        spec1.Unit_Price__c = 45635;
        spec1.FA_Spec_Name__c = 'Hyundai 28 ST reefer trailer w/LG';
        spec1.Spec_Pricing_Good_Until__c = date.today();
        insert spec1;

        Opportunity_Group__c requests = new Opportunity_Group__c();
        requests.Spec_Library__c = spec1.id;
        requests.Opportunity__c = opp.Id;
        requests.Quantity__c = 1;
        requests.Model_Year__c = '2018';
        requests.Make__c = 'Autocar';
        requests.Model_Desc__c = 'test';
        requests.Sub_Type__c = 'Straight Truck';
        requests.Spec_Type__c = 'FA Spec';
        requests.Extended_Warranty__c = 'Yes';
        requests.MPG_Spec__c = 'Performance';
        requests.OBC_Pricing__c = 'No';

        return requests;
    }
    public static Opportunity_SubGroup__c createOpportunitySubGroupOpportuni(){
        Product2 pro = createProduct2();
        insert pro;

        opportunity opp = createOpportunity();
        insert opp;

        Opportunity_Group__c requests = createOpportunitySubGroup();
        insert requests;

        Opportunity_SubGroup__c spec1 = new Opportunity_SubGroup__c();
        spec1.Engine_Warranty__c = 100;
        spec1.Opportunity_Group__c = requests.Id;
        spec1.Safety_AdaptiveCruiseControl__c = true;
        spec1.Type__c = 'FA Spec';
        spec1.Description__c = 'Trailer Spec - Industry Vertical 1 - Option A';
        spec1.Save_for_Library__c = true;
        spec1.Year__c = '2019';
        spec1.OEM__c = pro.Id;
        spec1.Brakes_Front__c = 'Not Specified';
        spec1.Brakes_Rear__c = 'Not Specified';
        spec1.Refrigeratation_Type__c = 'Single-Temp';
        spec1.Body_Make__c = 'Hyundai 28';
        spec1.Dealer__c = 'Hyundai Translead';
        spec1.Date_Sent__c = date.today();
        spec1.Date_Received__c = date.today();
        spec1.List_Price__c = 46100;
        spec1.Upfit_Description_1__c = 'Reefer';
        spec1.Upfit_1_Cost__c = 46100;
        spec1.Upfit_1_InvoiceMethod__c = 'Create Separate Invoice for Upfitting';
        spec1.Upfit_Description_2__c = 'Liftgate';
        spec1.Upfit_2_Cost__c = 0;
        spec1.Upfit_2_InvoiceMethod__c = 'N/A';
        spec1.Unit_Price__c = 45635;
        spec1.FA_Spec_Name__c = 'Hyundai 28 ST reefer trailer w/LG';
        spec1.Spec_Pricing_Good_Until__c = date.today();

        return spec1;
    }

    public static pricebook2 createNewPriceBook(){

        return new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
    }

    public static PricebookEntry createPricebookEntry(){

        Product2 prod = new Product2(Name = 'Laptop X200',
                Family = 'Tractor');
        insert prod;

        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();

        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
        return standardPrice;
    }

    public static product2 createProduct (String proFamily) {
        Product2 prod = new Product2(Name = 'Laptop X2000',
                Family = proFamily);
        insert prod;
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();

        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        return prod;
    }

    public static void createOpportunitySpecWithOpp (opportunity opp){
        insert opp;

        Opportunity_Group__c requests1 = new Opportunity_Group__c();
        requests1.Opportunity__c = opp.Id;
        requests1.Quantity__c = 1;
        requests1.Model_Year__c = '2018';
        requests1.Make__c = 'Autocar';
        requests1.Model_Desc__c = 'test';
        requests1.Sub_Type__c = 'Straight Truck';
        requests1.Spec_Type__c = 'FA Spec';
        requests1.Extended_Warranty__c = 'Yes';
        requests1.MPG_Spec__c = 'Performance';
        requests1.OBC_Pricing__c = 'No';
        insert requests1;


        quote quo = new quote();
        quo.Name = 'test';
        quo.OpportunityId = opp.Id;
        quo.Model__c = '8100';
        quo.Type__c = 'FA Spec';
        quo.Equipment_Type__c = 'Trailer';
        quo.Manufacturer__c='Cotrell';
        quo.Year__c = '2019';
        quo.Axle__c = '4x2';
        quo.OEM_Manufacturer__c = 'accuride';
        quo.Equipment_Pricing_Request__c = requests1.Id;
        quo.Unit_Price__c = 100;
        quo.Select_This_Opportunity_spec_for_LPAF__c = true;
        quo.OpportunityId = opp.Id;
        insert quo;
    }

}