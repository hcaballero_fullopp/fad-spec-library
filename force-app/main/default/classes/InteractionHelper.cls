public with sharing class InteractionHelper {
    public List<InteractionController.aPresentation> presentations {get;set;}
    public List<InteractionController.aContact> contacts {get;set;}
    public List<InteractionController.aIntContact> intcontacts {get;set;}
    
    public List<OnSiteCall_Presentation__c> presentationslist = New List<OnSiteCall_Presentation__c>();
    public List<Attendees__c> attendeeslist = New List<Attendees__c>();
    
    public boolean step1 {get;set;}
    public boolean step2 {get;set;}
    public boolean step3 {get;set;}
    public boolean step4 {get;set;}
    public boolean step5 {get;set;}
    
    public List<Id> selectedAccounts {get;set;}
    public List<Id> selectedContacts {get;set;}
    public List<Id> selectedInternalContacts {get;set;}
    public List<Id> selectedPresentations {get;set;}

    public String accountID {get;set;}
    public String opportunityId {get;set;}
    
    Interaction_Wizard_Settings__c config = Interaction_Wizard_Settings__c.getValues('Config');
    string FAAccountId = config.FA_Account_Id__c;
    
    public InteractionHelper(){
        
    }
    
    public Onsite_Call__c OnsiteCall;
    
    public Onsite_Call__c getOnsiteCall() {
        if(OnsiteCall == null) OnsiteCall = new Onsite_Call__c();
        if(String.isNotBlank(accountID)){
            OnsiteCall.Account__c = accountID;
            Address AccAdd = [SELECT Id, Name, RecordTypeId, BillingAddress FROM Account where id = :accountID].BillingAddress;
            if(AccAdd!=null){
                if(AccAdd.getStreet()!=null) OnsiteCall.Location__c = AccAdd.getStreet();
                if(AccAdd.getCity()!=null) OnsiteCall.Location__c = OnsiteCall.Location__c +' '+ AccAdd.getCity()+',';
                if(AccAdd.getState()!=null) OnsiteCall.Location__c = OnsiteCall.Location__c +' '+ AccAdd.getState();
                if(AccAdd.getPostalCode()!=null) OnsiteCall.Location__c = OnsiteCall.Location__c +' '+ AccAdd.getPostalCode();
                if(AccAdd.getCountry()!=null) OnsiteCall.Location__c = OnsiteCall.Location__c +' '+ AccAdd.getCountry();
            }
        }
        if(String.isNotBlank(opportunityId)) OnsiteCall.Opportunity__c = opportunityId;
        return OnsiteCall;
    }
    
    public void init(){
        contacts = new List<InteractionController.aContact>();
        intcontacts = new List<InteractionController.aIntContact>();
        presentations = new List<InteractionController.aPresentation>();
        selectedAccounts = new List<Id>();
        selectedContacts = new List<Id>();
        selectedInternalContacts = new List<Id>();
        selectedPresentations = new List<Id>();
        step1();
    }
    
    public void step1(){
        step1 = true;
        step2 = false;
        step3 = false;
        step4 = false;
        step5 = false;

    }
    
    public void step2(){
        step1 = false;
        step2 = true;
        step3 = false;
        step4 = false;
        step5 = false;
        
        if (presentations.isEmpty()){
            for(Presentation__c c : [SELECT Id, Name, Date_Presented__c, Opportunity__c, Assigned_To__c, Cancellation_Reason__c, Completed__c, Data_Checklist_ID__c, Due__c, Next_Meeting__c, Presentation_Requested__c, Presented_to_Finance__c, Presented_at_Onsite_Call__c, Related_to_Account__c, BDEX__c, Customer_Contact__c, notes__c, Requested_Date__c, Upload_Presentation__c, Age__c, RecordTypeId, RecordType.Name FROM Presentation__c where Related_to_Account__c = :accountID and (RecordType.Name = 'Standard Presentations' Or (RecordType.Name = 'Presentations to Complete' And Completed__c != null))]){
                presentations.add(new InteractionController.aPresentation(c));
            }
        }
        
    }
    
    public void step3(){
        step1 = false;
        step2 = false;
        step3 = true;
        step4 = false;
        step5 = false;
        
        if (contacts.isEmpty()){
            for(Contact c : [select id, name, accountid, account.name,Email, Contact_Type__c from contact where accountId = :accountID]){
                contacts.add(new InteractionController.aContact(c));
            }
        }
        
        System.debug('Contacts = ' + contacts);
    }
    
    public void step4(){
        
        step1 = false;
        step2 = false;
        step3 = false;
        step4 = true;
        step5 = false;
        
        if (intcontacts.isEmpty()){
            for(Contact c : [select id, name, accountid, account.name,Email from contact where accountId = :FAAccountId]){
                intcontacts.add(new InteractionController.aIntContact(c));
            }
        }
        
        System.debug('intcontacts = ' + intcontacts);
        
    }
    
    public PageReference saveInteraction(){
        if(Onsitecall.Start_Date__c==null){
            Onsitecall.Start_Date__c=Date.today();
        }
        DateTime intStartDate = Onsitecall.Start_Date__c;
	 	Date intDate = date.newinstance(intStartDate.year(), intStartDate.month(), intStartDate.day());
        Onsitecall.Date_of_Onsite__c = intDate;
        if(Onsitecall.End_Date__c==null){
        	Onsitecall.End_Date__c=Onsitecall.Start_Date__c;
        }
        insert Onsitecall;
        
        for(InteractionController.aPresentation a : presentations){
            if(a.selected){
                selectedPresentations.add(a.val.id);
                OnSiteCall_Presentation__c ascp = New OnSiteCall_Presentation__c();
                ascp.OnSiteCall__c = Onsitecall.Id;
                ascp.Presentation__c = a.val.id;
                if(Onsitecall.Opportunity__c!=null){
                    ascp.Opportunity__c = Onsitecall.Opportunity__c;
                }
                ascp.Presented_Date__c = Onsitecall.Date_of_Onsite__c;
                presentationslist.add(ascp);
            }
        }
        insert presentationslist;
        
        system.debug('step 2: '+selectedPresentations);        
        
        for(InteractionController.aContact a : contacts){
            if(a.selected){
                selectedContacts.add(a.val.id);
                Attendees__c att = New Attendees__c();
                att.On_Site_Call__c = Onsitecall.Id;
                att.Contact__c = a.val.id;
                att.Attended__c = true;
                attendeeslist.add(att);
            }
        }
        system.debug('step 3: '+selectedContacts);                
        
        for(InteractionController.aIntContact a : intcontacts){
            if(a.selected){
                selectedInternalContacts.add(a.val.id);
                Attendees__c attfa = New Attendees__c();
                attfa.On_Site_Call__c = Onsitecall.Id;
                attfa.Contact__c = a.val.id;
                attfa.Attended__c = true;
                attendeeslist.add(attfa);
            }
        }
        insert attendeeslist;
        system.debug('step 4: '+selectedInternalContacts);                
                
        Schema.DescribeSObjectResult result = Onsite_Call__c.SObjectType.getDescribe();
        PageReference pageRef = new PageReference('/' + Onsitecall.Id);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
}