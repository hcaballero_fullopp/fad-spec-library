public with sharing class ValidationUtils {
	
	public static void validateRequiredFields(String fieldSetName, Sobject obj) {
		validateRequiredFields(fieldSetName, obj, null); 
	}
	
	public static void validateRequiredFields(String fieldSetName, Sobject obj, String additionalMessage) {
		if (obj != null) {
			List<Schema.FieldSetMember> fields = ReflectUtils.getFields(obj.getSobjectType(), fieldSetName);
			for (Schema.FieldSetMember field : fields) {
				Object value = (Object)obj.get(field.getFieldPath());
				if (value == null || String.valueOf(value).trim() == '')
					obj.addError(' The field ' + field.getLabel() + ' is required.' + (additionalMessage == null ? '' : additionalMessage));
			}
		}
	}
}