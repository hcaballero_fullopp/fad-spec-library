global class LastInteractionSchedule implements Schedulable{
    global void execute(SchedulableContext sc){
        
		Id batchInstanceId = Database.executeBatch(new LastInteractionBatch(), 200);
        System.debug('Batch id -> ' + batchInstanceId);
        
	}
}