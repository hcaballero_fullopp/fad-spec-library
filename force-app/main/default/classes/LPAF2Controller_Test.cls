@isTest
private class LPAF2Controller_Test {
    @isTest
    public static void initSpecs_Test(){
        test.startTest();
        Contact con = new Contact();
        con.LastName = 'test';
        insert con;

        opportunity opp = TestDataFactory.createOpportunity();
        insert opp;
        Opportunity_Group__c oppgroup = TestDataFactory.createOpportunitySubGroup();
        insert oppgroup;

        quote quo = new quote();
        quo.Name = 'test';
        quo.OpportunityId = opp.Id;
        quo.Model__c = '8100';
        quo.Type__c = 'FA Spec';
        quo.Equipment_Type__c = 'Trailer';
        quo.Manufacturer__c='Cotrell';
        quo.Year__c = '2019';
        quo.Axle__c = '4x2';
        quo.OEM_Manufacturer__c = 'accuride';
        quo.Equipment_Pricing_Request__c = oppgroup.Id;
        quo.Unit_Price__c = 100;
        quo.Select_This_Opportunity_spec_for_LPAF__c = true;
        insert quo;

        Lease_Proposal_Acceptance_Form__c lpaf = new Lease_Proposal_Acceptance_Form__c();
        lpaf.LPAF_Quantity__c = 10;
        lpaf.Lease_Type__c = 'FMV - OPERATING LEASE';
        lpaf.Payment_Option__c = 'ADVANCE';
        lpaf.MPY__c = 12;
        lpaf.HPY__c = 32;
        lpaf.Est_Delivery__c = Date.Today();
        lpaf.Term_Mos__c = 3;
        lpaf.Ext_Term_Years__c = 18;
        lpaf.ExchangeIT_Month__c = 2018;
        lpaf.Quote__c = quo.Id;
        lpaf.Opportunity__c = opp.Id;
        lpaf.Due__c = Date.Today().addDays(4);
        lpaf.Primary_Contact__c = con.Id;
        lpaf.Notes__c = 'Notes Text';
        insert lpaf;
        List<quote> specs  = new List<Quote>([select LPAF_Page_Header__c,(Select Id,LPAF_Quantity__c,Lease_Type__c,Payment_Option__c,MPY__c,HPY__c,Est_Delivery__c,
                Term_Mos__c,Ext_Term_Years__c,ExchangeIT_Month__c,Quote__c,Opportunity__c,Due__c,Primary_Contact__c,Notes__c from LPAFs__r) from quote where OpportunityId =: opp.Id]);

        system.debug('specs--->>>'+specs);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        LPAF2Controller ext = new LPAF2Controller(sc);
        System.currentPageReference().getParameters().put('specId', quo.Id);
        ext.addNewRowToLPAFList();
        ext.deleteRecord();
        ext.save();
        ext.cancel();
        test.stopTest();
    }
}