public class LastInteractionDate {
    public static void CalcLastInteraction(Set<Id> AccIdList) {
        System.debug('-------------->CalcLastInteraction');
        AggregateResult[] AccInteractionDate = [SELECT Account__c Id, Max(Date_of_Onsite__c) dt FROM Onsite_Call__c Where Date_of_Onsite__c <= TODAY And Account__c in:AccIdList GROUP By Account__c ];
        
		List<Date> dateOnsite = new List<Date>();
        
        Account[] AccList = [SELECT Id, Last_Completed_Interaction_Date__c, Last_Completed_Interaction__c
                             FROM Account Where Id in :AccIdList];
        
        Map<Id,Date> MapAccTime=new Map<Id,Date>();
        Map<String,Id> MapAccLastInteraction=new Map<String,Id>();
        
        for(AggregateResult ar : AccInteractionDate){
            MapAccTime.put((Id)ar.get('Id'),(Date)ar.get('dt'));
			dateOnsite.add((Date)ar.get('dt'));
        }
        
		AggregateResult[] onCallList =[SELECT Account__c acc, Date_of_Onsite__c dt, Id Last_Completed_Interaction__c FROM Onsite_Call__c Where Date_of_Onsite__c <= TODAY And Account__c in:AccIdList And Date_of_Onsite__c in: dateOnsite GROUP By Account__c, Date_of_Onsite__c,Id ORDER BY Account__c,Id,Date_of_Onsite__c Desc];
        // [SELECT Account__c acc, Date_of_Onsite__c dt, Id Last_Completed_Interaction__c  FROM Onsite_Call__c Where Date_of_Onsite__c <= TODAY And Account__c in:AccIdList And Date_of_Onsite__c in: dateOnsite GROUP By Account__c, Date_of_Onsite__c,Id];
      
		for(AggregateResult ar : onCallList){
            if((Date)ar.get('dt')!=null && (Id)ar.get('acc')!=null ){
            	MapAccLastInteraction.put((Id)ar.get('acc')+''+(Date)ar.get('dt'),(Id)ar.get('Last_Completed_Interaction__c'));
            }
        }

		List<Account> updateAcc = New List<Account>();
        Id completedInteractionId;
        Date completedInteractionDt;
        for(Account acc :AccList){
            if(MapAccTime.get(acc.Id)!=null && MapAccLastInteraction.get(acc.Id+''+MapAccTime.get(acc.Id))!=null){		
                completedInteractionDt=MapAccTime.get(acc.Id);
                completedInteractionId=MapAccLastInteraction.get(acc.Id+''+MapAccTime.get(acc.Id));
                
                if(acc.Last_Completed_Interaction_Date__c!=completedInteractionDt || acc.Last_Completed_Interaction__c!=completedInteractionId){
                    acc.Last_Completed_Interaction_Date__c=completedInteractionDt;
                    acc.Last_Completed_Interaction__c=completedInteractionId;
                    updateAcc.add(acc);
                }
                
            }
		}
        if (updateAcc.size()>0) Update updateAcc;
        
        System.debug('updateAcc'+updateAcc);
    }
}