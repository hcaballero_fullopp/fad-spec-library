global class batchConvertSpectToOpportunitySpec implements Database.Batchable<SObject> {
	
	global batchConvertSpectToOpportunitySpec() 
    {
		delete [select id from quote];	
	}
		
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('select 	Opportunity_Group__c, Opportunity_Group__r.Opportunity__c, LPAF_Request__c, 	Original_Spec__c, 	Trailer_Body_Spec__c FROM Opportunity_SubGroup__c');
	}

	
   	global void execute(Database.BatchableContext context, List<Opportunity_SubGroup__c> scope) 
    {
        //variables
        Set<id> setOfSpecs = new Set<id> ();
        list<quote> quotes = new list<quote>();		
        
        for (Opportunity_SubGroup__c spec : scope)
        {
            setOfSpecs.add(spec.id);
        }
        
        // get the specs library details
        Map<Id, Opportunity_SubGroup__c> specLibraryMAP	=  equipmentPricingHelper.getSpecs(setOfSpecs)  ;
        
        //create a new quote for each Equipment Pricing
        for (Opportunity_SubGroup__c spec : scope)
        {
            if ( ! specLibraryMAP.containsKey(spec.id)  )
                continue;
        
            quotes.add(equipmentPricingHelper.createQuote(specLibraryMAP.get(spec.id) , spec.Opportunity_Group__r.Opportunity__c , spec.id, spec.Opportunity_Group__c));
        }		
        insert quotes;
        
        //equipmentPricingHelper.createQuoteLines(quotes,specLibraryMAP );
        
        Map<Id, quote> quoteMap= new Map<Id, quote>();
        for (quote q :quotes )
        {
            quoteMap.put ( q.Spec_Library__c , q);
        }
        
        List<Lease_Proposal_Acceptance_Form__c> lpafs = [ select id, Quote__c, Spec__c from Lease_Proposal_Acceptance_Form__c where spec__c!=null];
        
        for (Lease_Proposal_Acceptance_Form__c lpaf : lpafs )
        {
            if (quoteMap.containsKey(lpaf.Spec__c))
                lpaf.Quote__c = quoteMap.get(lpaf.Spec__c).id;
        }        
        update lpafs;
	}
		
	global void finish(Database.BatchableContext context) 
    {
		
	}
}