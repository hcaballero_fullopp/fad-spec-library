@isTest
private class AttachmentTest {

    static testMethod void presentationTest() {
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true);
        insert account;
        Opportunity opportunity = new Opportunity(StageName = 'Draft', CloseDate = Date.today(), Name = 'Test Opp', AccountId = account.Id);
        insert opportunity;
        Presentation__c presentation = new Presentation__c(Opportunity__c = opportunity.Id, Related_To_Account__c = account.Id);
        insert presentation;
        System.PageReference attachmentPage = Page.new_attachment;
        attachmentPage.getParameters().put('presentationId', presentation.Id);
        Test.setCurrentPageReference(attachmentPage);
        Attachment__c attachment = new Attachment__c();
        ApexPages.StandardController standard = new ApexPages.StandardController(attachment);
        AttachmentController controller = new AttachmentController(standard);
        controller.fileBody = Blob.valueOf('A123');
        controller.fileName = 'test.txt';
        Test.startTest();
        controller.save();
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(), ApexPages.getMessages());
        attachment = [SELECT Id, Presentation__c FROM Attachment__c WHERE Id = :attachment.Id];
        System.assertNotEquals(null, attachment.Presentation__c);
    }
    
    static testMethod void lpafTest() {
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true);
        insert account;
        Opportunity opportunity = new Opportunity(StageName = 'Draft', CloseDate = Date.today(), Name = 'Test Opp', AccountId = account.Id);
        insert opportunity;
        Contact contact = new Contact(FirstName = 'John', LastName = 'Doe');
        insert contact;
        Lease_Proposal_Acceptance_Form__c lpaf = new Lease_Proposal_Acceptance_Form__c(Opportunity__c = opportunity.Id, Primary_Contact__c = contact.Id);
        insert lpaf;
        System.PageReference attachmentPage = Page.new_attachment;
        attachmentPage.getParameters().put('lpafId', lpaf.Id);
        Test.setCurrentPageReference(attachmentPage);
        Attachment__c attachment = new Attachment__c();
        ApexPages.StandardController standard = new ApexPages.StandardController(attachment);
        AttachmentController controller = new AttachmentController(standard);
        controller.fileBody = Blob.valueOf('A123');
        controller.fileName = 'test.txt';
        Test.startTest();
        controller.save();
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(), ApexPages.getMessages());
        attachment = [SELECT Id, LPAF__c FROM Attachment__c WHERE Id = :attachment.Id];
        System.assertNotEquals(null, attachment.LPAF__c);
    }
    
    static testMethod void specTest() {
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true);
        insert account;
        Opportunity opportunity = new Opportunity(StageName = 'Draft', CloseDate = Date.today(), Name = 'Test Opp', AccountId = account.Id);
        insert opportunity;
        Opportunity_Group__c er = new Opportunity_Group__c(Opportunity__c = opportunity.Id);
        insert er;
        Opportunity_Subgroup__c spec = new Opportunity_Subgroup__c(Opportunity_Group__c = er.Id);
        insert spec;
        
        System.PageReference attachmentPage = Page.new_attachment;
        attachmentPage.getParameters().put('specId', spec.Id);
        Test.setCurrentPageReference(attachmentPage);
        Attachment__c attachment = new Attachment__c();
        ApexPages.StandardController standard = new ApexPages.StandardController(attachment);
        AttachmentController controller = new AttachmentController(standard);
        controller.fileBody = Blob.valueOf('A123');
        controller.fileName = 'test.txt';
        Test.startTest();
        controller.save();
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(), ApexPages.getMessages());
        attachment = [SELECT Id, Spec__c FROM Attachment__c WHERE Id = :attachment.Id];
        System.assertNotEquals(null, attachment.Spec__c);
    }
    
    static testMethod void opportunityTest() {
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true);
        insert account;
        Opportunity opportunity = new Opportunity(StageName = 'Draft', CloseDate = Date.today(), Name = 'Test Opp', AccountId = account.Id);
        insert opportunity;
                        System.PageReference attachmentPage = Page.new_attachment;
        attachmentPage.getParameters().put('opportunityId', opportunity.Id);
        Test.setCurrentPageReference(attachmentPage);
        Attachment__c attachment = new Attachment__c();
        ApexPages.StandardController standard = new ApexPages.StandardController(attachment);
        AttachmentController controller = new AttachmentController(standard);
        controller.fileBody = Blob.valueOf('A123');
        controller.fileName = 'test.txt';
        Test.startTest();
        controller.save();
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(), ApexPages.getMessages());
        attachment = [SELECT Id, Opportunity__c FROM Attachment__c WHERE Id = :attachment.Id];
        System.assertNotEquals(null, attachment.Opportunity__c);
    }
    
    static testMethod void accountTest() {
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true);
        insert account;
                        System.PageReference attachmentPage = Page.new_attachment;
        attachmentPage.getParameters().put('accountId', account.Id);
        Test.setCurrentPageReference(attachmentPage);
        Attachment__c attachment = new Attachment__c();
        ApexPages.StandardController standard = new ApexPages.StandardController(attachment);
        AttachmentController controller = new AttachmentController(standard);
        controller.fileBody = Blob.valueOf('A123');
        controller.fileName = 'test.txt';
        Test.startTest();
        controller.save();
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(), ApexPages.getMessages());
        attachment = [SELECT Id, Account__c FROM Attachment__c WHERE Id = :attachment.Id];
        System.assertNotEquals(null, attachment.Account__c);
    }
    
    /***
    static testMethod void wrongPresentationTest() {
        Account account = new Account(Name = 'Test Account', Approved_For_Opportunities__c = true);
        insert account;
        Opportunity opportunity = new Opportunity(StageName = 'Draft', CloseDate = Date.today(), Name = 'Test Opp', AccountId = account.Id);
        insert opportunity;
        Presentation__c presentation = new Presentation__c(Opportunity__c = opportunity.Id, Related_To_Account__c = account.Id);
        insert presentation;
        System.PageReference attachmentPage = Page.new_attachment;
        attachmentPage.getParameters().put('presentationId', presentation.Id);
        Test.setCurrentPageReference(attachmentPage);
        Attachment__c attachment = new Attachment__c();
        ApexPages.StandardController standard = new ApexPages.StandardController(attachment);
        AttachmentController controller = new AttachmentController(standard);
        controller.fileBody = Blob.valueOf('A123');
        controller.fileName = 'test.txt';
        Test.startTest();
        //Setting wrong presentation
        controller.attachment.Presentation__c = account.Id;
        controller.save();
        Test.stopTest();
        System.assert(ApexPages.hasMessages(), ApexPages.getMessages());
            }
***/
}