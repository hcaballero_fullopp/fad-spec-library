public class ActivitiesAndEventsExtension {

    private final Account acct;
    private Set<Id> AllIds;
    
    public ActivitiesAndEventsExtension(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        Id thisacct = this.acct.id;
        AllIds = new Set<Id>();
        AllIds.add(thisacct);
        }

    public List<Task> getRelatedTasks() {
        return [select Id, Subject, ActivityDate, Status, Owner.Name, What.Name, Account.name, Type, CallType from Task where AccountId in :AllIds Order By ActivityDate Desc];
    }

    public List<Event> getRelatedEvents() {
        return [select Id, Legacy_Calltype__c, Subject, Type, Event_Status__c, Owner.Name, What.Name, ActivityDate from Event where WhatId in :AllIds Order By ActivityDate Desc];
    }
}