global class LastInteractionBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    private Set<id> idList;
	
    global LastInteractionBatch(){
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        String q = 'Select Id';
        q += ' From Account';
        system.debug('SQL = ' + q);

      return Database.getQueryLocator(q);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
       Set<Id> AccIdList = new Set<Id>();
       for(sobject s : scope){
           AccIdList.add(s.Id);
       }
       LastInteractionDate.CalcLastInteraction(new Set<Id> (AccIdList));
    }

   global void finish(Database.BatchableContext BC){
   }

}