/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NormalizePostalCodeOnLeadTest {
	

	private static Lead lead;
	
	static {
						lead = new Lead(FirstName = 'Will', LastName = 'Smith', Company = 'Hollywood');
	}

    static testMethod void shortFormatTest() {
    	lead.PostalCode = '3506';
    	Test.startTest();
    	insert lead;
    	Test.stopTest();
    	lead = [SELECT PostalCode FROM Lead WHERE Id = :lead.Id];
    	System.assertEquals('03506', lead.PostalCode);
    }
    
    static testMethod void longFormatTest() {
    	lead.PostalCode = '3506-2512';
    	Test.startTest();
    	insert lead;
    	Test.stopTest();
    	lead = [SELECT PostalCode FROM Lead WHERE Id = :lead.Id];
    	System.assertEquals('03506-2512', lead.PostalCode);
    }
    
    static testMethod void wrongFormatTest() {
    	lead.PostalCode = '36-2512';
    	String message = null;
    	Test.startTest();
    	try {
    		insert lead;
    	} catch (Exception e) {
    		message = e.getMessage();
    	}
    	Test.stopTest();
    	System.assertNotEquals(null, message);
    }
}