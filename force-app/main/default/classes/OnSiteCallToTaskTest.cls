/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OnSiteCallToTaskTest {

	static testMethod void onInsertTest() {
		Account account = new Account (Name = 'Test Account');
		insert account;
		Contact contact = new Contact(FirstName = 'Christopher', LastName = 'Reeve', AccountId = account.Id);
		insert contact;
		Onsite_Call__c call = new Onsite_Call__c(Completed_by__c = UserInfo.getUserId(), Account__c = account.Id, Subject_or_Keywords__c = 'Testing trigger', Notes__c = 'This is a Description', Date_of_Onsite__c = Date.today(), Contact__c = contact.Id);
		Test.startTest();
		insert call;
		Test.stopTest();
		Task task = [SELECT Id, Type, OwnerId, Subject, Description, ActivityDate, WhoId, WhatId FROM Task WHERE Onsite_Call_Id__c = :call.Id];
		System.assertEquals(call.Completed_by__c, task.OwnerId);
		System.assertEquals(call.Account__c, task.WhatId);
		System.assertEquals(call.Subject_or_Keywords__c, task.Subject);
		System.assertEquals(call.Notes__c, task.Description);
		System.assertEquals(call.Date_of_Onsite__c, task.ActivityDate);
		System.assertEquals(call.Contact__c, task.WhoId);
		System.assertEquals('Onsite Call', task.Type);
	}
    
	static testMethod void onDeleteTest() {
		Account account = new Account (Name = 'Test Account');
		insert account;
		Contact contact = new Contact(FirstName = 'Christopher', LastName = 'Reeve', AccountId = account.Id);
		insert contact;
		Onsite_Call__c call = new Onsite_Call__c(Completed_by__c = UserInfo.getUserId(), Account__c = account.Id, Subject_or_Keywords__c = 'Testing trigger', Notes__c = 'This is a Description', Date_of_Onsite__c = Date.today(), Contact__c = contact.Id);
		insert call;
		Id callId = call.Id;
		Task task = [SELECT Id, OwnerId, Subject, Description, ActivityDate, WhoId, WhatId FROM Task WHERE Onsite_Call_Id__c = :callId];
		// task exists here
		Test.startTest();
		delete call;
		Test.stopTest();
		Integer count = [SELECT COUNT() FROM Task WHERE Onsite_Call_Id__c = :callId];
		System.assertEquals(0, count);
	}
}