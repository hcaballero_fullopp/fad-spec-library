@isTest
private class RequestLPAFControllerTest {
   
    static Opportunity_SubGroup__c spec1;
    static Opportunity opportunity;
    
    static void init() {
        Id clientId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'ACME', RecordTypeId = clientId);
        insert account;
        Contact contact = new Contact(FirstName = 'John', LastName = 'Doe');
        insert contact;
        opportunity = new Opportunity(Name = 'Test Opportunity', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
        insert opportunity;
        Opportunity_Group__c request = new Opportunity_Group__c(Opportunity__c = opportunity.Id);
        insert request;
        Lease_Proposal_Acceptance_Form__c lpaf = new Lease_Proposal_Acceptance_Form__c(Opportunity__c = opportunity.Id, Primary_Contact__c = contact.Id);
        insert lpaf;
        spec1 = new Opportunity_SubGroup__c(Opportunity_Group__c = request.Id, Engine_Warranty__c = 100, LPAF_Request__c = lpaf.Id);
        Opportunity_SubGroup__c spec2 = new Opportunity_SubGroup__c(Opportunity_Group__c = request.Id, Engine_Warranty__c = 200, LPAF_Request__c = lpaf.Id);
        insert spec1;
        insert spec2;
    }

    static testMethod void myUnitTest() {
        init();
        Test.startTest();
        ApexPages.StandardController standard = new ApexPages.StandardController(opportunity);
        RequestLPAFController controller = new RequestLPAFController(standard);
        controller.getInputLPAFFields();
        controller.getLabelSpecFields();
        controller.getInputSpecFields();
        controller.save();
        System.currentPageReference().getParameters().put('specId', spec1.Id);
        controller.cloneSpec();
        Test.stopTest(); 
    }
    
    static testMethod void leftPadTest() {
        String padded = RequestLPAFController.leftPad(null, '0', 3);
        System.assertEquals('000', padded, 'pad is incorrect');
        padded = RequestLPAFController.leftPad('1', '0', 3);
        System.assertEquals('001', padded, 'pad is incorrect');
        padded = RequestLPAFController.leftPad('1234', '0', 3);
        System.assertEquals('1234', padded, 'pad is incorrect');
    }
    
    static testMethod void nvlTest() {
        String nvl = RequestLPAFController.nvl(null);
        System.assertEquals('*', nvl, 'NVL is incorrect');
        nvl = RequestLPAFController.nvl(Decimal.valueOf(3), 3);
        System.assertEquals('003', nvl, 'NVL is incorrect');
        nvl = RequestLPAFController.nvl(Date.newInstance(2016,11,10));
        System.assertEquals('20161110', nvl, 'NVL is incorrect');
        nvl = RequestLPAFController.nvl('foo');
        System.assertEquals('foo', nvl, 'NVL is incorrect');
    }
    
    static testMethod void getKeyTest() {
        String key = RequestLPAFController.getKey((Opportunity_SubGroup__c)null);
        System.assertEquals('*', key, 'Key is incorrect');
        init();
        key = RequestLPAFController.getKey(spec1);
        System.assertEquals('*|*|******', key, 'Key is incorrect');
    }
    
    static testMethod void saveTest() {
        init();
        Test.startTest();
        ApexPages.StandardController standard = new ApexPages.StandardController(opportunity);
        RequestLPAFController controller = new RequestLPAFController(standard);
        for (Opportunity_SubGroup__c spec : controller.specs) {
            spec.Select_This_Spec_for_LPAF__c = true;
            spec.LPAF_Request__c = null;
        }
        controller.save();
                Test.stopTest();
    }
}