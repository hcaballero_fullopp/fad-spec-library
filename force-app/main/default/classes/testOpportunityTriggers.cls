@isTest
private class testOpportunityTriggers{

    static testMethod void testOpportunityAmount(){
    
    Account a = new Account();
         a.Name = 'Test';
         a.Total_Rating_Over_ride__c=100;
    a.Approved_For_Opportunities__c = true;
    insert a;
   
    
    Opportunity o = new Opportunity();
    o.Name = 'Test';
    o.AccountId = a.Id;
    o.StageName = '2 - Gathering Information';
    o.CloseDate = date.valueOf('2020-01-01');
    o.Amount = 0;
    o.Competitor__c = '...';
    insert o;
    
    Opportunity_Group__c og = new Opportunity_Group__c();
    og.Opportunity__c = o.Id;
    og.Quantity__c = 200;
    insert og;
    
    Opportunity_SubGroup__c osg = new Opportunity_SubGroup__c();
    osg.Opportunity_Group__c = og.Id;
    //osg.Name = 'test';
    osg.Amount__c = 200000; //This is overwritten by the system when a record is created
    osg.Unit_Price__c = 10000;
    insert osg;
    
    // Verify the Opportunity Amount is set to zero
    o = [select Id, Amount,Opportunity_Value__c  from Opportunity where Id = :o.Id];
    system.assertEquals(o.Opportunity_Value__c,o.Amount);
    
    // Now set the first subgroup Select flag to true
    osg.Select_This_SubGroup_For_Opportunity__c = true;
        osg.Active__c = true;
    update osg;
    
    // Verify the opportunity amount was updated by the trigger
    o = [select Id, Amount from Opportunity where Id = :o.Id];
    system.assertEquals(o.Amount,2000000);
    
    // Add another SubGroup
    Opportunity_SubGroup__c osg2 = new Opportunity_SubGroup__c();
    osg2.Opportunity_Group__c = og.Id;
    //osg2.Amount__c = 300000;
    osg2.Unit_Price__c = 15000;
    osg.Select_This_SubGroup_For_Opportunity__c=False;
        osg.Active__c = true;
    insert osg2;
    
    // Verify the Opportunity Amount was not updated
    o = [select Id, Amount from Opportunity where Id = :o.Id];
    //system.assertEquals(o.Amount,2000000);
    
    // Set the second subgroup to Selected and verify the Opportunity Amount gets updated
    osg.Select_This_SubGroup_For_Opportunity__c = false;
        osg.Active__c = true;
    update osg;
    
    osg2.Select_This_SubGroup_For_Opportunity__c = true;
        osg2.Active__c = true;
    update osg2;
    
    o = [select Id, Amount from Opportunity where Id = :o.Id];
    //system.assertEquals(o.Amount,3000000);
 
    }
    
     public static testMethod void testOpportunityControllerExtension() {
     
         Account a = new Account();
         a.Name = 'Test';
       a.Approved_For_Opportunities__c = true;
         a.Total_Rating_Over_ride__c=100;
         insert a;

   
         Opportunity o = new Opportunity();
         o.Name = 'Test';
         o.AccountId = a.Id;
         o.StageName = '2 - Gathering Information';
         o.CloseDate = date.valueOf('2020-01-01');
         o.LPAF_Due__c = date.valueOf('2014-01-01');
         o.Competitor__c = '...';
         //o.Amount = 0;
         insert o;
         
         Opportunity_Group__c og = new Opportunity_Group__c();
         og.Opportunity__c = o.Id;
         og.Quantity__c = 200;
         insert og;
        
         Opportunity_SubGroup__c osg = new Opportunity_SubGroup__c();
         osg.Opportunity_Group__c = og.Id;
         //osg.Amount__c = 200000;
         osg.Unit_Price__c = 10000;
         insert osg;
     
        ApexPages.StandardController opp = new ApexPages.StandardController(o);
        opportunityControllerExtension oppExtension  = new OpportunityControllerExtension(opp);
    }
 }