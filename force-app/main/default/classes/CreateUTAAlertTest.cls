/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CreateUTAAlertTest {

    static testMethod void myUnitTest() {
        Product2 product = new Product2(Name = 'Engine ABC', Family = 'Engine');
                insert product;
        UTA_Details__c detail = new UTA_Details__c(Approved__c = 1200, Component__c = product.Id, Status__c = 'Active');
        insert detail;
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'Test Opp', RecordTypeId = accountRecordTypeId);
        insert account;
        Opportunity opportunity = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = '0 - On Hold', AccountId = account.Id);
        insert opportunity;
        Opportunity_Group__c equipment = new Opportunity_Group__c(Opportunity__c = opportunity.Id);
        insert equipment;
        Opportunity_SubGroup__c spec = new Opportunity_SubGroup__c(Engine__c = product.Id, Opportunity_Group__c = equipment.Id);
                        Test.startTest();
        insert spec;
        Test.stopTest();
        UTA_Worksheet__c alert = [SELECT Id, Amount__c FROM UTA_Worksheet__c WHERE UTA_Detail__c = :detail.Id AND Spec_Formerly_SubGroup__c = :spec.Id];
        System.assertEquals(detail.Approved__c, alert.Amount__c);
            }
}