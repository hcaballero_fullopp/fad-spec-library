@isTest
public class testUpdatePresentationsonOpportunity{

    static testMethod void testPresentations(){
    
         Account a = new Account();
         a.Name = 'Test';
         a.Total_Rating_Over_ride__c=100;
         insert a;
    
         Opportunity o = new Opportunity();
         o.Name = 'Test';
         o.AccountId = a.Id;
         o.StageName = '2 - Gathering Information';
         o.CloseDate = date.valueOf('2020-01-01');
         o.LPAF_Due__c = date.valueOf('2014-01-01');
         o.Amount = 0;
         insert o;
             
         Opportunity_Group__c og = new Opportunity_Group__c();
         og.Opportunity__c = o.Id;
         og.Quantity__c = 200;
         insert og;
    
         Opportunity_SubGroup__c osg = new Opportunity_SubGroup__c();
         osg.Opportunity_Group__c = og.Id;
         //osg.Amount__c = 200000;
         osg.Unit_Price__c = 10000;
         insert osg;
    
    Presentation__c p = new Presentation__c();
    p.Related_to_Account__c = a.Id;
    p.Due__c = date.valueof('2020-01-01');
    insert p;
    
    // change something on the agreement and update
    p.Completed__c = date.valueOf('2020-01-02');
    update p;
    
    
    }
    
}