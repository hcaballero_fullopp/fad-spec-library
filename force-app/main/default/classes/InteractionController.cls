public with sharing class InteractionController /*extends PageControllerBase*/{
    
    public InteractionHelper sh {get; set;}
    public Id accountID {get; set;}
    public Id opportunityId {get; set;}
    
    public InteractionController(ApexPages.StandardController stdController){
        accountID =  ApexPages.currentPage().getParameters().get('AccId');
        opportunityId =  ApexPages.currentPage().getParameters().get('OppId');

        sh = new InteractionHelper();
        sh.init();
    }
    public InteractionController(){
        sh = new InteractionHelper();
        sh.init();
    }
    
    public class aContact {
        public Contact val { get; set; }
        public Boolean selected { get; set; }
        
        public InteractionController.aContact(Contact c){
            val = c;
            selected = false;
        }
    }
    
    public class aIntContact {
        public Contact val { get; set; }
        public Boolean selected { get; set; }
        
        public InteractionController.aIntContact(Contact c){
            val = c;
            selected = false;
        }
    }
    
    public class aPresentation {
        public Presentation__c val { get; set; }
        public Boolean selected { get; set; }
        
        public InteractionController.aPresentation(Presentation__c c){
            val = c;
            selected = false;
        }
    }    
}