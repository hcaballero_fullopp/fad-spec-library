/**
	Author		: Henry Caballero @fullOpp
	Date		: 9/3/2018
	Description : create the spec attributes ("Spec_Products__c") according to filled product lookups
**/

public class specHelper  
{

	static final Set<string> SpecProductFields = new Set<string> {
													'X5th_Wheel__c',
													'Engine__c',
													'Axle_LO__c',
													'Front_Suspension__c',
													'Front_Wheel__c',
													'OEM__c',
													'Rear_Axle__c',
													'Rear_Suspension__c',
													'Rear_Wheel__c',
													'Reefer__c',
													'Transmission__c'
												};

	public static void 	createSpecAttributes(List<Opportunity_SubGroup__c> specs)
	{
		List<Spec_Products__c> products = new List<Spec_Products__c>();

		for (Opportunity_SubGroup__c spec: specs )
		{
			for (string field : SpecProductFields)
			{
				id productId = (id)spec.get(field);
				if (productId==null)
					continue;

				products.add(new Spec_Products__c (Product__c = productId , Spec__c = spec.id) );	
			}
		}

		insert products;	
	}
    
    public static void preventUpdateProduct (Map<Id, Opportunity_SubGroup__c> newMap , Map<Id, Opportunity_SubGroup__c> oldMap , List<Opportunity_SubGroup__c> oppSubGroupList)
	{        
        for(Opportunity_SubGroup__c oppSubGroup : oppSubGroupList){
            for(string field : SpecProductFields) {
                if((oldMap.get(oppSubGroup.Id).get(field) != newMap.get(oppSubGroup.Id).get(field))){
                    oppSubGroup.put(field, oldMap.get(oppSubGroup.Id).get(field));                    
            	} 
            }
        }
    }

}