@isTest
private class EventsWithJohnFlynnTest {

    static testMethod void myUnitTest() {
    	Account account = new Account(Name = 'Test Account');
    	insert account;
        Event event = new Event(DurationInMinutes = 60, ActivityDatetime = Datetime.now(), John_Flynn_Presence_Requested__c = true, WhatId = account.Id);
        Test.startTest();
        insert event;
        Test.stopTest();
    }
}