@IsTest
private class InteractionWizardTest {
    static testMethod void InteractionControllerTest() {
        Test.startTest();

        
        Account acc = createAccount(true);

        Interaction_Wizard_Settings__c config = New Interaction_Wizard_Settings__c();
        config.Name = 'Config';
        config.FA_Account_Id__c = acc.Id;
        insert config;
        
        Contact contact = createContact(acc.Id, true);
        PageReference pageRef = Page.InteractionWizard;
        Test.setCurrentPage(pageRef);
        
        Onsite_Call__c interaction = New Onsite_Call__c();
        interaction.Account__c = acc.id;
        insert interaction;

        ApexPages.StandardController ic = new ApexPages.standardController(interaction);
        InteractionController controller = new InteractionController(ic);
        InteractionController controller2 = new InteractionController();   	        
        Test.stopTest();
    }

    static testMethod void testInteractionController_Steps() {
        Test.startTest();

        Account acc = createAccount(true);
        Contact contact = createContact(acc.Id, true);
        Opportunity opp = createOpp(acc.Id, true);
        Presentation__c pre = New Presentation__c();
        pre.Related_to_Account__c  = acc.Id;
        pre.Completed__c = system.today();
        insert pre;
        
        Interaction_Wizard_Settings__c config = New Interaction_Wizard_Settings__c();
        config.Name = 'Config';
        config.FA_Account_Id__c = acc.Id;
        insert config;
        
        PageReference pageRef = Page.InteractionWizard;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('AccId', acc.Id);
        ApexPages.currentPage().getParameters().put('OppId', opp.Id);

        InteractionController controller = new InteractionController();

        Onsite_Call__c inttest = controller.sh.getOnsiteCall();

        controller.sh.OnsiteCall.Start_Date__c=system.today();
        controller.sh.OnsiteCall.Account__c = acc.Id;

//        controller.accountID = acc.Id;
        controller.sh.accountID = acc.Id;

        //for(InteractionController.aAccount a : controller.sh.accounts){
        //    a.selected = true;
        //}
        controller.sh.init();

        controller.sh.step2();
        for(InteractionController.aPresentation a : controller.sh.presentations){
            a.selected = true;
        }

        controller.sh.step3();
        for(InteractionController.aContact c : controller.sh.contacts){
            c.selected = true;
        }

		controller.sh.step4();
        for(InteractionController.aIntContact d : controller.sh.intcontacts){
            d.selected = true;
        }

        PageReference p = controller.sh.saveInteraction();
        Test.stopTest();

    }

    static testMethod void testInteractionController_StepsNoDatesOpp() {
        Test.startTest();

        Account acc = createAccount(true);
        Contact contact = createContact(acc.Id, true);
        Opportunity opp = createOpp(acc.Id, true);
        Presentation__c pre = New Presentation__c();
        pre.Related_to_Account__c  = acc.Id;
        pre.Completed__c = system.today();
        insert pre;
        
        Interaction_Wizard_Settings__c config = New Interaction_Wizard_Settings__c();
        config.Name = 'Config';
        config.FA_Account_Id__c = acc.Id;
        insert config;
        
        PageReference pageRef = Page.InteractionWizard;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('AccId', acc.Id);
        ApexPages.currentPage().getParameters().put('OppId', opp.Id);

        InteractionController controller = new InteractionController();

        Onsite_Call__c inttest = controller.sh.getOnsiteCall();
        
        controller.sh.OnsiteCall.Account__c = acc.Id;

//        controller.accountID = acc.Id;
        controller.sh.accountID = acc.Id;

        //for(InteractionController.aAccount a : controller.sh.accounts){
        //    a.selected = true;
        //}
        controller.sh.init();

        controller.sh.step2();
        for(InteractionController.aPresentation a : controller.sh.presentations){
            a.selected = true;
        }

        controller.sh.step3();
        for(InteractionController.aContact c : controller.sh.contacts){
            c.selected = true;
        }

		controller.sh.step4();
        for(InteractionController.aIntContact d : controller.sh.intcontacts){
            d.selected = true;
        }

        PageReference p = controller.sh.saveInteraction();
        Test.stopTest();

    }
    
    
    private static Account createAccount(boolean save){
        Account acc = new Account(name = 'acc');

        if(save) insert acc;
        return acc;
    }

    private static Contact createContact(id account, boolean save){
        Contact c = new Contact();
        c.LastName = 'Name';
        c.FirstName = 'Name';
        c.Department = 'Name';
        c.Email = 'test@test.test';
        c.AccountId = account;
        if(save) insert c;
        return c;

    }

    private static Opportunity createOpp(id account, boolean save){
        Opportunity o = new Opportunity();
        o.Name = 'OppName';
        o.AccountId = account;
        o.StageName = '1- Discovery/FA Intro';
        o.CloseDate = system.today();        
        o.Amount=0;
        o.Type='New';
        o.AntDelDate__c=system.today();
        if(save) insert o;
        return o;

    }

    
}