// Author 		: Henry Caballero - fullOpp
// Date 		: 3/31/2017
// Description	: Update the Last Event Subject & Last Event Type fields on account.

public class lastEventOnAccountHandler 
{

    public static void updateLastEvent(list<event> events)
    {
        list<account> accounts = new  list<account> ();
        
        for (event e : events )
        {
            //check if the event is related to an Account
            if (e.whatId !=null && e.whatId.getSObjectType().getDescribe().getName().equalsIgnoreCase('account') ) 
            {
            	accounts.add( new account (id = e.whatId, Last_Event_Subject__c = e.Subject, Last_Event_Type__c = e.Type) );
            }    
        }

		update accounts;
        
    }
}