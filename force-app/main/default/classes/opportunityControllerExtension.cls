public with sharing class opportunityControllerExtension {

    public opportunityControllerExtension(ApexPages.StandardController controller) {
    }
    
        // Map<Id,String> OppOEMsMap = new Map<Id,String>();

    List<Opportunity_Group__c> oppgroup;
    public List<Opportunity_Group__c> getoppgroup() {
        if(oppgroup == null) {
                //strOEMs='';
                
           
          oppgroup = [SELECT id, Name, Opportunity__c,Quantity__c,Equipment_Pricing_Total__c, Tractor_Type__c,Trailer__c,
                (SELECT id, Name, Model__c, Type__c,Total_Price__c, Equipment_Type__c, EqPricing_Subtype__c,
                Manufacturer__c, OEM__c, OEM__r.Name,Year__c,Unit_Price__c,TermYears__c,MPY__c,Select_This_SubGroup_For_Opportunity__c,
                        LRF__c,ExchangeIT_Month__c,Extension_Discount__c, Axle__c, OEM_Model__c, OEM_Manufacturer__c
                // doesn't work ,(SELECT id, Name, Extended_Warranty__c FROM OEM_Pricing_Requests__r)
                    FROM Opportunity_SubGroups__r ORDER BY Select_This_SubGroup_For_Opportunity__c DESC)
                FROM Opportunity_Group__c 
                WHERE Opportunity__c =:ApexPages.CurrentPage().getparameters().get('id')];
            }
            return oppgroup;
        }
        
    
}