public class TriggerHandler {
	
	public static final Evt[] AFTER_ALL_EVENTS = new Evt[] {Evt.afterinsert, Evt.afterupdate, Evt.afterdelete, Evt.afterundelete};
	public static final Evt[] BEFORE_INSERT_UPDATE = new Evt[] {Evt.beforeinsert, Evt.beforeupdate};
		public static final Evt[] AFTER_INSERT_UPDATE = new Evt[] {Evt.afterinsert, Evt.afterupdate};
	
	public enum Evt {
		afterdelete, afterinsert, afterundelete,
		afterupdate, beforedelete, beforeinsert, beforeupdate
	}

	public interface HandlerInterface {
		void handle();
	}
	
	private static Set<String> controlTest = new Set<String>();
	
	public static void enable(Type handlerClass) {
		controlTest.add(handlerClass.getName());
	}

	Map<String, List<HandlerInterface>> eventHandlerMapping = new Map<String, List<HandlerInterface>>();
	
	public TriggerHandler bind(Evt[] events, HandlerInterface[] ehs) {
		for (HandlerInterface eh : ehs)
			bind(events, eh);
		return this;
	}

	public TriggerHandler bind(Evt[] events, HandlerInterface eh) {
		for (Evt event : events)
			bind(event, eh);
		return this;
	}

	public TriggerHandler bind(Evt event, HandlerInterface eh) {
		List<HandlerInterface> handlers = eventHandlerMapping.get(event.name());
		if (handlers == null) {
			handlers = new List<HandlerInterface>();
			eventHandlerMapping.put(event.name(), handlers);
		}
		handlers.add(eh);
		return this;
	}

	public void manage() {
		Evt ev = null;
		if(Trigger.isInsert && Trigger.isBefore)
			ev = Evt.beforeinsert;
		else if(Trigger.isInsert && Trigger.isAfter)
			ev = Evt.afterinsert;
		else if(Trigger.isUpdate && Trigger.isBefore)
			ev = Evt.beforeupdate;
		else if(Trigger.isUpdate && Trigger.isAfter)
			ev = Evt.afterupdate;
		else if(Trigger.isDelete && Trigger.isBefore)
			ev = Evt.beforedelete;
		else if(Trigger.isDelete && Trigger.isAfter)
			ev = Evt.afterdelete;
		else if(Trigger.isundelete)
			ev = Evt.afterundelete;
        
		List<HandlerInterface> handlers = eventHandlerMapping.get(ev.name());
		if (handlers != null && ! handlers.isEmpty()) {
			for (HandlerInterface h : handlers) {
				String name = String.valueOf(h).split(':')[0];
				if (!Test.isRunningTest() || controlTest.contains(name))
					h.handle();
			}
		}
	}
}