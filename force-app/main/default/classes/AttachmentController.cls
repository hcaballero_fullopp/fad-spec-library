public with sharing class AttachmentController {
   
    public Attachment__c attachment {get; set;}
    public transient Blob fileBody {get; set;}
    public transient String fileName {get; set;}
    
    public AttachmentController(ApexPages.StandardController controller) {
        if (!Test.isRunningTest())
        controller.addFields(new String[] {'Name', 'Account__c', 'Opportunity__c', 'Attachment_Type__c', 'Last_Attachment_Id__c', 'Spec__c', 'Presentation__c', 'LPAF__c'});
        this.attachment = (Attachment__c) controller.getRecord();
        String type = System.currentPageReference().getParameters().get('type');
        String specId = System.currentPageReference().getParameters().get('specId');
        String presentationId = System.currentPageReference().getParameters().get('presentationId');
        String lpafId = System.currentPageReference().getParameters().get('lpafId');
        String opportunityId = System.currentPageReference().getParameters().get('opportunityId');
        String accountId = System.currentPageReference().getParameters().get('accountId');
        if (type != null)
        this.attachment.Attachment_Type__c = type;
        if (lpafId != null) {
            Lease_Proposal_Acceptance_Form__c[] lpafs = [SELECT Id, Opportunity__c, Opportunity__r.AccountId FROM Lease_Proposal_Acceptance_Form__c WHERE Id = :lpafId];
            System.assertNotEquals(0, lpafs.size(), 'Wrong lpaf Id');
            attachment.LPAF__c = lpafId;
            attachment.Opportunity__c = lpafs[0].Opportunity__c;
            attachment.Account__c = lpafs[0].Opportunity__r.AccountId;
        }
        if (presentationId != null) {
            Presentation__c[] presentations = [SELECT Id, Opportunity__c, Opportunity__r.AccountId FROM Presentation__c WHERE Id = :presentationId];
            System.assertNotEquals(0, presentations.size(), 'Wrong presentation');
            attachment.Presentation__c = presentations[0].Id;
            attachment.Opportunity__c = presentations[0].Opportunity__c;
            attachment.Account__c = presentations[0].Opportunity__r.AccountId;
        }
        if (specId != null) {
            Opportunity_SubGroup__c[] specs = [SELECT Id, Opportunity_Group__r.Opportunity__c, Opportunity_Group__r.Opportunity__r.AccountId FROM Opportunity_SubGroup__c WHERE Id = :specId];
            System.assertNotEquals(0, specs.size(), 'The specId is wrong');
            if (specs.size() > 0) {
                attachment.Spec__c = specId;
                attachment.Opportunity__c = specs[0].Opportunity_Group__r.Opportunity__c;
                attachment.Account__c = specs[0].Opportunity_Group__r.Opportunity__r.AccountId;
            }
        }
        if (opportunityId != null) {
            Opportunity[] opportunities = [SELECT AccountId FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
            if (opportunities.size() > 0) {
                accountId = opportunities[0].AccountId;
            this.attachment.Opportunity__c = opportunityId;
            }
        }
        if (accountId != null && accountId != '')
        this.attachment.Account__c = accountId;
            }
    
    public System.Pagereference save() {
        Attachment[] files = null;
        Attachment file = null;
        if (attachment.Last_Attachment_Id__c != null)
            files = [SELECT Id, ParentId, Body, Name FROM Attachment WHERE Id = :attachment.Last_Attachment_Id__c ORDER BY LastModifiedDate LIMIT 1];
        if (files != null && files.size() > 0)
            file = files[0];
        else
            file = new Attachment();
        file.Body = fileBody;
        file.Name = fileName;

        if (file.Body == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a file to upload.'));
            return null;
        }
        if (attachment.Presentation__c != null) {
            Presentation__c[] presentations = [SELECT Id, Related_to_Account__c, Related_to_Account__r.Name, RecordTypeId, Opportunity__r.Account.Name, Opportunity__r.Name, Opportunity__c, Opportunity__r.AccountId FROM Presentation__c WHERE Id = :attachment.Presentation__c];
            Id customAnalyticsId = Schema.SObjectType.Presentation__c.getRecordTypeInfosByName().get('Custom Analytics').getRecordTypeId();
            if (attachment.Opportunity__c == null && presentations[0].Opportunity__c != null)
            attachment.Opportunity__c = presentations[0].Opportunity__c;
            else if (attachment.Opportunity__c == null && presentations[0].Opportunity__c != null && presentations[0].RecordTypeId == customAnalyticsId) {
                attachment.Opportunity__c.addError('The opportunity is required for Custom Analytics Presentations.');
                return null;
            } else if (attachment.Opportunity__c != presentations[0].Opportunity__c) {
                attachment.Opportunity__c.addError('The Opportunity doesn\'t match with presentation Opportunity. Correct opportunity is ' + presentations[0].Opportunity__r.Name);
                return null;
            }
            if (presentations[0].Opportunity__c == null && attachment.Opportunity__c != null)
            presentations[0].Opportunity__r = [SELECT Id, AccountId FROM Opportunity WHERE Id = :attachment.Opportunity__c];
            
            
            if (attachment.Account__c == null && presentations[0].Opportunity__r.AccountId != null)
            attachment.Account__c = presentations[0].Opportunity__r.AccountId;
            else if (attachment.Account__c != presentations[0].Related_to_Account__c) {
                attachment.Account__c.addError('The Account doesn\'t match with Presentation Account. Correct Account is ' + presentations[0].Related_to_Account__r.Name);
                return null;
            }
                    }
    if (attachment.LPAF__c != null) {
        Lease_Proposal_Acceptance_Form__c[] lpafs = [SELECT Id, Opportunity__r.Name, Opportunity__r.Account.Name, Opportunity__c, Opportunity__r.AccountId FROM Lease_Proposal_Acceptance_Form__c WHERE Id = :attachment.LPAF__c];
            if (attachment.Opportunity__c == null)
            attachment.Opportunity__c = lpafs[0].Opportunity__c;
            else if (attachment.Opportunity__c != lpafs[0].Opportunity__c) {
                attachment.Opportunity__c.addError('The Opportunity doesn\'t match with LPAF Opportunity. Correct opportunity is ' + lpafs[0].Opportunity__r.Name);
                return null;
            }
            
            if (attachment.Account__c == null)
            attachment.Account__c = lpafs[0].Opportunity__r.AccountId;
            else if (attachment.Account__c != lpafs[0].Opportunity__r.AccountId) {
                attachment.Account__c.addError('The Account doesn\'t match with LPAF Account. Correct Account is ' + lpafs[0].Opportunity__r.Account.Name);
                return null;
            }
            }
        if (attachment.Spec__c != null) {
            Opportunity_SubGroup__c spec = [SELECT Id, Opportunity_Group__r.Opportunity__c, Opportunity_Group__r.Opportunity__r.Name, Opportunity_Group__r.Opportunity__r.Account.Name, Opportunity_Group__r.Opportunity__r.AccountId FROM Opportunity_SubGroup__c WHERE Id = :attachment.Spec__c];
            if (attachment.Opportunity__c == null)
                attachment.Opportunity__c = spec.Opportunity_Group__r.Opportunity__c;
        else if (attachment.Opportunity__c != spec.Opportunity_Group__r.Opportunity__c) {
attachment.Opportunity__c.addError('The Opportunity doesn\'t match with Spec Opportunity. Correct opportunity is ' + spec.Opportunity_Group__r.Opportunity__r.Name);
            return null;
                    }
            
        if (attachment.Account__c == null)
                attachment.Account__c = spec.Opportunity_Group__r.Opportunity__r.AccountId;
        else if (attachment.Account__c != spec.Opportunity_Group__r.Opportunity__r.AccountId) {
            attachment.Account__c.addError('The Account doesn\'t match with Spec Account. Correct account is ' + spec.Opportunity_Group__r.Opportunity__r.Account.Name);
            return null;
        }
                    }
    if (attachment.Opportunity__c != null) {
        Opportunity opportunity = [SELECT Account.Name, AccountId FROM Opportunity WHERE Id = :attachment.Opportunity__c];
            if (attachment.Account__c == null)
            attachment.Account__c = opportunity.AccountId;
            else if (attachment.Account__c != opportunity.AccountId) {
                attachment.Account__c.addError('The account doesn\'t match with Opportunity Account. Correct account Name is ' + opportunity.Account.Name);
                return null;
            }
    }
        Savepoint sp = Database.setSavepoint();
        try {
            upsert attachment;
            if (file.parentId == null)
                file.ParentId = attachment.Id;
            upsert file;
            attachment = [SELECT Id FROM Attachment__c WHERE Id = :attachment.Id FOR UPDATE][0];
            attachment.Name = file.Name;
            attachment.Last_Attachment_Id__c = file.Id;
            update attachment;
        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }
        String retURL = System.currentPageReference().getParameters().get('retURL');
        if (retURL != null)
        return new System.Pagereference(retURL); 
        return new System.Pagereference('/'+attachment.Id);
    }
}