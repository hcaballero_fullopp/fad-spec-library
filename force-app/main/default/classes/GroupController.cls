public with sharing class GroupController {
	
	public fO_Group__c[] getGroups() {
		String sql = 'SELECT Id, Name, Schedule__c.Name, Unit_Group__c FROM fO_Group__c';
		return (fO_Group__c[])Database.query(sql);
	} 
}