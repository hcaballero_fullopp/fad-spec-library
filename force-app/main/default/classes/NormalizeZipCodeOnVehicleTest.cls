/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NormalizeZipCodeOnVehicleTest {
	
	private static account account;
	private static fO_Vehicle__c vehicle;
	
	static {
		account = new Account(Name = 'Test Account');
		insert account;
		vehicle = new fO_Vehicle__c(Account__c = account.Id);
	}

    static testMethod void shortFormatTest() {
    	vehicle.ZIP_Code__c = '3506';
    	Test.startTest();
    	insert vehicle;
    	Test.stopTest();
    	vehicle = [SELECT ZIP_Code__c FROM fO_Vehicle__c WHERE Id = :vehicle.Id];
    	System.assertEquals('03506', vehicle.ZIP_Code__c);
    }
    
    static testMethod void longFormatTest() {
    	vehicle.ZIP_Code__c = '3506-2512';
    	Test.startTest();
    	insert vehicle;
    	Test.stopTest();
    	vehicle = [SELECT ZIP_Code__c FROM fO_Vehicle__c WHERE Id = :vehicle.Id];
    	System.assertEquals('03506-2512', vehicle.ZIP_Code__c);
    }
    
    static testMethod void wrongFormatTest() {
    	vehicle.ZIP_Code__c = '36-2512';
    	String message = null;
    	Test.startTest();
    	try {
    		insert vehicle;
    	} catch (Exception e) {
    		message = e.getMessage();
    	}
    	Test.stopTest();
    	System.assertNotEquals(null, message);
    }
}