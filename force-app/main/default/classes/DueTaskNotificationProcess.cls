global class DueTaskNotificationProcess implements Schedulable {
	
	public static void start() {
		DueTaskNotificationProcess process = new DueTaskNotificationProcess();
		process.execute(null);
	}
	

	global void execute(SchedulableContext SC) {
		List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
		AggregateResult[] results = [SELECT COUNT(Id) TaskCOunt, OwnerId FROM Task WHERE ActivityDate < :Date.today() AND Status <> 'Completed' GROUP BY OwnerId];
		EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = 'Due Task Notification Template'];
		for (AggregateResult result : results) {
			Id ownerId = (Id)result.get('OwnerId');
			
			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
			message.setTargetObjectId(ownerId);
			message.setTemplateId(template.Id);
			message.setSaveAsActivity(false);
						//message.setWhatId(task.Id);
			messages.add(message);
		}
	
		Messaging.reserveSingleEmailCapacity(messages.size());
		Messaging.SendEmailResult[] email_results = Messaging.sendEmail(messages);
		for (Messaging.SendEmailResult result : email_results) {
			if (!result.isSuccess()) {
				for (Messaging.SendEmailError error : result.getErrors()) {
					String errorMessage = error.getMessage();
					//ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
				}
			}
		}
	}
}