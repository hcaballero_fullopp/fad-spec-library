/***	Author		: Henry Caballero @ fullOpp
		Date		: 8/27/2018
		Description	: Handle "Equipment Pricing" business logic.		
***/
public class equipmentPricingHelper  
{
	
	public final static List<Schema.FieldSetMember> fields = SObjectType.Quote.FieldSets.Specs.getFields();
    public static id priceBookEntryIdInCreateQuote{get;set;}

	// create the Opportunity Specs when a new "quote" is created 
	public static void updateQuote( list<quote> quotes )
	{
		set<Id> setOfSpecs = new set<Id>();
		for (quote q : quotes)
		{
			// get the speck library, if the quote has not a selected spect library we try to get it from its equiment pricing			
			q.Spec_Library__c = q.Spec_Library__c !=null ? q.Spec_Library__c : (q.Equipment_Pricing_Request__c!=null && q.Equipment_Pricing_Request__r.Spec_Library__c!=null ? q.Equipment_Pricing_Request__r.Spec_Library__c : null );
		
			if (q.Spec_Library__c!=null && !setOfSpecs.contains(q.Spec_Library__c))
				setOfSpecs.add(q.Spec_Library__c);		
		}

		// there is not any spec library selected
		if (setOfSpecs.size()==0)
			return;
		
		// get the specs library details
		Map<Id, Opportunity_SubGroup__c> specLibraryMAP	=  getSpecs(setOfSpecs)  ;

		//update the quote with the spec library info
		for (quote q : quotes)
		{
			if ( ! specLibraryMAP.containsKey(q.Spec_Library__c)  )
				continue;
			
			q.Pricebook2Id		= '01s60000000AZIZAA4';

			Opportunity_SubGroup__c spec = 	specLibraryMAP.get(q.Spec_Library__c);
		
			for(Schema.FieldSetMember f : fields) 
			{
				q.put(	f.getFieldPath() , spec.get(f.getFieldPath()) );					
			}

		}



	}

	public static void createQuoteLines(List<Quote> quotes)		
	{		
		set<Id> setOfSpecs = new set<Id>();
		for (quote q : quotes)
		{			
			if (q.Spec_Library__c!=null && !setOfSpecs.contains(q.Spec_Library__c))
				setOfSpecs.add(q.Spec_Library__c);					
		}

		// there is not any spec library selected
		if (setOfSpecs.size()==0)
			return;
		
		// get the specs library details
		Map<Id, Opportunity_SubGroup__c> specLibraryMAP	=  getSpecs(setOfSpecs)  ;


		createQuoteLines(quotes,specLibraryMAP );
	}


	// create the Opportunity Specs when the "Equipment Pricing" is created 
	public static void createQuote( list<Opportunity_Group__c> eps )
	{		
		//variables
		list<quote> quotes = new list<quote>();		
		set<Id> setOfSpecs = new set<Id>();

		for (Opportunity_Group__c ep : eps )
		{
			if (!setOfSpecs.contains(ep.Spec_Library__c))
				setOfSpecs.add(ep.Spec_Library__c);	
		}
		
		// there is not any spec library selected
		if (setOfSpecs.size()==0)
			return;

		// get the specs library details
		Map<Id, Opportunity_SubGroup__c> specLibraryMAP	=  getSpecs(setOfSpecs)  ;
		
		//create a new quote for each Equipment Pricing
		for (Opportunity_Group__c ep : eps )
		{
			if ( ! specLibraryMAP.containsKey(ep.Spec_Library__c)  )
				continue;

			quotes.add(createQuote(specLibraryMAP.get(ep.Spec_Library__c) , ep.Opportunity__c , ep.Spec_Library__c, ep.id));
		}		
		insert quotes;

		createQuoteLines(quotes,specLibraryMAP );
	
	}
	
	// create quote lines
	public static void createQuoteLines(list<quote> quotes,Map<Id, Opportunity_SubGroup__c> specLibraryMAP )
	{
		List<QuoteLineItem> quoteItems = new List<QuoteLineItem>();
        
		
        List<Pricebook2> pb = new List<Pricebook2>();
        pb = [select id, name from Pricebook2 where isStandard = true Limit 1];
        
		// get the price book entries
		Map<Id,PricebookEntry>	pricebook = getPriceBoolEntry( specLibraryMAP.values() , pb[0].Id );
        
		// create the quote lines based on spec attributes
		for (quote q : quotes )
		{
			for (Spec_Products__c specAttribute : specLibraryMAP.get(q.Spec_Library__c).Spec__r  )
			{			
				id PricebookEntryId = pricebook.containsKey(specAttribute.Product__c) ? pricebook.get(specAttribute.Product__c).id : null;
				decimal price		= pricebook.containsKey(specAttribute.Product__c) ? pricebook.get(specAttribute.Product__c).UnitPrice : 0; 
				quoteItems.add( new QuoteLineItem  ( UnitPrice = price, quantity = 1, PricebookEntryId = PricebookEntryId, Product2Id = specAttribute.Product__c, QuoteId =  q.id )	) ; 								

			}			
		}
		
		insert quoteItems;

	}
		
	// get the pricebook for the products in the specs
	private static Map<Id,PricebookEntry> getPriceBoolEntry	 (List<Opportunity_SubGroup__c> specs , id priceBookId )
	{
		Map<Id,PricebookEntry> productPriceBookMAP = new Map<Id,PricebookEntry> ();

		Set<id> setOfProducts = new Set<id >();
		for (Opportunity_SubGroup__c spec : specs)
		{            
			for (Spec_Products__c specAttribute : spec.Spec__r  )
			{
                
				if (!setOfProducts.contains(specAttribute.Product__c ) )
					setOfProducts.add(specAttribute.Product__c );				
			}
		}
        
        List<PricebookEntry> pbe1 = new List<PricebookEntry>([select id, Product2Id, UnitPrice from PricebookEntry where Pricebook2Id=:priceBookId and Product2Id in:setOfProducts]);
		
		if (setOfProducts.size() > 0 ) 
		{
			for (PricebookEntry pbe : [select id, Product2Id, UnitPrice from PricebookEntry where Pricebook2Id=:priceBookId and Product2Id in:setOfProducts])
			{
				productPriceBookMAP.put(pbe.Product2Id, pbe);
			}
		}
		
		return productPriceBookMAP;
	}

	public static quote createQuote(Opportunity_SubGroup__c spec, id OppID, id SpecLibraryId, id epID )
	{
		// crea a new quote mapping the spec.field to its corresponding quote.field
		//fixed fields
		quote Opportunity_Specs = new quote();
		Opportunity_Specs.OpportunityId		= OppID;	
		Opportunity_Specs.Spec_Library__c	= SpecLibraryId;
		Opportunity_Specs.Equipment_Pricing_Request__c = epID;
		Opportunity_Specs.Name				= spec.Name;
		Opportunity_Specs.Pricebook2Id		= '01s60000000AZIZAA4';

		//dynamic fields
		for(Schema.FieldSetMember f : fields) 
		{			
			Opportunity_Specs.put(	f.getFieldPath() , spec.get(f.getFieldPath()) );					
		}

		return Opportunity_Specs;
	}

	 public static  Map<Id, Opportunity_SubGroup__c> getSpecs(set<id> specs) 
	 {		
		Map<Id, Opportunity_SubGroup__c> specsMAP =  new Map<Id, Opportunity_SubGroup__c>();		
		String query = 'SELECT ';
        for(Schema.FieldSetMember f : fields) 
		{
            query += f.getFieldPath() + ', ';
        }

        query += 'Id, Name, (select Product__c  from Spec__r ) FROM Opportunity_SubGroup__c where id in : specs';
		List<Opportunity_SubGroup__c > oppSubGroup = Database.query(query);
      
        for (Opportunity_SubGroup__c spec :  oppSubGroup)       
		{
			specsMAP.put(spec.Id, spec);
		}	 	
	    return specsMAP;
    }
}