public with sharing class MassUpdateController {
	
	public String accountName {get; set;}
	public String faId {get; set;}
	public String selectedOpportunity {get; set;}
	public String debug {get; set;}
	
	public String scheduleName {get; set;}
	public String groupName {get; set;}
	public String unitName {get; set;}
	

	public Opportunity[] getOpportunities() {
		String sql = 'SELECT Id, Account.Name, FAID__c FROM Opportunity';
		String criteria = '';
		if (faId != null && faId != '')
			criteria = ' FAID__c LIKE \'%' + faId + '%\'';
		if (accountName != null && accountName != '')
			criteria += (criteria == '' ? '' : ' AND') + ' Account.Name LIKE \'%' + accountName + '%\'';
		if (criteria != '')
			sql += ' WHERE' + criteria;
		sql += ' LIMIT 1000';
		return (Opportunity[])Database.query(sql);
	}
	
	public fO_Group__c[] getGroups() {
		String sql = 'SELECT Id, Name, Schedule__r.Name, Unit_Group__c FROM fO_Group__c';
		return (fO_Group__c[])Database.query(sql);
	}
	
	public fO_Vehicle__c[] getUnits() {
		String sql = 'SELECT Id, Status__c, Vendor_ID__c, VIN__c, Type__c, Model__c, Make__c, Unit_Number__c, Asset_ID__c FROM fO_Vehicle__c LIMIT 20';
		return (fO_Vehicle__c[])Database.query(sql);
	}
	
	public System.Pagereference selectUnits() {
		String opportunityId = System.currentPageReference().getParameters().get('opportunityId');
		this.debug = 'Hola: ' + opportunityId;
		return null;
	}
	
	public Integer getRandomUnits() {
		return Integer.valueOf(Math.random()*10000);
	}
}