@isTest
private class LastInteractionTest {   
	public static testMethod void testLastInteractionSchedule() {
		RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' And Name='Qualified Prospect' LIMIT 1];
		
		Account acc = new Account(
			Name = 'Test - Account',
			RecordType=rt
		);
        
        insert acc;
		
		DateTime myDateTime=System.now();
        
		Onsite_Call__c oncall = new Onsite_Call__c(
			Account__c=acc.Id,
			Start_Date__c=myDateTime,
			End_Date__c=myDateTime.addHours(5)
		);
		insert oncall;
		
        
        Test.StartTest();
       	//Schedule Job
        LastInteractionSchedule lidtsch = new LastInteractionSchedule();
		String sch = '0 40 23 * * ?'; 
		system.schedule('Testing LastInteractionSchedule', sch, lidtsch);
        Test.stopTest();
    }
}