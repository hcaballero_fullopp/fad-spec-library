public with sharing class OBCTemplateSelectorController{
	private ApexPages.StandardController stdCtrl {get; set;}   
	public Map <String,Id> em {get; set;}
    private List<Contact> conts {get; set;} 
	private String setWhatId;
 	private  List<Folder> lf;
	public EmailMessage emailMsg {get; private set;}
    
    public Boolean render{get;set;}
    public Boolean show {get;set;}
    public String fileName {get {
            if (fileName==null) {
                fileName = '';
            }
            return fileName;
        }
        set;
	}
    public String fileId {get {
            if (fileId==null) {
                fileId = '';
            }
            return fileId;
        }
        set;
	}
    //public String addlRecipients {get; set;}
    
    private String addresses;
    private String ccAddresses;
    private String[] toAddresses;
    
    private static final String SUPPORT_EMAIL_ADDRESS = 'obcSupport@obcSupport.com';

    private EmailTemplate emailTemplate; 
    
    public Attachment attachment {
        get {
            if (attachment==null) {
                System.debug('==========> creating new empty Attachment.');
                attachment = new Attachment();
            }
            return attachment;
        }
        set;
    }   
	public OBCTemplateSelectorController(ApexPages.StandardController std) {
        render=true;
		stdCtrl=std;
        
		emailTemplate = new EmailTemplate();
    
        OBC_Process_Settings__c config = OBC_Process_Settings__c.getValues('Config');        
        
        String folder = config.OBC_Folder__c;
        if (folder==null) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please ensure that you have defined your email templates folder, contact your administrator'));
        
            
		List<Folder> lf = [SELECT Id FROM Folder where DeveloperName=:folder limit 1];
        System.debug('lf'+lf+'');
		Map <Id,Folder> m = new Map<Id, Folder>(lf);
        
        List<EmailTemplate> emailTemplates = [ SELECT Id,Name from EmailTemplate where FolderId in: m.keySet()];
        em = new Map <String,Id>();
        
        if (emailTemplates.size()==0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please ensure that you have defined your email templates, contact your administrator'));
            render=false;
            return;
        }
        for(EmailTemplate emt:emailTemplates){
           em.put(emt.Name, emt.Id);
        } 
        
 		conts = new List<Contact>();
        
        String typeList=ApexPages.currentPage().getParameters().get('type');
        System.debug('typeList='+typeList);
        
        if (typeList=='FleetList'){
          setupContactsFleetList(true); 
        }else if (typeList=='FleetListCont'){
          setupContactsFleetList(false);
        }else if (typeList=='OBCRequest'){
          setupContactsOBCRequest(true);  
        }else if (typeList=='OBCRequestCont'){
          setupContactsOBCRequest(false);  
        }else if (typeList=='DataIntRequest'){
            If(config.Manual_Data_Authorized__c !=null){
                String u =config.Manual_Data_Authorized__c;
                List<String> usernamelist = u.split(',');                
                List<User> users = [SELECT Id FROM User where username in :usernamelist And Id=:UserInfo.getUserId()];
                if(users.size()>0){
                	setupContactsDataIntRequest();                     
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This button is only authorized for IT. Please contact IT@FleetAdvantage.com for assistance'));
                }
            }

        }else{
            return;
        }
        
	
        System.debug('Contactos='+conts.size());        
        if (conts.size()>0){
            if (conts[0].Email != null){
        		addresses=conts[0].Email;
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Contact without Email'));
                render=false;
            	return;
            }
        }else{            
            render=false;
            return;
        }
        for (Integer i = 1; i < conts.size(); i++){
            if (conts[i].Email != null){
                addresses +=';' + conts[i].Email;
            }
        }
        System.debug('addresses='+addresses+' ccAddresses='+ccAddresses);
        System.debug('toAddresses='+toAddresses);
    	toAddresses = addresses.split(';',0);        
        System.debug('toAddresses='+toAddresses);
        
        //Create EmailMessage 
        emailMsg = new EmailMessage();
	}

	private void setupContactsOBCRequest(Boolean isAccount) {
        Account acct = new Account();
		if (isAccount){  
            setWhatId=stdCtrl.getId();
			acct = [SELECT id, Name, OBC_Type_dropdown__c, Owner.email from Account where Id=:stdCtrl.getId()];        
			conts=[SELECT Id, Name, Email, Phone,Account.Id from Contact where AccountId=:stdCtrl.getId() And OBC_Credentials_Contact__c=true And Account.Refuses_to_Give_Credentials__c=false];    
            if(conts.size()==0){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Client has elected not to provide OBC Credentials.  Please contact IT to manually send data extraction instructions.'));
                render=false;
                return;
        	}
        }else{	
			conts=[SELECT Id, Name, Email, Phone, Account.Id from Contact where Id=:stdCtrl.getId() And OBC_Credentials_Contact__c=true And Account.Refuses_to_Give_Credentials__c=false];            		
            if(conts.size()==0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Client has elected not to provide OBC Credentials.  Please contact IT to manually send data extraction instructions.'));
                render=false;
                return;
        	}
            acct = [SELECT Id, Name, OBC_Type_dropdown__c, Owner.email from Account where Id=:conts[0].Account.Id]; 
			if(acct.Id==null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Contact is not related to an Account.  Please contact IT to manually send data extraction instructions.'));
                render=false;
                return;
        	}            
			setWhatId=conts[0].Account.Id;			
        }
		if (acct.Owner.email!= null){
			ccAddresses=''+acct.Owner.email ;
		}
		
		
		//Set the email template
        if(acct.OBC_Type_dropdown__c=='Fleetmatics' || acct.OBC_Type_dropdown__c==null || acct.OBC_Type_dropdown__c=='Forward Thinking System' ||acct.OBC_Type_dropdown__c=='Vnomics' || acct.OBC_Type_dropdown__c=='Other/TBD'){
            if (em.get('Send OBC Request Generic/Default')!=null){
                emailTemplate.Id=em.get('Send OBC Request Generic/Default');
            }else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Send Fleet List Request Generic/Default Exist'));
				render=false;
				return;
			}
        }else{
            System.debug(String.valueOf(acct.OBC_Type_dropdown__c));
            System.debug('Send OBC Request '+String.valueOf(acct.OBC_Type_dropdown__c));
            if (em.get('Send OBC Request '+String.valueOf(acct.OBC_Type_dropdown__c))!=null){
                emailTemplate.Id=em.get('Send OBC Request '+String.valueOf(acct.OBC_Type_dropdown__c));
                
            }else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Send Fleet List Request '+String.valueOf(acct.OBC_Type_dropdown__c)+' Exist'));
				render=false;
				return;
			}
        }
 
	}
    
    private void setupContactsFleetList(Boolean isAccount) {
        Account acct=new Account();
		if (isAccount){
			conts=[SELECT Id, Name, Email, Phone, Account.Id from Contact where AccountId=:stdCtrl.getId() And Fleet_List_Contact__c=true];    
        }else{		
			conts=[SELECT Id, Name, Email, Phone, Account.Id from Contact where Id=:stdCtrl.getId() And Fleet_List_Contact__c=true];            
		}
	
        if(conts.size()==0){        
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Contacts related to this account have at least one Contact with Fleet List Contact Check'));
            return;
        }
		acct  = [SELECT id, Name, Owner.email, OBC_Type_dropdown__c from Account where Id=:conts[0].Account.Id]; 
        
		/*if(acct.Id==null){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Check if the Account Exist'));
            render=false;
            return;
        }*/
        
		setWhatId=acct.Id;
        
		if (acct.Owner.email!= null){
			ccAddresses=''+acct.Owner.email ;
		}
       	
        /*if (em.get('Fleet List Request')!=null){
            emailTemplate.Id=em.get('Fleet List Request');
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Fleet List Request'));
            render=false;
            return;
		}*/
        //Set the email template
        if(acct.OBC_Type_dropdown__c=='Fleetmatics' || acct.OBC_Type_dropdown__c==null || acct.OBC_Type_dropdown__c=='Forward Thinking System' ||acct.OBC_Type_dropdown__c=='Vnomics' || acct.OBC_Type_dropdown__c=='Other/TBD'){
            if (em.get('Send Fleet List Request Generic/Default')!=null){
                emailTemplate.Id=em.get('Send Fleet List Request Generic/Default');
            }else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Send Fleet List Request Generic/Default Exist'));
				render=false;
				return;
			}
        }else{
            System.debug(String.valueOf(acct.OBC_Type_dropdown__c));
            System.debug('Send Fleet List Request '+String.valueOf(acct.OBC_Type_dropdown__c));
            if (em.get('Send Fleet List Request '+String.valueOf(acct.OBC_Type_dropdown__c))!=null){
                emailTemplate.Id=em.get('Send Fleet List Request '+String.valueOf(acct.OBC_Type_dropdown__c));
                
            }else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Send Fleet List Request '+String.valueOf(acct.OBC_Type_dropdown__c)+' Exist'));
				render=false;
				return;
			}
        }
        
	}
    
    private void setupContactsDataIntRequest() { 
		Account acct=new Account();
		conts = [SELECT id, Name, Email, Phone, Account.Id from Contact where AccountId=:stdCtrl.getId() And Account.Authorized_Performance_Data_Request__c=true];    
		        
		if(conts.size()==0){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'In order to send manual data extraction instruction, please check IT Authorized to Send Manual Data Instructions'));
        	render=false;
			return;
        }
		acct  = [SELECT id, Name, OBC_Type_dropdown__c, Owner.email from Account where Id=:conts[0].Account.Id]; 
              
		/*if(acct.Id==null){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Check if the Account Exist'));
            render=false;
            return;
        }*/
        
		setWhatId=acct.Id;
        
		if (acct.Owner.email!= null){
			ccAddresses=''+acct.Owner.email ;
		}
        
		//Set the email template
        if(acct.OBC_Type_dropdown__c=='Fleetmatics' || acct.OBC_Type_dropdown__c==null || acct.OBC_Type_dropdown__c=='Forward Thinking System' ||acct.OBC_Type_dropdown__c=='Vnomics' || acct.OBC_Type_dropdown__c=='Other/TBD'){
            if (em.get('Manual Data Extraction Instructions Generic/Default')!=null){
                emailTemplate.Id=em.get('Manual Data Extraction Instructions Generic/Default');
            }else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Send Fleet List Request Generic/Default Exist'));
				render=false;
				return;
			}
        }else{
            System.debug(String.valueOf(acct.OBC_Type_dropdown__c));
            System.debug('Manual Data Extraction Instructions '+String.valueOf(acct.OBC_Type_dropdown__c));
            if (em.get('Manual Data Extraction Instructions '+String.valueOf(acct.OBC_Type_dropdown__c))!=null){
                emailTemplate.Id=em.get('Manual Data Extraction Instructions '+String.valueOf(acct.OBC_Type_dropdown__c));
                
            }else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Review the Email Template Send Fleet List Request '+String.valueOf(acct.OBC_Type_dropdown__c)+' Exist'));
				render=false;
				return;
			}
        }
              
	}
    
    // send email message per the attributes specified by user.
    public PageReference send() {
        try {
            // now create our SingleEmailMessage to send out.
            Messaging.SingleEmailMessage singleEmailMsg = new Messaging.SingleEmailMessage();

            // concatenate all To Addresses
            if (emailMsg.ToAddress != null && emailMsg.ToAddress != '') {
              	singleEmailMsg.setToAddresses(emailMsg.ToAddress.split(';',0));
            }
            
            // concatenate all Bcc Addresses
            if (emailMsg.BccAddress != null && emailMsg.BccAddress != '') {
                singleEmailMsg.setBccAddresses(emailMsg.BccAddress.split(';'));
            }

            // concatenate all CC Addresses
            if (emailMsg.CcAddress != null && emailMsg.CcAddress != '') {
                singleEmailMsg.setCcAddresses(emailMsg.CcAddress.split(';'));
            }
  
            singleEmailMsg.setSubject(emailMsg.Subject);
            singleEmailMsg.setHtmlBody(emailMsg.TextBody);
			//setPlainTextBody(emailMsg.TextBody);

            // now add additional recipients
            /*
			String[] addlToAddresses = null;
            if (addlRecipients != null && addlRecipients != '') {
                addlToAddresses = addlRecipients.split(';');
            }
            // now lets add any additional recipients to our list of recipients.
            List<String> lstToAddresses = null;
            if (addlToAddresses != null) {
                // now append these to our main recipient.
                lstToAddresses = new List<String>(addlToAddresses);
            } else {
                lstToAddresses = new List<String>();
            }*/
            
            //lstToAddresses.add(emailMsg.ToAddress);
            //singleEmailMsg.setToAddresses(lstToAddresses); 

            // now we need to reset the ToAddress for our EmailMessage.
            //emailMsg.ToAddress += (addlRecipients != null ? ';' + addlRecipients : '');

            //AttachFile from Email Template            
			List<Attachment> attachmentsList = [SELECT Id,Name, Body, ContentType FROM Attachment WHERE ParentId = :emailTemplate.Id];
            List<Messaging.EmailFileAttachment> email_attachments = new List<Messaging.EmailFileAttachment>();
            for(Attachment att : attachmentsList){
                Messaging.EmailFileAttachment email_att = new Messaging.EmailFileAttachment();
                email_att.setBody(att.Body);
                email_att.setContentType(att.ContentType);
                email_att.setFileName(att.Name);
                email_att.setinline(false);
                email_attachments.add(email_att);
            }
     
            // now attach file to email if there is one. Have to check the Body as Attachment
            // itself will never be null as it is always created first time it is accessed.
            if (attachment.Body != null) {
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                emailAttachment.setBody(attachment.Body);
                emailAttachment.setFileName(attachment.Name);
                email_attachments.add(emailAttachment);                
            }
            if (email_attachments.size()>0){
                singleEmailMsg.setFileAttachments(email_attachments);
            }
            List<Messaging.SendEmailResult> results =  Messaging.sendEmail(
                new List<Messaging.SingleEmailMessage> {singleEmailMsg});

            // now parse  our results
            if (results[0].success) {
                PageReference pgRef = new PageReference('/' + stdCtrl.getId());
                pgRef.setRedirect(true);
                return pgRef;
            } else {
                // on failure, display error message on existing page so return null to return there.
                String errorMsg = 'Error sending Email Message. Details = ' + results.get(0).getErrors()[0].getMessage();
                System.debug('==========> ' + errorMsg);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
                return null;
            }
        }
        catch (Exception e) {
            // on failure, display error message on existing page so return null to return there.
            String errorMsg = 'Exception thrown trying to send Email Message. Details = ' + e;
            System.debug('==========> ' + errorMsg);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
            return null;
        }
    }
    
    // Cancel creation of emailMessage. 
    public PageReference cancel() {
        PageReference pgRef = new PageReference('/' + stdCtrl.getId());
        pgRef.setRedirect(true);
        return pgRef;
    }
	
    public PageReference populateTemplate() {
        if (!render) return null;
        // construct dummy email to have Salesforce merge BrandTemplate (HTML letterhead) with our email
        Messaging.SingleEmailMessage dummyEmailMsg = new Messaging.SingleEmailMessage();
        dummyEmailMsg.setTemplateId(emailTemplate.Id);
        
        // This ensures that sending this email is not saved as an activity for the targetObjectId. 
        dummyEmailMsg.setSaveAsActivity(false);

        // send dummy email to populate HTML letterhead in our EmailMessage object's html body.
        dummyEmailMsg.setToAddresses(toAddresses);
        dummyEmailMsg.setReplyTo(SUPPORT_EMAIL_ADDRESS); 
        
        // now send email and then roll it back but invocation of sendEmail() 
        // means merge of letterhead & body is done

        // TargetObject is User. This tells the emailMsg to use the email message
        // associated with our dummy User. This is necessary so we can populate our
        // email message body & subject with merge fields from template
        Savepoint sp = Database.setSavepoint();

        Account dummyAcct = new Account(Name='dummy account');
        insert dummyAcct;
        
        Contact dummyContact        = new Contact(AccountId=dummyAcct.Id);
        dummyContact.FirstName      = 'First';
        dummyContact.LastName       = 'Last';
        dummyContact.Email          = 'generateContact@template.com';
        insert dummyContact;

        dummyEmailMsg.setTargetObjectId(dummyContact.Id);        
        dummyEmailMsg.setWhatId(setWhatId);
            
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {dummyEmailMsg});
        // now rollback our changes.
        Database.rollback(sp);

        // now populate our fields with values from SingleEmailMessage.
        emailMsg.BccAddress  = '';//UserInfo.getUserEmail();
        emailMsg.Subject     = dummyEmailMsg.getSubject();
        //emailMsg.HtmlBody    = dummyEmailMsg.getHtmlBody();//getPlainTextBody();
        emailMsg.ToAddress   = addresses;//dummyEmailMsg.getToAddresses();
        emailMsg.FromAddress = UserInfo.getUserEmail(); 
        emailMsg.CcAddress   = ccAddresses;
        emailMsg.ParentId    = stdCtrl.getId(); 
        String myHTML =    String.valueOf(dummyEmailMsg.getHtmlBody());

        System.debug(''+myHTML);
        String htmlPattern = '<(?i).*?>';
        String breakPattern = '(?i)<br\\s*/?>'; 
        String whitespacePattern = '\\s+';
        
        String workingText;
        String plainText;

        // replace all HTML break tags with the newLine character
        pattern lineBreaks = pattern.compile(breakPattern);
        matcher matchedLineBreaks = lineBreaks.matcher(myHTML);
        workingText = matchedLineBreaks.replaceAll('\n');
        
        System.debug(''+workingText);        
        emailMsg.TextBody  =  workingText;//String.valueOf(dummyEmailMsg.getPlainTextBody()); 
		
		show=true;
		fileId='';
		List<Attachment> attachmentsList = [SELECT Id,Name, Body, ContentType FROM Attachment WHERE ParentId = :emailTemplate.Id limit 1];
        if(attachmentsList.size()==0){
            show=false;
            return null;
        }
        fileId=attachmentsList[0].Id;
        fileName=attachmentsList[0].Name;
        return null;        
    }
}