public with sharing class accountControllerExtension {

    private final Account acct;
    private Set<Id> AllIds;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public accountControllerExtension(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        Id thisacct = this.acct.id;
        AllIds = new Set<Id>();
        AllIds.add(thisacct);
        Set<Id> ChildIds = new Set<Id>();
       
        System.debug('acct.id: ' + thisacct);
        // Get Child Account Ids
        for (Account a : [select Id, ParentId from Account where ParentId = :thisacct]) {
           ChildIds.add(a.Id);
           AllIds.add(a.Id);
        }
        System.debug('ChildIds: ' + childIds.size());
        
        // Get Grandchild Account Ids
        Set<Id> grandChildIds = new Set<Id>();
        for (Account a : [select Id, ParentId from Account where ParentId = :ChildIds]) {
           grandChildIds.add(a.Id);
           AllIds.add(a.Id);
        }
        // Get Great-Grandchild Account Ids
        for (Account a : [select Id, ParentId from Account where ParentId = :grandChildIds]) {
           AllIds.add(a.Id);
        }
        System.debug('All Accounts: ' + AllIds.size());
        
        // Get Ids of related Opportunities
        for (Opportunity o : [select Id from Opportunity where AccountId in :AllIds]) {
           AllIds.add(o.Id);
        }
        System.debug('All Accounts and Opportunities: ' + AllIds.size());
        
    }

    public List<Task> getRelatedTasks() {
        return [select Id, Subject, ActivityDate, Status, Owner.Name, What.Name, Account.name, Type, CallType from Task where AccountId in :AllIds Order By ActivityDate Desc];
         
    
    }
    public List<Event> getRelatedEvents() {
        return [select Id, Legacy_Calltype__c, Subject, Type, Event_Status__c, Owner.Name, What.Name, ActivityDate from Event where WhatId in :AllIds Order By ActivityDate Desc];
    }
}