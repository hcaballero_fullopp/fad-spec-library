@isTest
private class opportunityControllerExtension2_Test {
    @isTest
    public static void getoppgroup_Test (){
        test.startTest();
        
   		Opportunity_Group__c grop = TestDataFactory.createOpportunitySubGroup();		
		insert grop;
        Opportunity opp = [Select Id , Name from Opportunity];
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        opportunityControllerExtension2 oppCon = new opportunityControllerExtension2(sc);
        List<Opportunity_Group__c> opp_Group = oppCon.getoppgroup ();
        test.stopTest();
        List<Opportunity_Group__c> opp_Group1 = [Select Id, Opportunity__c from Opportunity_Group__c where Opportunity__c =:opp.Id];
        System.assert(opp_Group1.size() > 0);
    }
	
}