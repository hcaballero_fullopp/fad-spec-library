@IsTest
private class OBCTemplateSelectorTest {
    
    static testMethod void OBCTemplateSelectorControllerCancelTest() {
        Test.startTest();
		createConfigNoTemplate();
		
        Account acc = createAccount(true,'');
        Contact contact = createContactNoMail(acc.Id,true,false,false,'1');
        
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id',acc.Id);
        pageRef.getParameters().put('type','type'); //FleetList
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        controller.cancel();   
        controller.send(); 
        
        createConfig();        
        controller = new OBCTemplateSelectorController(obcController);
   	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorControllerTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'');
        Contact contact = createContact(acc.Id,true,true,false,'1');
        
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id',acc.Id);
        pageRef.getParameters().put('type','FleetList'); //FleetList
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        controller.cancel(); 
        controller.send();      
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorFleetListTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'');

        Contact contact = createContact(acc.Id, true,true,false,'1');
        Contact contact2 = createContact(acc.Id, true,true,false,'2');
        
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','FleetList');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        controller.populateTemplate();
		controller.send();
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorFleetListTestCont() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'');
        Contact contact = createContact(acc.Id,true,true,false,'1');
        
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','FleetListCont');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(contact);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
         
        controller.attachment=attach;
        String mfilename=controller.fileName;
        String mfileId=controller.fileId;
		//controller.addlRecipients='addlRecipients@test.com;addlRecipients2@test.com';
        controller.populateTemplate();
        controller.send();
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorNoFleetListTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'');

        Contact contact = createContact(acc.Id,true,false,false,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','FleetList');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestNoContactTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Cadec');

        Contact contact = createContact(acc.Id,true,false,false,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccCadecTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Cadec');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController); 
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccFleetmaticsTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Fleetmatics');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        controller.populateTemplate();
        controller.send();
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccOmnitracsTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Omnitracs');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccPeopleNetTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'PeopleNet');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccRandMcNallyTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'RandMcNally');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }

    static testMethod void OBCTemplateSelectorOBCRequestAccRoadnetTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Roadnet');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccTelogisTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Telogis');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestAccXRSTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'XRS');
         
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestContactCadecTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Cadec');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','OBCRequestCont');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(contact);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestDataIntRequestCadecTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccountNA(true,'Cadec');
       
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestCadecTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Cadec');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }

	static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestNATest() {
        Test.startTest();
		createConfigNotMatch();
		
        Account acc = createAccount(true,'Cadec');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }    
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestFleetmaticsTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Fleetmatics');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestOmnitracsTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Omnitracs');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }

	static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestPeopleNetTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'PeopleNet');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }    
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestRandMcNallyTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'RandMcNally');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
   
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestRoadnetTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'Roadnet');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestTelogisTest() {
        Test.startTest();
        createConfigOtherTemplate();
	
        Account acc = createAccount(true,'Telogis');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        createConfig();
        
        controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    static testMethod void OBCTemplateSelectorOBCRequestContactDataIntRequestXRSTest() {
        Test.startTest();
		createConfig();
		
        Account acc = createAccount(true,'XRS');
        
        Contact contact = createContact(acc.Id,true,false,true,'1');
        PageReference pageRef = Page.OBCTemplateSelector;
        
        pageRef.getParameters().put('Id', acc.Id);
        pageRef.getParameters().put('type','DataIntRequest');
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController obcController = new ApexPages.standardController(acc);
        OBCTemplateSelectorController controller = new OBCTemplateSelectorController(obcController);
        
 	    Test.stopTest();
    }
    
    private static Account createAccount(Boolean save,String ObcType){
        Account acc = new Account(name = 'acc');
        acc.OBC_Type_dropdown__c=ObcType;
        acc.Authorized_Performance_Data_Request__c=true;
        if(save) insert acc;
        return acc;
    }
	
    private static Account createAccountNA(Boolean save,String ObcType){
        Account acc = new Account(name = 'accTest');
        acc.OBC_Type_dropdown__c=ObcType;
        acc.Authorized_Performance_Data_Request__c=false;
        if(save) insert acc;
        return acc;
    }
    
    private static Contact createContact(Id account, boolean save,boolean fleetList,boolean obcCredential,String i){
        Contact c = new Contact();
        c.LastName = 'Name'+i;
        c.FirstName = 'Name'+i;
        c.Department = 'Name';
        c.Email = 'test'+i+'@test.com';
        c.AccountId = account;
        c.Fleet_List_Contact__c=fleetList;
        c.OBC_Credentials_Contact__c=obcCredential;
        if(save) insert c;
        return c;

    }
    
    private static Contact createContactNoMail(Id account, boolean save,boolean fleetList,boolean obcCredential,String i){
        Contact c = new Contact();
        c.LastName = 'Name'+i;
        c.FirstName = 'Name'+i;
        c.Department = 'Name';
        c.AccountId = account;
        c.Fleet_List_Contact__c=fleetList;
        c.OBC_Credentials_Contact__c=obcCredential;
        if(save) insert c;
        return c;

    }
    
    private static void createConfig(){
        OBC_Process_Settings__c sc = new OBC_Process_Settings__c();
        sc.Manual_Data_Authorized__c = UserInfo.getUserName();
        sc.OBC_Folder__c = 'OBC_Templates';
        sc.name = 'Config';
        insert sc;
    }
    
    private static void createConfigNotMatch(){
        OBC_Process_Settings__c sc = new OBC_Process_Settings__c();
        sc.Manual_Data_Authorized__c = 'username';
        sc.OBC_Folder__c = 'OBC_Templates';
        sc.name = 'Config';
        insert sc;
    }
    
    private static void createConfigNoTemplate(){
        OBC_Process_Settings__c sc = new OBC_Process_Settings__c();        
        sc.Manual_Data_Authorized__c = UserInfo.getUserName();
        sc.OBC_Folder__c = 'OBC Templates';
        sc.name = 'Config';
        insert sc;
    }
        
	private static void createConfigOtherTemplate(){
        OBC_Process_Settings__c sc = new OBC_Process_Settings__c();
        sc.Manual_Data_Authorized__c = UserInfo.getUserName();
        sc.OBC_Folder__c = 'Workflow_Emails';
        sc.name = 'Config';
        insert sc;
    }
}