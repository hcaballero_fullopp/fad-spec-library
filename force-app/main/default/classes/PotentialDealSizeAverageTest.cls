/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PotentialDealSizeAverageTest {

    static testMethod void myUnitTest() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account = new Account(Name = 'Test Opp', RecordTypeId = accountRecordTypeId);
        insert account;
        Opportunity opportunity = new Opportunity(Name = 'Test Opp', CloseDate = Date.today(), StageName = 'Draft', AccountId = account.Id);
        insert opportunity;
        Opportunity_Group__c request = new Opportunity_Group__c(Opportunity__c = opportunity.Id);
        insert request;
        Contact contact = new Contact(FirstName = 'John', LastName = 'Doe');
        insert contact;
        Lease_Proposal_Acceptance_Form__c lpaf = new Lease_Proposal_Acceptance_Form__c(Opportunity__c = opportunity.Id, Primary_Contact__c = contact.Id);
        insert lpaf;
        Opportunity_SubGroup__c spec1 = new Opportunity_SubGroup__c(Opportunity_Group__c = request.Id, Engine_Warranty__c = 100, LPAF_Request__c = lpaf.Id);
        Opportunity_SubGroup__c spec2 = new Opportunity_SubGroup__c(Opportunity_Group__c = request.Id, Engine_Warranty__c = 200, LPAF_Request__c = lpaf.Id);
        Test.startTest();
        insert spec1;
        insert spec2;
        Test.stopTest();
        spec1 = [SELECT Total_Unit_Price__c FROM Opportunity_SubGroup__c WHERE Id = :spec1.Id];
        System.assertEquals(100, spec1.Total_Unit_Price__c);
        request = [SELECT Potential_Deal_Size__c FROM Opportunity_Group__c WHERE Id = :request.Id];
        System.assertEquals(150, request.Potential_Deal_Size__c);
            }
}