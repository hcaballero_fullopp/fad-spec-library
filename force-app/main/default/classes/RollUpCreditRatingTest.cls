@isTest
private class RollUpCreditRatingTest {

	static testMethod void myUnitTest() {
		Account account = new Account(Name = 'John Doe');
		insert account;
		Credit_Worthiness_Tracking__c tracking1 = new Credit_Worthiness_Tracking__c(FA_Credit_Rating__c = '5', Account__c = account.Id, Completed_Date__c = Date.today());
		Credit_Worthiness_Tracking__c tracking2 = new Credit_Worthiness_Tracking__c(FA_Credit_Rating__c = '6', Account__c = account.Id, Completed_Date__c = Date.today().addDays(-1));
		Credit_Worthiness_Tracking__c tracking3 = new Credit_Worthiness_Tracking__c(FA_Credit_Rating__c = '7', Account__c = account.Id, Completed_Date__c = Date.today().addDays(-2));
		Test.startTest();
		insert tracking1;
		insert tracking2;
		insert tracking3;
		Test.stopTest();
		account = [SELECT FA_Credit_Rating__c FROM Account WHERE Id = :account.Id];
		System.assertEquals(5, account.FA_Credit_Rating__c, 'FA Credit Rating is not 5');
    }
}