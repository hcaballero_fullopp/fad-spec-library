trigger LPAFSelectedByClient on Lease_Proposal_Acceptance_Form__c (after delete, after insert, after undelete, 
after update) {
	Set<Id> lpafIds = new Set<Id>();
	if (Trigger.old != null)
		for (Lease_Proposal_Acceptance_Form__c lpaf : Trigger.old) {
			if (lpaf.Id != null && !lpafIds.contains(lpaf.Id))
				lpafIds.add(lpaf.Id);
		}

	if (Trigger.new != null)
		for (Lease_Proposal_Acceptance_Form__c lpaf : Trigger.new) {
			if (lpaf.Id != null && !lpafIds.contains(lpaf.Id))
				lpafIds.add(lpaf.Id);
		}

Opportunity_SubGroup__c[] specs = [SELECT Id, LPAF_Request__r.Client_Selected_this_LPAF__c FROM Opportunity_Subgroup__c WHERE LPAF_Request__c IN :lpafIds];
	for (Opportunity_SubGroup__c spec : specs) {
		spec.LPAF_Selected_by_Client__c =spec.LPAF_Request__r.Client_Selected_this_LPAF__c == true; // false if field is false or null
	}
	update specs;
}