trigger UpdateFleetZipOnVehicle on fO_Vehicle__c (before insert, before update) {
	
	Set<String> zips = new Set<String>();
	for (fO_Vehicle__c vehicle : Trigger.new) {
		if (!zips.contains(vehicle.Normalized_ZIP_Code__c))
		zips.add(vehicle.Normalized_ZIP_Code__c);
	}
	
	FleetZip__c[] fleetZips = [SELECT Id, Name FROM FleetZip__c WHERE Name IN :zips];
	Map<String, FleetZip__c> fleetZipMap = new Map<String, FleetZip__c>();
	for (FleetZip__c fleet : fleetZips)
		fleetZipMap.put(fleet.Name, fleet);
		
for (fO_Vehicle__c vehicle : Trigger.new) {
	FleetZip__c fleet = fleetZipMap.get(vehicle.Normalized_ZIP_Code__c);
	if (fleet != null)
		vehicle.FleetZip__c = fleet.Id;
}
}