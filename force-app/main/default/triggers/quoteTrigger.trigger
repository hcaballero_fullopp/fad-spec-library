/***	Author		: Henry Caballero @ fullOpp
		Date		: 8/29/2018
		Description	: 
***/
trigger quoteTrigger on Quote (before insert, after insert)  
{ 
	if (Trigger.isBefore && Trigger.isInsert)
	{
		equipmentPricingHelper.updateQuote(trigger.new);
	}

	if (Trigger.isAfter && Trigger.isInsert)
	{
		equipmentPricingHelper.createQuoteLines(trigger.new);
	}

}