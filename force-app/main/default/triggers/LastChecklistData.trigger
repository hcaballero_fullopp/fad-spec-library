trigger LastChecklistData on Data_Checklist__c (after delete, after insert, after undelete, 
after update) {
	Set<Id> opportunityIds = new Set<Id>();
	if (Trigger.new != null)
		for (Data_Checklist__c data : Trigger.new) {
			if (!opportunityIds.contains(data.Related_To_Opportunity__c))
				opportunityIds.add(data.Related_To_Opportunity__c);
		}
		
	if (Trigger.old != null)
		for (Data_Checklist__c data : Trigger.old) {
			if (!opportunityIds.contains(data.Related_To_Opportunity__c))
				opportunityIds.add(data.Related_To_Opportunity__c);
		}
		
	AggregateResult[] results = [SELECT MAX(Fleet_List_Received_Date__c) LastReceivedDate, MAX(Portal_Load_Date__c) LastLoadDate, Related_To_Opportunity__c FROM Data_Checklist__c WHERE Related_To_Opportunity__c = :opportunityIds AND Related_To_Opportunity__c <> NULL GROUP BY Related_To_Opportunity__c];
	List<Opportunity> opportunities = new List<Opportunity>();
	for (AggregateResult result : results) {
		Datetime lastReceivedDate = (Datetime)result.get('LastReceivedDate');
		Date lastLoadDate = (Date)result.get('LastLoadDate');
		Id OpportunityId = (Id)result.get('Related_To_Opportunity__c');
		Opportunity opportunity = new Opportunity(Id = opportunityId, Fleet_List_Rec_d__c = lastReceivedDate, OBC_Data_Load__c = lastLoadDate);
		opportunities.add(opportunity);
	}
	update opportunities;
}