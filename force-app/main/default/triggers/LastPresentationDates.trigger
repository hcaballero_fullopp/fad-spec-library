trigger LastPresentationDates on Presentation__c (after delete, after insert, after undelete, 
after update) {
		
	Set<Id> opportunityIds = new Set<Id>();
	if (Trigger.new != null)
		for (Presentation__c presentation : Trigger.new) {
			if (!opportunityIds.contains(presentation.Opportunity__c))
				opportunityIds.add(presentation.Opportunity__c);
		}
		
	if (Trigger.old != null)
		for (Presentation__c presentation : Trigger.old) {
			if (!opportunityIds.contains(presentation.Opportunity__c))
				opportunityIds.add(presentation.Opportunity__c);
		}
		
	AggregateResult[] results = [SELECT
			Opportunity__c,
			MAX(Intro_TCO_Presented__c) Intro_TCO_Presented,
			MAX(Fuel_Break_Even__c) Fuel_Break_Even,
			MAX(CCA_Barrel_Chart__c) CCA_Barrel_Chart,
			MAX(LMS_Date__c) LMS_Date,
			MAX(MPG_MPY__c) MPG_MPY,
			MAX(M_R__c) M_R
		FROM Presentation__c
		WHERE Opportunity__c IN :opportunityIds AND Opportunity__c <> NULL
		GROUP BY Opportunity__c];
		
	List<Opportunity> opportunities = new List<Opportunity>();
	for (AggregateResult result : results) {
		Id opportunityId = (Id)result.get('Opportunity__c');
		Date Intro_TCO_Presented = (Date)result.get('Intro_TCO_Presented');
		Date Fuel_Break_Even = (Date)result.get('Fuel_Break_Even');
		Date CCA_Barrel_Chart = (Date)result.get('CCA_Barrel_Chart');
		Date LMS_Date = (Date)result.get('LMS_Date');
		Date MPG_MPY = (Date)result.get('MPG_MPY');
		Date M_R = (Date)result.get('M_R');
		Opportunity opportunity = new Opportunity(Id = opportunityId);

		if (Intro_TCO_Presented !=null)
			opportunity.Intro_TCO_Presented__c = Intro_TCO_Presented ;

		if (Fuel_Break_Even !=null)	
		opportunity.Fuel_Break_Even_Date__c = Fuel_Break_Even; 

		if (CCA_Barrel_Chart !=null)	
			opportunity.CCA_Barrel_Chart__c = CCA_Barrel_Chart; 

		if (LMS_Date !=null)	
			opportunity.LMS_Date__c = LMS_Date; 

		if (MPG_MPY !=null)	
			opportunity.MPG_MPY__c = MPG_MPY; 

		if (M_R !=null)	
			opportunity.M_R__c = M_R;	


		opportunities.add(opportunity);
	}
	update opportunities;
	}