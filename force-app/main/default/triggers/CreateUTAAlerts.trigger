trigger CreateUTAAlerts on Opportunity_SubGroup__c (after insert) {
	
	Set<Id> productIds = new Set<Id>();
	Set<String> productFields = new Set<String> {'Engine__c', 'Axle_LO__c', 'OEM__c', 'Trailer__c', 'Transmission__c'};
	for (Opportunity_SubGroup__c spec : Trigger.new) {
		for (String productField : productFields) {
			if (spec.get(productField) != null)
				productIds.add((Id)spec.get(productField));
		}
	}
	
	UTA_Details__c[] details = [SELECT Approved__c, Id, Component__c FROM UTA_Details__c WHERE Component__c IN :productIds AND Status__c = 'Active'];
	Map<Id, UTA_Details__c> detailMap = new Map<Id, UTA_Details__c>();
	for (UTA_Details__c detail : details)
		if (!detailMap.containsKey(detail.Component__c))
			detailMap.put(detail.Component__c, detail);
	List<UTA_Worksheet__c> alerts = new List<UTA_Worksheet__c>();
	for (Opportunity_SubGroup__c spec : Trigger.new) {
for (String productField : productFields) {
			UTA_Details__c detail = detailMap.get((Id)spec.get(productField));
			if (detail != null) {
				Decimal amount = detail.Approved__c;
				UTA_Worksheet__c alert = new UTA_Worksheet__c(UTA_Detail__c = detail.Id, Spec_Formerly_SubGroup__c = spec.Id, Amount__c = amount);
				alerts.add(alert);
			}
		}
	}
	insert alerts;
}