trigger specTrigger on Opportunity_SubGroup__c (after insert, before update)  
{ 
	if (Trigger.isInsert && Trigger.isAfter)
	{
		specHelper.createSpecAttributes(trigger.new);
	}
    
    if (Trigger.isUpdate && Trigger.isBefore){
        specHelper.preventUpdateProduct(Trigger.newMap , Trigger.oldMap, Trigger.new);        
    }
}