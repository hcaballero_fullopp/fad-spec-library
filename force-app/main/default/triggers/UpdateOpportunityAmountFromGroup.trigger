trigger UpdateOpportunityAmountFromGroup on Opportunity_Group__c (after update) {
//
/***********************************************************************************
*  
* Trigger:      UpdateOpportunityAmountFromGroup
* Author:       Harvest Solutions
* Date:         March 2014
* Description:  Update Opportunity and Group Totals when a Group is changed
*
* Updates
*
* Date        Author         Description
*------------------------------------------------------------------------------------
*
*
************************************************************************************/



/* THIS IS NOT ACTIVE  - CK 6/1/2014 */



   Set<Id> OppIds = new Set<Id>();
   for (integer i=0; i<Trigger.size;i++) {
      if (Trigger.new[i].Quantity__c != null && Trigger.new[i].Quantity__c != Trigger.old[i].Quantity__c) 
         OppIds.add(Trigger.new[i].Opportunity__c);
   }
   if (OppIds.size() > 0) {
      OpportunityUPdate ou = new OpportunityUpdate();
      ou.UpdateOpps(OppIds);
   }
}