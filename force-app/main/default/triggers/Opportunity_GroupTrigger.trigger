/***	Author		: Henry Caballero @ fullOpp
		Date		: 8/27/2018
		Description	: Implements "Equipment Pricing" business logic.		
***/
trigger Opportunity_GroupTrigger on Opportunity_Group__c (after insert)  
{

	if (Trigger.isInsert && Trigger.isAfter)
	{
		equipmentPricingHelper.createQuote(Trigger.new);
	} 

}