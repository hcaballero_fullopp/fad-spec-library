trigger RollUpCreditRating on Credit_Worthiness_Tracking__c (after delete, after insert, after undelete, 
after update) {
	
	Map<Id, Account> accountMap = new Map<Id, Account>();
	if (Trigger.new != null) {
		for (Credit_Worthiness_Tracking__c tracking : Trigger.new) {
			if (!accountMap.containsKey(tracking.Account__c))
				accountMap.put(tracking.Account__c, new Account(Id = tracking.Account__c, Fa_Credit_Rating__c = null));
		}
	}
	if (Trigger.old != null) {
		for (Credit_Worthiness_Tracking__c tracking : Trigger.old) {
			if (!accountMap.containsKey(tracking.Account__c))
				accountMap.put(tracking.Account__c, new Account(Id = tracking.Account__c, Fa_Credit_Rating__c = null));
		}
	}
	
	AggregateResult[] results = [SELECT MAX(FA_Credit_Rating_Number__c) Rating, Account__c, Completed_Date__c FROM Credit_Worthiness_Tracking__c WHERE Account__c IN :accountMap.keySet() AND Completed_Date__c <> null GROUP BY Account__c, Completed_Date__c ORDER BY Completed_Date__c DESC];
	Set<Id> accountIds = new Set<Id>();
	for (AggregateResult result : results) {
		Id accountId = (Id)result.get('Account__c');
		Decimal rating = (Decimal)result.get('Rating');
		if (accountIds.contains(accountId))
		continue;
		Account account = accountMap.get(accountId);
		account.FA_Credit_Rating__c = rating;
		accountIds.add(accountId);
	}
	update accountMap.values();

}