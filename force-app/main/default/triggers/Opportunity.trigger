trigger Opportunity on Opportunity (before insert, before update) {

	new TriggerHandler().bind(TriggerHandler.BEFORE_INSERT_UPDATE, new TriggerHandler.HandlerInterface[] {
		new OpportunityValidationHandler()
	}).manage();
}