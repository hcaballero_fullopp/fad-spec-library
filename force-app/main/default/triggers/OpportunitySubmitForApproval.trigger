trigger OpportunitySubmitForApproval on Opportunity (after insert, after update) {
	
	for (Opportunity opportunity : Trigger.new) {
		Opportunity old = Trigger.isUpdate ? Trigger.oldMap.get(opportunity.Id) : null;
		if ((opportunity.IsClosed && (old == null || !old.IsClosed)) || (opportunity.Fleet_Study_Approval_Request__c && (old == null || !old.Fleet_Study_Approval_Request__c))) {
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(opportunity.Id);
            // submit the approval request for processing
            try {
            	Approval.ProcessResult result = Approval.process(req);
            } catch (Exception e) {
            	//opportunity.addError(e.getMessage());
            }
					}
	}
}