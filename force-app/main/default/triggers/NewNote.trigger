trigger NewNote on Note (before insert) {

     List<Onsite_Note_Email__c> emailToCreate = new List<Onsite_Note_Email__c>();
     String idAsString;
     for (Note n : trigger.new) {
          idAsString = n.ParentId;
          // check for Account type id
          if (idAsString.subString(0,3) == 'a0G') {
              // Change name for fun
              //n.Title += '-TRIGGER-WAS-HERE';
              //Insert Email Record
              emailToCreate.add(new Onsite_Note_Email__c(Onsite_Call__c=n.ParentId,
              	Subject__c = n.Title,  
              	Body__c = n.Body
				));
				//              	OSC_Subject__c=n.Parent.Subject_or_Keywords__c, 
				//              	OSC_Body__c=n.Parent.Notes__c 
              insert emailToCreate;
              delete emailToCreate;
          }
     }
}