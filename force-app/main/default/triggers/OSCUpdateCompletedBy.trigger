trigger OSCUpdateCompletedBy on Onsite_Call__c (Before insert) {
	for (Onsite_Call__c osc : trigger.new) {
		osc.Completed_by__c = System.Userinfo.getUserId();
	}
}