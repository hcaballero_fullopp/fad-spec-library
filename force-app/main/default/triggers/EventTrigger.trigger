trigger EventTrigger on Event (after insert) 
{
    if (trigger.isAfter && trigger.isInsert)
    	lastEventOnAccountHandler.updateLastEvent(trigger.new);
}