trigger CreateDeliverables on Opportunity (after insert) {
/*  WE USE THE PRESENTATION OBJECT and we do not autocreate presentation requests.

Set<Id> OppIds = new Set<Id>();
List<Deliverable__c> DeliverablesForInsert = new List<Deliverable__c>();

for (Opportunity o : Trigger.new) {
   Deliverable__c d1 = new Deliverable__c();
   d1.Opportunity__c = o.Id;
   d1.Status__c = 'New';
   d1.Item__c = 'Lifecycle Management';
   DeliverablesForInsert.add(d1);
   
   Deliverable__c d2 = new Deliverable__c();
   d2.Opportunity__c = o.Id;
   d2.Status__c = 'New';
   d2.Item__c = 'Mileage and Fuel PPT';
   DeliverablesForInsert.add(d2);
   
   Deliverable__c d3 = new Deliverable__c();
   d3.Opportunity__c = o.Id;
   d3.Status__c = 'New';
   d3.Item__c = 'M&R Analysis';
   DeliverablesForInsert.add(d3);
   
   Deliverable__c d4 = new Deliverable__c();
   d4.Opportunity__c = o.Id;
   d4.Status__c = 'New';
   d4.Item__c = 'Utilization Report';
   DeliverablesForInsert.add(d4);
   
   Deliverable__c d5 = new Deliverable__c();
   d5.Opportunity__c = o.Id;
   d5.Status__c = 'New';
   d5.Item__c = 'Lease v Buy';
   DeliverablesForInsert.add(d5);
   
}

if (DeliverablesForInsert.size() > 0) {
   insert DeliverablesForInsert;
}
*/
}