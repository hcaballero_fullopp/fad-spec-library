trigger UpdateOpportunityAmountFromSubGroup on Opportunity_SubGroup__c (after insert, after update, after delete, after undelete) {
//
/***********************************************************************************
*  
* Trigger:      UpdateOpportunityAmountFromSubGroup
* Author:       Harvest Solutions
* Date:         March 2014
* Description:  Update Opportunity and Group Totals when a SubGroup is changed
*
* Updates
*
* Date        Author         Description
*------------------------------------------------------------------------------------
*
*
************************************************************************************/

/*
    Commented by fullOpp / Mauricio Offermann caused by issue found
   Set<Id> GroupIds = new Set<Id>();
   Set<Id> OppIds = new Set<Id>();
   if (Trigger.isdelete) {
      for (Opportunity_Subgroup__c sg : Trigger.old) {
      GroupMap.put(sg.Opportunity_Group__c);
      }
   } else {
      for (Opportunity_Subgroup__c sg : Trigger.new) {
         GroupMap.put(sg.Opportunity_Group__c);
      }
   }
   // Collect Opportunity Ids
   for (Opportunity_Group__c og : [select Id, Opportunity__c from Opportunity_Group__c where Id in :GroupIds]) {
      OppMap.put(og.Opportunity__c);
   }
   if (OppIds.size() > 0) {
      OpportunityUPdate ou = new OpportunityUpdate();
      ou.UpdateOpps(OppIds);
   }
    ******/
    
    Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
    Map<Id, Opportunity_Group__c> erMap = new Map<Id, Opportunity_Group__c>();
    if (Trigger.new != null) {
        for (Opportunity_SubGroup__c spec : Trigger.new) {
            if (!opportunityMap.containsKey(spec.Opportunity_ID__c))
                opportunityMap.put(spec.Opportunity_ID__c, new Opportunity(Amount = 0, Id = spec.Opportunity_ID__c, OEM_Make__c = ''));
            if (!erMap.containsKey(spec.Opportunity_Group__c))
                erMap.put(spec.Opportunity_Group__c, new Opportunity_Group__c(Group_Total__c = 0, Id = spec.Opportunity_Group__c));
        }
    }
    if (Trigger.old != null) {
        for (Opportunity_SubGroup__c spec : Trigger.old) {
            if (!opportunityMap.containsKey(spec.Opportunity_ID__c))
                opportunityMap.put(spec.Opportunity_ID__c, new Opportunity(Amount = 0, Id = spec.Opportunity_ID__c, OEM_Make__c = ''));
            if (!erMap.containsKey(spec.Opportunity_Group__c))
                erMap.put(spec.Opportunity_Group__c, new Opportunity_Group__c(Group_Total__c = 0, Id = spec.Opportunity_Group__c));
        }
    }
    
    Opportunity_Group__c[] groups = [SELECT Opportunity__c, (SELECT Manufacturer__c FROM Opportunity_SubGroups__r WHERE Active__c = true) FROM Opportunity_Group__c WHERE Id IN :erMap.keySet()];
    for (Opportunity_Group__c er : groups) {
        Opportunity opportunity = opportunityMap.get(er.Opportunity__c);
        if (opportunity == null || er.Opportunity_Subgroups__r == null)
            continue;
        for (Opportunity_Subgroup__c spec : er.Opportunity_Subgroups__r) {
            if (spec.Manufacturer__c != null && !opportunity.OEM_Make__c.contains(spec.Manufacturer__c))
                opportunity.OEM_Make__c += (opportunity.OEM_Make__c == '' ? '' : ', ') + spec.Manufacturer__c; 
        }
    }

    
    AggregateResult[] results = [SELECT SUM(Total_Price__c) Sum, Opportunity_Group__r.Opportunity__c FROM Opportunity_Subgroup__c WHERE Opportunity_Group__r.Opportunity__c IN :opportunityMap.keySet() AND Select_This_SubGroup_For_Opportunity__c = true AND Active__c = true GROUP BY Opportunity_Group__r.Opportunity__c];
    for (AggregateResult result : results) {
        Id opportunityId = (Id)result.get('Opportunity__c');
        Decimal total = (Decimal)result.get('Sum');
        Opportunity opportunity = opportunityMap.get(opportunityId);
        opportunity.Amount = total;
    }
    update opportunityMap.values();
    }