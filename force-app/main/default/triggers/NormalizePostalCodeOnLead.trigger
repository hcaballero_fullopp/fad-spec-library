trigger NormalizePostalCodeOnLead on Lead (before insert, before update) {
	private String leftPad(String source, Integer length, String pad) {
		if (source == null)
		return leftPad('', length, pad);
		if (source.length() >= length)
		return source;
		return leftPad(pad+source, length, pad);
	}
	
	for (Lead lead : Trigger.new) {
		if (lead.PostalCode != null && lead.PostalCode.trim() != '') {
			lead.PostalCode = lead.PostalCode.trim();
			String regex = '\\d{3,5}(-\\d{4})?';
			if (!Pattern.matches(regex, lead.PostalCode)) {
				lead.PostalCode.addError('The Zip Code format is incorrect.');
				continue;
							}
							if (!lead.PostalCode.contains('-')) {
								lead.PostalCode = leftPad(lead.PostalCode, 5, '0');
								continue;
							}
							String[] zip = lead.PostalCode.split('\\-');
							lead.PostalCode = leftPad(zip[0], 5, '0') + '-' + zip[1];
		}
	}

}