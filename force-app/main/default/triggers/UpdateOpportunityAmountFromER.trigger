trigger UpdateOpportunityAmountFromER on Opportunity_Group__c (after delete, after undelete, after update) {
    
    Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
    if (Trigger.new != null) {
        for (Opportunity_Group__c er : Trigger.new) {
            Opportunity_Group__c old = Trigger.oldMap != null ? Trigger.oldMap.get(er.Id) : null;
            if (er.Opportunity__c != null && !opportunityMap.containsKey(er.Opportunity__c) && (old == null || old.Quantity__c != er.Quantity__c))
                opportunityMap.put(er.Opportunity__c, new Opportunity(Amount = 0, Id = er.Opportunity__c));
        }
    }
    
    if (Trigger.old != null) {
        for (Opportunity_Group__c er : Trigger.old) {
            Opportunity_Group__c newER = Trigger.newMap != null ? Trigger.newMap.get(er.Id) : null;
            if (er.Opportunity__c != null && !opportunityMap.containsKey(er.Opportunity__c) && (newER == null || newER.Quantity__c != er.Quantity__c))
                opportunityMap.put(er.Opportunity__c, new Opportunity(Amount = 0, Id = er.Opportunity__c));
        }
    }
    
    if (opportunityMap.size() == 0)
        return;

    AggregateResult[] results = [SELECT SUM(Total_Price__c) Sum, Opportunity_Group__r.Opportunity__c FROM Opportunity_Subgroup__c WHERE Opportunity_Group__r.Opportunity__c IN :opportunityMap.keySet() AND Select_This_SubGroup_For_Opportunity__c = true AND Active__c = true GROUP BY Opportunity_Group__r.Opportunity__c];
    for (AggregateResult result : results) {
        Id opportunityId = (Id)result.get('Opportunity__c');
        Decimal total = (Decimal)result.get('Sum');
        total = total == null ? 0 : total;
        Opportunity opportunity = opportunityMap.get(opportunityId);
        opportunity.Amount = total;
    }
    update opportunityMap.values();
    }