trigger OnSiteCallToTask on Onsite_Call__c (after insert, after update, after delete, after undelete) {
	
	List<Task> tasks = new List<Task>();
	if (Trigger.new != null) {
		for ( Onsite_Call__c call : Trigger.new) {
			String notes = call.Notes__c != null ? call.Notes__c : '';
			notes = notes.replaceAll('\\<br>', '\n');
			notes = notes.replaceAll('\\<[^>]+>', '');
			Task task = new Task(Onsite_Call_Id__c = call.Id, OwnerId = call.Completed_by__c, WhatId = call.Account__c, Subject = call.Subject_or_Keywords__c,Description = notes, ActivityDate = call.Date_of_Onsite__c, WhoId = call.Contact__c, Type = 'Onsite Call', Status = 'Completed');
									tasks.add(task);
		}
		upsert tasks Task.Onsite_Call_Id__c;
	}
	if (Trigger.IsDelete) {
		tasks = [SELECT Id FROM Task WHERE Onsite_Call_Id__c IN :Trigger.oldMap.keySet()];
		delete tasks;
	}
}