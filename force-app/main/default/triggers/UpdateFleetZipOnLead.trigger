trigger UpdateFleetZipOnLead on Lead (before insert, before update) {
	
	Set<String> zips = new Set<String>();
	for (Lead lead : Trigger.new) {
		if (!zips.contains(lead.Normalized_ZIP_Code__c))
		zips.add(lead.Normalized_ZIP_Code__c);
	}
	
	FleetZip__c[] fleetZips = [SELECT Id, Name FROM FleetZip__c WHERE Name IN :zips];
	Map<String, FleetZip__c> fleetZipMap = new Map<String, FleetZip__c>();
	for (FleetZip__c fleet : fleetZips)
		fleetZipMap.put(fleet.Name, fleet);
		
for (Lead lead : Trigger.new) {
	FleetZip__c fleet = fleetZipMap.get(lead.Normalized_ZIP_Code__c);
	if (fleet != null)
		lead.FleetZip__c = fleet.Id;
}
}