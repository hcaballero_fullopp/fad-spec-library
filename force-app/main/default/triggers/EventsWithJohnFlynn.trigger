trigger EventsWithJohnFlynn on Event (after insert) {
	
	GroupMember[] members = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.Name = 'John Flynn'];
	System.assert(members.size() > 0, 'There are no members on John Flynn public group.');
	Set<Id> userIds = new Set<Id>();
	for (GroupMember member : members)
	userIds.add(member.UserOrGroupId);
	User[] users = [SELECT Id, Email FROM User WHERE Id IN :userIds AND IsActive = true];
	List<String> emails = new List<String> {'MCrucilla@fleetadvantage.net'};
	for (User user : users)
	emails.add(user.Email);

	System.assert(emails.size() > 0, 'There are no emails');
	Set<Id> accountIds = new Set<Id>();
	Set<Id> ownerIds = new Set<Id>();
	for (Event event : Trigger.new) {
		accountIds.add(event.WhatId);
		ownerIds.add(event.OwnerId);
	}
	Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :accountIds]);
	Map<Id, User> ownerMap = new Map<Id, User>([SELECT Id, Name FROM User WHERE Id IN :ownerIds]);
	
	//EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = 'Events with John Flynn'];

	

	List<Messaging.SingleEmailMessage> messages =new List<Messaging.SingleEmailMessage>();
	for (Event event : Trigger.new) {
		if (event.John_Flynn_Presence_Requested__c && event.WhatId != null && String.valueOf(event.WhatId).startsWith('001')) {
			Account account = accountMap.get(event.WhatId);
			User owner = ownerMap.get(event.OwnerId);
			System.assertNotEquals(null, account, 'Related account does not exist');
			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
			message.setToAddresses(emails);
String url = 'https://fleetadvantage.my.salesforce.com/' + event.Id;
			String plainBody = '' +
					'Hello,\n' +
				'\n' +
				'John\'s presence has been requested at an on-site call by a BDEX: \n' +
				'\n' +
				'Account: ' + account.Name + '\n' +
				'BDEX: ' + owner.Name + '\n' +
				'Meeting Date: ' + event.ActivityDate + '\n' +
				'Time Zone: ' + (event.Time_Zone__c == null ? '' : event.Time_Zone__c) + '\n' +
				'Nearest Airport: ' + (event.Nearest_Airport__c == null ? '' : event.Nearest_Airport__c) + '\n' +
				'\n' +
				'Agenda: ' + (event.Description == null ? '' : event.Description) + '\n' +
				'\n' +
				'Link to the request for full details:\n' +
				'\n' +
				url + '\n' +
				'\n';
		message.setPlainTextBody(plainBody);
			String htmlBody = '<html><body>' +
					'Hello,<br/>' +
				'<br/>' +
				'John\'s presence has been requested at an on-site call by a BDEX: <br/>' +
				'<br/>' +
				'<b>Account:</b> ' + account.Name + '<br/>' +
				'<b>BDEX:</b> ' + owner.Name + '<br/>' +
				'<b>Meeting Date:</b> ' + event.ActivityDate + '<br/>' +
				'<b>Time Zone:</b> ' + (event.Time_Zone__c == null ? '' : event.Time_Zone__c) + '<br/>' +
				'<b>Nearest Airport:</b> ' + (event.Nearest_Airport__c == null ? '' : event.Nearest_Airport__c) + '<br/>' +
				'<br/>' +
				'<b>Agenda:</b> ' + (event.Description == null ? '' : event.Description) + '<br/>' +
				'<br/>' +
				'Link to the request for full details:<br/>' +
				'<br/>' +
				 '<a href="' + url + '">' + url + '</a><br/>' +
				'<br/></body></html>';
		message.setHtmlBody(htmlBody);
			message.setSubject('John Flynn requested at On-site Call: ' + account.Name);
			message.setSaveAsActivity(false);
			messages.add(message);
		}
	}
	if (messages.size() > 0 && emails.size() > 0) {
				Messaging.reserveSingleEmailCapacity(messages.size());
		Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
		for (Messaging.SendEmailResult result : results) {
			System.assert(result.isSuccess(), 'There was an error sending the email, plase try again.');
		}
	}
}