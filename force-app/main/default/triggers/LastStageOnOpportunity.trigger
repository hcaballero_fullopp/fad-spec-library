trigger LastStageOnOpportunity on Opportunity (before insert, before update) {
	
	for (Opportunity opportunity : Trigger.new) {
		Opportunity old = Trigger.isUpdate ? Trigger.oldMap.get(opportunity.Id) : null;
		if (Trigger.isInsert && old == null)
		opportunity.Last_Stage__c = '1 - Estimate - Gathering Info CNA';
		else if (old.StageName != opportunity.StageName)
		opportunity.Last_Stage__c = old.StageName;
		if (opportunity.Is_Approved__c == 'No' && (old == null || old.Is_Approved__c != opportunity.Is_Approved__c))
		opportunity.StageName = opportunity.Last_Stage__c;
	}

}