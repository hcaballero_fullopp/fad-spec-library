trigger DoubleEncodeOpportunityName on Opportunity (before insert, before update) {
Set<Id> accountIds = new Set<Id>();
	for (Opportunity opportunity : Trigger.new) {
if (!accountIds.contains(opportunity.AccountId) && opportunity.AccountId != null)
			accountIds.add(opportunity.AccountId);
	}
	
	Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :accountIds]);
	
	for (Opportunity opportunity : Trigger.new) {
		Account account = accountMap.get(opportunity.AccountId);
		opportunity.Encoded_Name__c = Encodingutil.urlEncode(Opportunity.Name, 'UTF-8');
		opportunity.Double_Encoded_Name__c = Encodingutil.urlEncode(opportunity.Encoded_Name__c, 'UTF-8');
		opportunity.Encoded_Account_Name__c = account == null ? 'Please%20press%20look%20up%20icon' : EncodingUtil.urlEncode(account.Name, 'UTF-8');
	}
}