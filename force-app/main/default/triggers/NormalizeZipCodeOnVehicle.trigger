trigger NormalizeZipCodeOnVehicle on fO_Vehicle__c (before insert, before update) {
	private String leftPad(String source, Integer length, String pad) {
		if (source == null)
		return leftPad('', length, pad);
		if (source.length() >= length)
		return source;
		return leftPad(pad+source, length, pad);
	}
	
	for (fO_Vehicle__c vehicle : Trigger.new) {
		if (vehicle.ZIP_Code__c != null && vehicle.ZIP_Code__c.trim() != '') {
			vehicle.ZIP_Code__c = vehicle.ZIP_Code__c.trim();
			String regex = '\\d{3,5}(-\\d{4})?';
			if (!Pattern.matches(regex, vehicle.ZIP_Code__c)) {
				vehicle.ZIP_Code__c.addError('The Zip Code format is incorrect.');
				continue;
							}
							if (!vehicle.ZIP_Code__c.contains('-')) {
								vehicle.ZIP_Code__c = leftPad(vehicle.ZIP_Code__c, 5, '0');
								continue;
							}
							String[] zip = vehicle.ZIP_Code__c.split('\\-');
							vehicle.ZIP_Code__c = leftPad(zip[0], 5, '0') + '-' + zip[1];
		}
	}

}