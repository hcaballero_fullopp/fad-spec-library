trigger PotentialDealSizeAverage on Opportunity_SubGroup__c (after delete, after insert, after undelete, 
after update) {
    Map<Id, Opportunity_Group__c> equipmentRequestMap = new Map<Id, Opportunity_Group__c>();
    Opportunity_SubGroup__c[] specs = Trigger.isDelete ? trigger.old : Trigger.new;
    for (Opportunity_SubGroup__c spec : specs) {
        equipmentRequestMap.put(spec.Opportunity_Group__c, new Opportunity_Group__c(Id = spec.Opportunity_Group__c, Potential_Deal_Size__c = 0));
    }
    
    AggregateResult[] results = [SELECT AVG(Total_Unit_Price__c) PotentialDealSize, Opportunity_Group__c FROM Opportunity_SubGroup__c WHERE LPAF_Request__c <> NULL AND Opportunity_Group__c IN :equipmentRequestMap.keySet() GROUP BY Opportunity_Group__c];
    
    for (AggregateResult result : results) {
        Id equipmentRequestId = (Id)result.get('Opportunity_Group__c');
        Decimal potentialDealSize = (Decimal)result.get('PotentialDealSize');
        Opportunity_Group__c equipmentRequest = equipmentRequestMap.get(equipmentRequestId);
        if (equipmentRequest != null)
        equipmentRequest.Potential_Deal_Size__c = potentialDealSize;
    }
    update equipmentRequestMap.values();
}