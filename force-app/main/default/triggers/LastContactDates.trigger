trigger LastContactDates on Onsite_Call__c (after delete, after insert, after undelete, 
after update) {
	Set<Id> accountIds = new Set<Id>();
	if (Trigger.new != null)
		for (Onsite_Call__c call : Trigger.new) {
			if (!accountIds.contains(call.Account__c))
				accountIds.add(call.Account__c);
		}
		
	if (Trigger.old != null)
		for (Onsite_Call__c call : Trigger.old) {
			if (!accountIds.contains(call.Account__c))
				accountIds.add(call.Account__c);
		}
AggregateResult[] results = [SELECT MAX(Date_of_Finance_Contact__c) FinanceDate, MAX(Date_of_Operations_Contact__c) OperationsDate, Account__c FROM Onsite_Call__c WHERE Account__c IN :accountIds AND Account__c <> NULL GROUP BY Account__c];
	List<Account> accounts = new List<Account>();
	for (AggregateResult result : results) {
		Date financeDate = (Date)result.get('FinanceDate');
		Date operationsDate = (Date)result.get('OperationsDate');
		Id accountId = (Id)result.get('Account__c');
		Account account = new Account(Id = accountId, Onsite_with_Finance__c = financeDate, Onsite_with_Operations__c = operationsDate);
		accounts.add(account);
	}
	update accounts;
}